// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
// TODO: Include a description of the file here.
//

using System;
using DI.Test;
using DI.Core.Pooling;
using UnityEngine;

namespace DI.Core.Test
{
	public class Test_Pooling : DI_Test, DI_TestInterface
	{

		public GameObject goOne;
		public GameObject goTwo;
		public GameObject goThree;
		public GameObject goFour;
		public GameObject goFive;

		public Test_Pooling(): base() {}
		
		public bool buildUp()
		{
			DI_PoolManager.reset();

			goOne = new GameObject("Test Object One");
			goTwo = new GameObject("Test Object Two");
			goThree = new GameObject("Test Object Three");
			goFour = new GameObject("Test Object Four");
			goFive = new GameObject("Test Object Five");

			DI_PoolManager.addObjectToManagedPool(goOne);
			DI_PoolManager.addObjectToManagedPool(goTwo);
			DI_PoolManager.addObjectToManagedPool(goThree);
			DI_PoolManager.addObjectToManagedPool(goFour);
			DI_PoolManager.addObjectToManagedPool(goFive);
			return true;
		}
		
		public DI_TestResult run()
		{
			DI_PoolManager.setStartingSize(10);
			DI_PoolManager.setWillGrow(true);
			DI_PoolManager.initalize();

			isEqual<bool>((DI_PoolManager.getPooledObject("Test Object One") == null), false, "Adding an object to pool management and fetching it works. 1/10");
			isEqual<bool>((DI_PoolManager.getPooledObject("Test Object Two") == null), false, "Adding an object to pool management and fetching it works. 2/10");
			isEqual<bool>((DI_PoolManager.getPooledObject("Test Object Three") == null), false, "Adding an object to pool management and fetching it works. 3/10");
			isEqual<bool>((DI_PoolManager.getPooledObject("Test Object Four") == null), false, "Adding an object to pool management and fetching it works. 4/10");
			isEqual<bool>((DI_PoolManager.getPooledObject("Test Object Five") == null), false, "Adding an object to pool management and fetching it works. 5/10");
			isEqual<bool>((DI_PoolManager.getPooledObject(goOne) == null), false, "Adding an object to pool management and fetching it works. 6/10");
			isEqual<bool>((DI_PoolManager.getPooledObject(goTwo) == null), false, "Adding an object to pool management and fetching it works. 7/10");
			isEqual<bool>((DI_PoolManager.getPooledObject(goThree) == null), false, "Adding an object to pool management and fetching it works. 8/10");
			isEqual<bool>((DI_PoolManager.getPooledObject(goFour) == null), false, "Adding an object to pool management and fetching it works. 9/10");
			isEqual<bool>((DI_PoolManager.getPooledObject(goFive) == null), false, "Adding an object to pool management and fetching it works. 10/10");
			return new DI_TestResult(passedTests, failedTests, totalTests);
		}
		
		public bool tearDown()
		{
			DI_PoolManager.removeObjectFromManagedPool(goOne);
			DI_PoolManager.removeObjectFromManagedPool(goTwo);
			DI_PoolManager.removeObjectFromManagedPool(goThree);
			DI_PoolManager.removeObjectFromManagedPool(goFour);
			DI_PoolManager.removeObjectFromManagedPool(goFive);

			GameObject.DestroyImmediate(goOne);
			GameObject.DestroyImmediate(goTwo);
			GameObject.DestroyImmediate(goThree);
			GameObject.DestroyImmediate(goFour);
			GameObject.DestroyImmediate(goFive);

			DI_PoolManager.reset();
			return true;
		}
		
		public string getTestName() {
			return "Pool Manager Test";
		}
	}
}