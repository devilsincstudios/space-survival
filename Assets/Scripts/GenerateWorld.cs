﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public enum Parts
{
	StraightSingle,
	StraightDouble,
	StraightSingleTall,
	StraightDoubleTall,
	StraightSingleToDouble,
	StraightDoubleToSingle,
	TurnLeftSingle,
	TurnRightSingle,
	RampUp,
	RampDown,
	Start,
	End
}

public class GenerateWorld : MonoBehaviour {

	public List<GameObject> StraightSingle;
	public List<GameObject> StraightDouble;
	public List<GameObject> StraightSingleTall;
	public List<GameObject> StraightDoubleTall;
	public List<GameObject> StraightSingleToDouble;
	public List<GameObject> StraightDoubleToSingle;
	public List<GameObject> SingleTurnLeft;
	public List<GameObject> SingleTurnRight;
	public List<GameObject> RampUp;
	public List<GameObject> RampDown;
	public List<GameObject> Start;
	public List<GameObject> End;

	public string levelGenSeed = "007";
	public int maxParts = 100;

	public float turnChance = 0.2f;
	public float rampChange = 0.05f;

	public float generateDelay = 0f;

	public Vector3 currentPosition = Vector3.zero;
	public Vector3 currentRotation = Vector3.zero;
	public GameObject worldRoot;

	public void OnEnable()
	{
		StartCoroutine(Generate());
	}

	private void spawnPart(GameObject prefab)
	{
		GameObject part = GameObject.Instantiate(prefab);
		part.transform.parent = worldRoot.transform;
		part.transform.position = currentPosition;
		part.transform.localRotation = Quaternion.Euler(currentRotation);
	}

	public GameObject randomPart(List<GameObject> options)
	{
		return options[UnityEngine.Random.Range(0, options.Count - 1)];
	}

	public Parts getNextPart(Parts previous)
	{
		List<Parts> selection = getPossibleSelections(previous);
		return selection[UnityEngine.Random.Range(0, selection.Count -1)];
	}

	public List<Parts> getPossibleSelections(Parts previous)
	{
		List<Parts> selection = new List<Parts>();
		switch (previous)
		{
			case Parts.StraightDouble:
				selection.Add(Parts.StraightDoubleToSingle);
				selection.Add(Parts.StraightDouble);
				break;
			case Parts.TurnLeftSingle:
				selection.Add(Parts.StraightSingleToDouble);
				selection.Add(Parts.StraightSingle);
				selection.Add(Parts.TurnRightSingle);
				//selection.Add(Parts.RampUp);
				//selection.Add(Parts.RampDown);
				break;
			case Parts.TurnRightSingle:
				selection.Add(Parts.StraightSingleToDouble);
				selection.Add(Parts.StraightSingle);
				selection.Add(Parts.TurnLeftSingle);
				//selection.Add(Parts.RampUp);
				//selection.Add(Parts.RampDown);
				break;
			case Parts.StraightSingleToDouble:
				selection.Add(Parts.StraightDouble);
				break;
			case Parts.StraightDoubleToSingle:
				selection.Add(Parts.StraightSingleToDouble);
				selection.Add(Parts.StraightSingle);
				selection.Add(Parts.TurnLeftSingle);
				selection.Add(Parts.TurnRightSingle);
				//selection.Add(Parts.RampUp);
				//selection.Add(Parts.RampDown);
				break;
			case Parts.RampUp:
				selection.Add(Parts.StraightSingleToDouble);
				selection.Add(Parts.StraightSingle);
				selection.Add(Parts.TurnLeftSingle);
				selection.Add(Parts.TurnRightSingle);
				break;
			case Parts.RampDown:
				selection.Add(Parts.StraightSingleToDouble);
				selection.Add(Parts.StraightSingle);
				selection.Add(Parts.TurnLeftSingle);
				selection.Add(Parts.TurnRightSingle);
				break;
			default:
				selection.Add(Parts.StraightSingleToDouble);
				selection.Add(Parts.StraightSingle);
				selection.Add(Parts.TurnLeftSingle);
				selection.Add(Parts.TurnRightSingle);
				//selection.Add(Parts.RampUp);
				//selection.Add(Parts.RampDown);
				break;
		}
		return selection;
	}


	public IEnumerator Generate()
	{
		worldRoot = GameObject.Find("World");
		if (worldRoot == null) {
			worldRoot = new GameObject("World");
		}
		else {
			DestroyImmediate(worldRoot, true);
			worldRoot = new GameObject("World");
		}

		currentPosition = Vector3.zero;
		currentRotation = Vector3.zero;

		UnityEngine.Random.InitState(levelGenSeed.GetHashCode());

		int partsPlaced = 0;
		Parts selectedPart = Parts.Start;

		while (partsPlaced < maxParts) {
			if (generateDelay > 0f) {
				yield return new WaitForSeconds(generateDelay);
			}
			Debug.Log("Selected part: " + selectedPart.ToString());

			switch (selectedPart) {
				case Parts.Start:
					spawnPart(randomPart(Start));
					break;
				case Parts.End:
					spawnPart(randomPart(End));
					break;
				case Parts.StraightDouble:
					spawnPart(randomPart(StraightDouble));
					break;
				case Parts.StraightSingle:
					spawnPart(randomPart(StraightSingle));
					break;
				case Parts.StraightSingleTall:
					spawnPart(randomPart(StraightSingleTall));
					break;
				case Parts.StraightDoubleTall:
					spawnPart(randomPart(StraightDoubleTall));
					break;
				case Parts.StraightSingleToDouble:
					spawnPart(randomPart(StraightSingleToDouble));
					break;
				case Parts.StraightDoubleToSingle:
					spawnPart(randomPart(StraightDoubleToSingle));
					break;
				case Parts.RampUp:
					spawnPart(randomPart(RampUp));
					break;
				case Parts.RampDown:
					spawnPart(randomPart(RampDown));
					break;

				case Parts.TurnLeftSingle:
					spawnPart(randomPart(SingleTurnLeft));
					break;
				case Parts.TurnRightSingle:
					spawnPart(randomPart(SingleTurnRight));
					break;
			}

			switch (selectedPart) {
				case Parts.TurnLeftSingle:
					currentRotation += new Vector3(0f, -180f, 0f);
					break;
				case Parts.TurnRightSingle:
					currentRotation += new Vector3(0f, 180f, 0f);
					break;
			}

			if (currentRotation.y == -180f) {
				currentPosition += new Vector3(0f, 0f, 1f);
			}
			else if (currentRotation.y == 180f) {
				currentPosition += new Vector3(0f, 0f, -1f);
			}
			else {
				currentPosition += new Vector3(1f, 0f, 0f);
			}

			selectedPart = getNextPart(selectedPart);
			partsPlaced++;
		}
	}
}