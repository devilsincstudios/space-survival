﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.AI;
using UnityEngine.SceneManagement;

namespace MapGen
{
	public class MapGenerator : MonoBehaviour
	{
		public static MapGenerator instance;

		protected Map map;
		protected float progress = 0f;
		protected bool finished = false;
		[SerializeField]
		protected GeneratorSettings settings;
		protected GameObject worldRoot;
		protected int roomsPlaced = 0;
		protected List<int> elevations = new List<int>();
		protected string status;

		public bool randomNumberOfTiles = true;

		public bool runBuildFloors = true;
		public bool runBuildWalls = true;
		public bool runBuildMap = true;
		public bool runBuildNavMesh = true;

		public bool showDebugObjects = false;
		public LayerMask navLayerMask;

		private NavMeshDataInstance navaDataInstance;

		public string currentStatus()
		{
			return status;
		}


		public void Awake()
		{
			if (instance == null) {
				instance = this;
			}
			else {
				Destroy(this);
			}

			if (PlayerPrefs.GetString("Seed", "") != "") {
				settings.mapSeed = PlayerPrefs.GetString("Seed");
				PlayerPrefs.SetString("Seed", "");
			}

			if (randomNumberOfTiles) {
				settings.numberOfTiles = UnityEngine.Random.Range(settings.minNumberOfTiles, settings.maxNumberOfTiles + 1);
			}

			addElevation(0);
			startBuild(settings);
			SceneManager.sceneUnloaded += OnSceneUnloaded;
		}

		public void addElevation(int height)
		{
			if (!elevations.Contains(height)) {
				elevations.Add(height);
			}
		}

		public void startBuild(GeneratorSettings _settings)
		{
			this.settings = _settings;
			this.settings.setRandomTileSet();
			if (settings.mapSeed == "") {
				settings.mapSeed = generateSeed();
			}
			UnityEngine.Random.InitState(settings.mapSeed.GetHashCode());
			StartCoroutine(build());
		}

		public string generateSeed()
		{
			//return UnityEngine.Random.Range(0, 1000000) + "";
			List<string> lines = new List<string>(Resources.Load<TextAsset>("words").text.Split(System.Environment.NewLine.ToCharArray()));
			return lines[UnityEngine.Random.Range(0, lines.Count)] + "_" + UnityEngine.Random.Range(0, lines.Count);
		}

		public string getMapSeed()
		{
			return settings.mapSeed;
		}

		public IEnumerator build()
		{
			float startTime = Time.realtimeSinceStartup;
			Debug.Log("Map generation started");
			// Destory the old map if one exists.
			worldRoot = GameObject.Find("World");
			if (worldRoot == null) {
				worldRoot = new GameObject("World");
			}
			else {
				DestroyImmediate(worldRoot, true);
				worldRoot = new GameObject("World");
			}

			map = new Map();
			map.setMapRoot(worldRoot);
			map.setGridSize(settings);

			status = "Generating Map: Building Floors";
			progress = 0f;
			if (runBuildFloors) {
				yield return StartCoroutine(buildFloors());
				Debug.Log("Build Floors finished in: " + (Time.realtimeSinceStartup - startTime) + " seconds");
			}
			progress = 1f/4f;

			status = "Generating Map: Building Walls";
			if (runBuildWalls) {
			float startTimeBuildWalls = Time.realtimeSinceStartup;
				yield return StartCoroutine(buildWalls());
				Debug.Log("Build Walls finished in: " + (Time.realtimeSinceStartup - startTimeBuildWalls) + " seconds");
			}
			progress = 2f/4f;

			status = "Generating Map: Finalizing Map";
			if (runBuildMap) {
				float startTimeFinalize = Time.realtimeSinceStartup;
				yield return StartCoroutine(buildMap());
				Debug.Log("Finalize Map finished in: " + (Time.realtimeSinceStartup - startTimeFinalize) + " seconds");
			}
			progress = 3f/4f;

			status = "Generating Map: Building NavMesh";
			if (runBuildMap && runBuildNavMesh) {
				float startTimeNavMesh = Time.realtimeSinceStartup;
				yield return StartCoroutine(buildNavMesh());
				Debug.Log("Build NavMesh finished in: " + (Time.realtimeSinceStartup - startTimeNavMesh) + " seconds");
			}
			progress = 4f/4f;
			status = "Generating Map: Finished";
			finished = true;
			Debug.Log("Map generation finished in: " + (Time.realtimeSinceStartup - startTime) + " seconds");
		}

		protected bool isRoomTileValid(Vector3 position)
		{
			try {
				MapTile tile = map.getTile(position);
				if (tile.type == Tiles.Entrance || tile.type == Tiles.Exit || tile.type == Tiles.Room || tile.type == Tiles.Ramp || tile.type == Tiles.Void) {
					//Debug.Log("Checking Tile @: " + position + " invalid location");
					return false;
				}
				//Debug.Log("Checking Tile @: " + position + " valid location");
				return true;
			}
			catch (NullReferenceException) {
				//Debug.Log("Checking Tile @: " + position + " no tile found, valid location");
				return true;
			}
		}

		/// <summary>
		/// Places the room tiles.
		/// </summary>
		/// <returns><c>true</c>, if room tiles was placed, <c>false</c> otherwise.</returns>
		/// <param name="room">Room.</param>
		/// <param name="currentDirection">Current direction.</param>
		/// <param name="currentPosition">Current position.</param>
		protected bool placeRoomTiles(MapTile room, ref Vector3 currentDirection, ref Vector3 currentPosition)
		{
			// Ideally a room is built heading north, since this won't always be the case when doing the drunken walk, we need to compensate for that.
			// If the tile is facing west X <- []
			// Then the north is to the west
			// If the tile is facing east [] -> X
			// Then the north is to the east
			#region Convert absolute directions to relative directions
			Vector3 relativeNorth;
			Vector3 relativeEast;
			Vector3 relativeSouth;
			Vector3 relativeWest;

			if (currentDirection / settings.tileSet.gridSize == CardinalDirection.north) {
				relativeNorth = CardinalDirection.north;
				relativeEast = CardinalDirection.east;
				relativeSouth = CardinalDirection.south;
				relativeWest = CardinalDirection.west;
				room.rotation = Vector3.zero;
			}
			else if (currentDirection / settings.tileSet.gridSize == CardinalDirection.east) {
				relativeNorth = CardinalDirection.east;
				relativeEast = CardinalDirection.south;
				relativeSouth = CardinalDirection.west;
				relativeWest = CardinalDirection.north;
				room.rotation = new Vector3(0f, 270f, 0f);
			}
			else if (currentDirection / settings.tileSet.gridSize == CardinalDirection.south) {
				relativeNorth = CardinalDirection.south;
				relativeEast = CardinalDirection.west;
				relativeSouth = CardinalDirection.north;
				relativeWest = CardinalDirection.east;
				room.rotation = new Vector3(0f, 180f, 0f);
			}
			else {
				relativeNorth = CardinalDirection.west;
				relativeEast = CardinalDirection.north;
				relativeSouth = CardinalDirection.east;
				relativeWest = CardinalDirection.south;
				room.rotation = new Vector3(0f, 90f, 0f);
			}

			#endregion

			bool validPlacement = true;
			//Debug.Log("Attempting to place " + room.type + " at location: "  + currentPosition + " with a heading of: " + CardinalDirection.directionToString(currentDirection / settings.tileSet.gridSize));

			Vector3 position;

			// Is the placement valid?
			// Does the room or any of the tiles of the room collide with another room, an exit, an entrance or a ramp?
			// Start at the room tile and fan out, east then west of the inital tile
			// Move up one tile north and repeat.
			// Do the same for the south.
			// Directions are relative to the currentDirection when being placed.
			// When checking the placement, go out an extra 5 tiles to make sure its not going to collide with another room.

			#region Check Room Placement
			for (int elevation = -room.downSpace; elevation < room.upSpace; elevation++) {
				for (int north = 0; north < room.northSpace + 5; north++) {
					for (int east = 0; east < room.eastSpace + 5; east++) {
						position = currentPosition + (relativeEast * settings.tileSet.gridSize * east) + (relativeNorth * settings.tileSet.gridSize * north);
						position += new Vector3(0f, settings.tileSet.gridHeight * elevation, 0f);
						if (!isRoomTileValid(position)) {
							validPlacement = false;
							break;
						}
					}
					for (int west = 0; west < room.westSpace + 5; west++) {
						position = currentPosition + (relativeWest * settings.tileSet.gridSize * west) + (relativeNorth * settings.tileSet.gridSize * north);
						position += new Vector3(0f, settings.tileSet.gridHeight * elevation, 0f);
						if (!isRoomTileValid(position)) {
							validPlacement = false;
							break;
						}
					}
				}
				for (int south = 0; south < room.southSpace + 5; south++) {
					for (int east = 0; east < room.eastSpace; east++) {
						position = currentPosition + (relativeEast * settings.tileSet.gridSize * east) + (relativeSouth * settings.tileSet.gridSize * south);
						position += new Vector3(0f, settings.tileSet.gridHeight * elevation, 0f);
						if (!isRoomTileValid(position)) {
							validPlacement = false;
							break;
						}
					}
					for (int west = 0; west < room.westSpace + 5; west++) {
						position = currentPosition + (relativeWest * settings.tileSet.gridSize * west) + (relativeSouth * settings.tileSet.gridSize * south);
						position += new Vector3(0f, settings.tileSet.gridHeight * elevation, 0f);
						if (!isRoomTileValid(position)) {
							validPlacement = false;
							break;
						}
					}
				}
			}

			#endregion

			// Place floors around rooms to make sure that the pathing can not be overwriten which would render them inaccessable.
			// Place the void tiles to represent the space of the room so that nothing else can occupy the space of the room.
			if (validPlacement) {
				//Debug.Log("Attempting to place " + room.type + " at location: "  + currentPosition + " with a heading of: " + CardinalDirection.directionToString(currentDirection / settings.tileSet.gridSize) + " was successfull.");
				map.setTile(currentPosition, room);

				// The Logic of this loop is to start at the bottom and cycle upwards from left to right until all tiles are reached
				for (int elevation = -room.downSpace; elevation < room.upSpace; elevation++) {
					for (int vertical = -room.southSpace; vertical <= room.northSpace; vertical++) {
						for (int horizontal = -room.westSpace; horizontal <= room.eastSpace; horizontal++) {
							position = currentPosition + (relativeEast * settings.tileSet.gridSize * horizontal) + (relativeNorth * settings.tileSet.gridSize * vertical);
							position += new Vector3(0f, settings.tileSet.gridHeight * elevation, 0f);
							if ((vertical == -room.southSpace || vertical == room.northSpace) || (horizontal == -room.westSpace || horizontal == room.eastSpace)) {
								map.setTile(position, settings.tileSet.getFloor());
								//GameObject.CreatePrimitive(PrimitiveType.Cube).transform.position = position;
							}
							else {
								map.setTile(position, settings.tileSet.getVoidTile());
								//GameObject.CreatePrimitive(PrimitiveType.Cube).transform.position = position;
							}
						}
					}
				}
				currentPosition += currentDirection + (relativeNorth * settings.tileSet.gridSize) * (room.northSpace - 1);
				return true;
			}
			else {
				//Debug.Log("Attempting to place " + room.type + " at location: "  + currentPosition + " with a heading of: " + CardinalDirection.directionToString(currentDirection / settings.tileSet.gridSize) + " was successfull.");
				return false;
			}
		}

		protected IEnumerator buildFloors()
		{
			// Drunken walk
			// http://pcg.wikidot.com/pcg-algorithm:drunkard-walk
			// Pick a random point on a filled grid and mark it empty.
			// Choose a random cardinal direction (N, E, S, W).
			// Move in that direction, and mark it empty unless it already was.
			// Repeat steps 2-3, until you have emptied as many grids as desired

			// Start at 0,0,0
			// Build the entrance here.
			Vector3 currentPosition = Vector3.zero;

			Vector3 currentDirection = CardinalDirection.random * settings.tileSet.gridSize;
			Vector3 previousDirection = currentDirection;

			// The entrance and exits are now rooms
			placeRoomTiles(settings.tileSet.getEntrance(), ref currentDirection, ref currentPosition);

			bool placedRoom = false;
			bool placedRamp = false;

			while (map.getTileCount() < settings.numberOfTiles - 1) {
				if (placedRoom || placedRamp) {
					currentDirection = previousDirection;
					placedRoom = false;
					placedRamp = false;
				}
				else {
					currentDirection = CardinalDirection.random * settings.tileSet.gridSize;
					// Loopback protection
					if (currentDirection == previousDirection) {
						// If this check fails, do the loop back.
						if (UnityEngine.Random.Range(0, 100) <= settings.loopProtectionChance) {
							currentDirection = CardinalDirection.random * settings.tileSet.gridSize;
						}
					}
				}

				// Straightness preference
				if (currentDirection != previousDirection) {
					// If this check fails, do the turn.
					if (UnityEngine.Random.Range(0, 100) <= settings.straightChance) {
						currentDirection = CardinalDirection.random * settings.tileSet.gridSize;
					}
				}

				// Roll for a chance to spawn a room.
				if (roomsPlaced < settings.maxNumberOfRooms) {
					if (UnityEngine.Random.Range(0, 100) <= settings.roomChance) {
						// A room has been selected.
						placedRoom = true;
					}
				}

				if (elevations.Count <= settings.maxNumberOfLevels && !placedRoom) {
					if (UnityEngine.Random.Range(0, 100) <= settings.rampChance) {
						// A ramp has been selected.
						placedRamp = true;
					}
				}

				// Rooms are areas that do not conform to the single tile = single grid spot rule
				// A room can contain multiple tiles but it consists of a single tile itself.
				// Areas that the room occupies are carved out with Void tiles to prevent filling them with other tiles.
				if (placedRoom) {
					placedRoom = placeRoomTiles(settings.tileSet.getRoom(), ref currentDirection, ref currentPosition);
					if (placedRoom) {
						roomsPlaced++;
					}
				}

				if (placedRamp) {
					MapTile ramp;
					GameObject startingPoint = null;

					if (showDebugObjects) {
						startingPoint = GameObject.CreatePrimitive(PrimitiveType.Cube);
						startingPoint.name = "Ramp - Starting Point";
						startingPoint.transform.position = currentPosition;
						startingPoint.transform.localRotation = Quaternion.Euler(currentDirection);
						Destroy(startingPoint.GetComponent<BoxCollider>());
					}

					if (UnityEngine.Random.Range(0, 100) <= settings.rampDirectionChance) {
						ramp = settings.tileSet.getRampUp();
						if (showDebugObjects) {
							startingPoint.GetComponent<MeshRenderer>().material.color = Color.yellow;
							startingPoint.transform.position = currentPosition;
						}

						placedRamp = placeRoomTiles(ramp, ref currentDirection, ref currentPosition);
						if (placedRamp) {
							currentPosition += new Vector3(0f, settings.tileSet.gridHeight, 0f);
						}
					}
					else {
						ramp = settings.tileSet.getRampDown();
						if (showDebugObjects) {
							startingPoint.GetComponent<MeshRenderer>().material.color = Color.green;
						}
						currentPosition -= new Vector3(0f, settings.tileSet.gridHeight, 0f);
						if (showDebugObjects) {
							startingPoint.transform.position = currentPosition;
						}

						placedRamp = placeRoomTiles(ramp, ref currentDirection, ref currentPosition);
						if (!placedRamp) {
							currentPosition += new Vector3(0f, settings.tileSet.gridHeight, 0f);
						}
					}

					if (placedRamp) {
						if (showDebugObjects) {
							GameObject endPoint = GameObject.CreatePrimitive(PrimitiveType.Cube);
							endPoint.transform.position = currentPosition;
							endPoint.name = "Ramp - End Point";
							endPoint.GetComponent<MeshRenderer>().material.color = Color.red;
							Destroy(endPoint.GetComponent<BoxCollider>());
						}
						addElevation((int)currentPosition.y);
					}
					else {
						Destroy(startingPoint);
					}
				}

				if (!placedRoom && !placedRamp) {
					// Add the tile to the map.
					map.setTile(currentPosition, settings.tileSet.getFloor());
					currentPosition += currentDirection;
				}
				previousDirection = currentDirection;
			}

			// The entrance and exits are now rooms
			while (!placeRoomTiles(settings.tileSet.getExit(), ref currentDirection, ref currentPosition)) {
				map.setTile(currentPosition, settings.tileSet.getFloor());
				currentPosition += currentDirection;
				previousDirection = currentDirection;
				currentDirection = CardinalDirection.random * settings.tileSet.gridSize;
			}

			yield return null;
		}

		protected IEnumerator buildWalls()
		{
			List<Vector3> mapPositions = map.getPositions();
			List<Vector3> wallPositions = new List<Vector3>();

			for (int index = 0; index < mapPositions.Count; index++) {
				try {
					Neighbors neighbors = map.getNeighbors(mapPositions[index]);

					// Add Walls for null tiles.
					if (neighbors.north == null) {
						map.setTile(mapPositions[index] + CardinalDirection.north * settings.tileSet.gridSize, settings.tileSet.getWall());
						wallPositions.Add(mapPositions[index] + CardinalDirection.north * settings.tileSet.gridSize);
					}
					if (neighbors.east == null) {
						map.setTile(mapPositions[index] + CardinalDirection.east * settings.tileSet.gridSize, settings.tileSet.getWall());
						wallPositions.Add(mapPositions[index] + CardinalDirection.east * settings.tileSet.gridSize);
					}
					if (neighbors.south == null) {
						map.setTile(mapPositions[index] + CardinalDirection.south * settings.tileSet.gridSize, settings.tileSet.getWall());
						wallPositions.Add(mapPositions[index] + CardinalDirection.north * settings.tileSet.gridSize);
					}
					if (neighbors.west == null) {
						map.setTile(mapPositions[index] + CardinalDirection.west * settings.tileSet.gridSize, settings.tileSet.getWall());
						wallPositions.Add(mapPositions[index] + CardinalDirection.west * settings.tileSet.gridSize);
					}
				}
				catch (NullReferenceException)
				{
					
				}
			}
			mapPositions.Clear();

			#region Convert walls to windows if they are an outside wall and pillars if they are surounded by floors
			// Check the number of floor tiles a wall has around it.
			for (int index = 0; index < wallPositions.Count; index++) {
				try {
					Neighbors neighbors = map.getNeighbors(wallPositions[index]);

					// If a wall has 4 floors around it, then it should be a floor
					if (getNeighborsCountOfType(neighbors, Tiles.Floor) == 4) {
						map.setTile(wallPositions[index], settings.tileSet.getFloor());
						continue;
					}

					// If a wall has a missing neighbor then its an outside wall and should be a window.
					// If the window sits against more than one floor, it needs to be a wall.
					// The orientation of the window is dependant on which side is a floor.
					if (getNeighborsCountOfType(neighbors, Tiles.Floor) <= 1) {
						if (getNullNeighborsCount(neighbors) >= 1) {
							MapTile window = settings.tileSet.getWindow();
							if (neighbors.north != null) {
								if (neighbors.north.type == Tiles.Floor) {
									window.rotation = Vector3.zero;
								}
							}
							else if (neighbors.east != null) {
								if (neighbors.east.type == Tiles.Floor) {
									window.rotation = new Vector3(0f, 270f, 0f);
								}
							}
							else if (neighbors.south != null) {
								if (neighbors.south.type == Tiles.Floor) {
									window.rotation = new Vector3(0f, 180f, 0f);
								}
							}
							else if (neighbors.west != null) {
								if (neighbors.west.type == Tiles.Floor) {
									window.rotation = new Vector3(0f, 90f, 0f);
								}
							}

							map.setTile(wallPositions[index], window);
							//Debug.Log("Set Window");
							continue;
						}
					}
				}
				catch (NullReferenceException)
				{
					//map.setTile(wallPositions[index], settings.tileSet.getWindow());
					Debug.Log("Caught a null reference when checking walls");
					//Debug.Log("Set Window");
				}
			}
			#endregion

			yield return null;
		}

		protected IEnumerator buildNavMesh()
		{
			List<NavMeshBuildSource> buildSources = new List<NavMeshBuildSource>();
			List<NavMeshModifier> navMeshModifiers = new List<NavMeshModifier>(worldRoot.GetComponentsInChildren<NavMeshModifier>(true));
			List<NavMeshBuildMarkup> buildMarkups = new List<NavMeshBuildMarkup>();

			for (int index = 0; index < navMeshModifiers.Count; index++) {
				NavMeshBuildMarkup markup = new NavMeshBuildMarkup();
				markup.area = navMeshModifiers[index].area;
				markup.ignoreFromBuild = navMeshModifiers[index].ignoreFromBuild;
				markup.overrideArea = navMeshModifiers[index].overrideArea;
				markup.root = navMeshModifiers[index].gameObject.transform;
				buildMarkups.Add(markup);
			}

			NavMeshBuilder.CollectSources(worldRoot.transform, navLayerMask, NavMeshCollectGeometry.PhysicsColliders, 0, buildMarkups, buildSources);
			NavMeshData navData = NavMeshBuilder.BuildNavMeshData(NavMesh.GetSettingsByID(0), buildSources, new Bounds(Vector3.zero, new Vector3(10000f, 10000f, 10000f)), Vector3.zero, Quaternion.identity);
			navaDataInstance = NavMesh.AddNavMeshData(navData);
			yield return null;
		}

		protected IEnumerator buildMap()
		{
			yield return(StartCoroutine(map.render(settings.mapGenDelay)));
		}

		protected int getNullNeighborsCount(Neighbors neighbors)
		{
			int hits = 0;
			if (neighbors.north == null) {
				++hits;
			}
			if (neighbors.east == null) {
				++hits;
			}
			if (neighbors.south == null) {
				++hits;
			}
			if (neighbors.west == null) {
				++hits;
			}
			return hits;
		}

		protected int getNeighborsCountOfType(Neighbors neighbors, Tiles type)
		{
			int hits = 0;
			if (neighbors.north != null) {
				if (neighbors.north.type == type) {
					++hits;
				}
			}
			if (neighbors.east != null) {
				if (neighbors.east.type == type) {
					++hits;
				}
			}
			if (neighbors.south != null) {
				if (neighbors.south.type == type) {
					++hits;
				}
			}
			if (neighbors.west != null) {
				if (neighbors.west.type == type) {
					++hits;
				}
			}
			return hits;
		}

		public float buildProgress()
		{
			return progress;
		}

		public bool buildComplete()
		{
			return finished;
		}

		public void OnSceneUnloaded(Scene scene)
		{
			Debug.Log("Scene Unloaded");
			NavMesh.RemoveNavMeshData(navaDataInstance);
			SceneManager.sceneUnloaded -= OnSceneUnloaded;
		}
	}
}