﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//
using System;
using System.Collections.Generic;
using UnityEngine;

namespace MapGen
{
	[Serializable]
	public class TileSet : ScriptableObject
	{
		[SerializeField]
		protected List<MapTile> floors;
		[SerializeField]
		protected List<MapTile> entrances;
		[SerializeField]
		protected List<MapTile> exits;
		[SerializeField]
		protected List<MapTile> walls;
		[SerializeField]
		protected List<MapTile> pillars;
		[SerializeField]
		protected List<MapTile> windows;
		[SerializeField]
		protected List<MapTile> rooms;
		[SerializeField]
		protected List<MapTile> rampsUp;
		[SerializeField]
		protected List<MapTile> rampsDown;

		public int gridSize = 1;
		public int gridHeight = 3;

		protected MapTile selectTileWeighted(List<MapTile> tiles)
		{
			MapTile selectedTile = null;

			for (int index = 0; index < tiles.Count; index++) {
				float random = UnityEngine.Random.Range(0f, 100f);
				// If a tile is selected return that tile
				if (tiles[index].selectionChance >= random) {
					return tiles[index];
				}
				// If a tile can't be chosen then select it as the current "winner"
				if (selectedTile == null) {
					selectedTile = tiles[index];
				}
				else if (tiles[index].selectionChance > selectedTile.selectionChance) {
					selectedTile = tiles[index];
				}
			}

			if (selectedTile == null) {
				throw new NullReferenceException("Tileset was unable to resolve a weighted tile request.");
			}

			return selectedTile;
		}

		public MapTile getFloor()
		{
			return selectTileWeighted(floors);
		}

		public MapTile getEntrance()
		{
			return selectTileWeighted(entrances);
		}

		public MapTile getExit()
		{
			return selectTileWeighted(exits);
		}

		public MapTile getWall()
		{
			return selectTileWeighted(walls);
		}

		public MapTile getPillar()
		{
			return selectTileWeighted(pillars);
		}
		public MapTile getWindow()
		{
			return selectTileWeighted(windows);
		}
		public MapTile getRoom()
		{
			return selectTileWeighted(rooms);
		}

		public MapTile getVoidTile()
		{
			MapTile tile = MapTile.CreateInstance<MapTile>();
			tile.type = Tiles.Void;
			tile.name = "Void";
			return tile;
		}

		public MapTile getRampUp()
		{
			return selectTileWeighted(rampsUp);
		}
		public MapTile getRampDown()
		{
			return selectTileWeighted(rampsDown);
		}
	}
}

