﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using System.Collections.Generic;

namespace MapGen
{
	public class MapTile : ScriptableObject
	{
		public Vector3 position = Vector3.zero;
		public Vector3 rotation = Vector3.zero;
		public Tiles type;
		public GameObject prefab;
		public GameObject instance;
		[Range(0f, 100f)]
		public float selectionChance = 1f;
		public int northSpace = 1;
		public int southSpace = 1;
		public int westSpace = 1;
		public int eastSpace = 1;
		public int upSpace = 1;
		public int downSpace = 1;
	}
}