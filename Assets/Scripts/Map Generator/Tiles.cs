﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

namespace MapGen
{
	public enum Tiles
	{
		Floor,
		Wall,
		Pillar,
		Entrance,
		Exit,
		Window,
		Decorative,
		Room,
		Ramp,
		Void,
		Empty,
	}
}
