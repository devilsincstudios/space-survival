﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//
using System;
using UnityEngine;
using System.Collections.Generic;

namespace MapGen
{
	[Serializable]
	public class GeneratorSettings
	{
		[Range(10, 500)]
		public int mapSizeX = 100;
		[Range(10, 500)]
		public int mapSizeY = 100;
		[Range(0, 5000)]
		public int minNumberOfTiles = 200;
		[Range(200, 10000)]
		public int maxNumberOfTiles = 200;
		[Range(0, 10000)]
		public int numberOfTiles = 200;
		[Range(0, 100)]


		public float loopProtectionChance = 25f;
		[Range(0, 100)]
		public float straightChance = 30f;
		[Range(0, 100)]
		public float roomChance = 1f;
		[Range(0,20)]
		public int maxNumberOfRooms = 10;
		[Range(0, 100)]
		public float rampChance = 1f;
		[Range(0, 100)]
		[Tooltip("A setting of 50 here means that for each ramp that is rolled, there is a 50% chance that it goes up. A setting of 20 means a 20% it goes upwards and 80% chance it goes down.")]
		public float rampDirectionChance = 50f;
		[Range(0, 5)]
		public int maxNumberOfLevels = 5;

		public string mapSeed;
		public List<TileSet> tileSets;
		public TileSet tileSet;

		public void setTileSet(TileSet ts)
		{
			if (!tileSets.Contains(ts)) {
				tileSets.Add(ts);
			}

			tileSet = ts;
		}

		public void setRandomTileSet()
		{
			TileSet tileSet = tileSets[UnityEngine.Random.Range(0, tileSets.Count)];
		}

		[Header("Debug setting to view the map being generated at run time.")]
		public float mapGenDelay = 0.1f;
	}
}
