﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//
using UnityEngine;

namespace MapGen
{
	public static class CardinalDirection
	{
		public static Vector3 north = new Vector3(1f, 0f, 0f);
		public static Vector3 south = new Vector3(-1f, 0f, 0f);
		public static Vector3 east = new Vector3(0f, 0f, 1f);
		public static Vector3 west = new Vector3(0f, 0f, -1f);
		public static Vector3 random
		{
			get
			{
				return getRandomDirection();
			}
		}

		public static Vector3 getRandomDirection()
		{
			int direction = UnityEngine.Random.Range(0, 4);

			switch (direction) {
				case 0:
					return north;
				case 1:
					return south;
				case 2:
					return east;
				case 3:
					return west;
				default:
					return north;
			}
		}

		public static string directionToString(Vector3 direction)
		{
			if (direction == north) {
				return "North";
			}
			if(direction == south) {
				return "South";
			}
			if (direction == east)
			{
				return "East";
			}
			if (direction == west) {
				return "West";
			}
			return "???";
		}
	}
}

