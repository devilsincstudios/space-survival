﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

namespace MapGen
{
	public struct Neighbors
	{
		public MapTile north;
		public MapTile east;
		public MapTile south;
		public MapTile west;
		public MapTile up;
		public MapTile down;
	}
}