﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//
using UnityEngine;
using System.Collections.Generic;
using System;
using System.Collections;

namespace MapGen
{
	public class Map
	{
		protected Dictionary<Vector3, MapTile> map;
		protected GameObject mapRoot;
		protected int gridSize = 2;
		protected int gridHeight = 3;

		public Map()
		{
			map = new Dictionary<Vector3, MapTile>();
		}

		/// <summary>
		/// Sets the map root.
		/// </summary>
		/// <param name="mapRoot">Map root.</param>
		public void setMapRoot(GameObject _mapRoot)
		{
			mapRoot = _mapRoot;
		}

		public void setGridSize(GeneratorSettings settings)
		{
			gridSize = settings.tileSet.gridSize;
			gridHeight = settings.tileSet.gridHeight;
		}

		/// <summary>
		/// Update the map to set the tile at the given location.
		/// </summary>
		/// <returns><c>true</c>, if tile already existed, <c>false</c> otherwise.</returns>
		/// <param name="position">Map position.</param>
		/// <param name="tile">Tile.</param>
		public bool setTile(Vector3 position, MapTile tile)
		{
			MapTile mapTile;
			// Check if the position already has a tile in it.
			// If it does follow the rules to decide if its allow to be overwritten or not.
			// mapTile = the tile already stored.
			// tile = the tile requesting to overwrite the stored tile.
			if (map.TryGetValue(position, out mapTile)) {
				switch (mapTile.type) {
					case Tiles.Entrance:
//						Debug.Log("Attempted to replace an entrance with a normal tile, request denied.");
						return false;
					case Tiles.Exit:
//						Debug.Log("Attempted to replace an exit with a normal tile, request denied.");
						return false;
					case Tiles.Ramp:
//						Debug.Log("Attempted to replace a ramp with a normal tile, request denied.");
						return false;
					case Tiles.Room:
//						Debug.Log("Attempted to replace a room with a normal tile, request denied.");
						return false;
					case Tiles.Void:
						if (tile.type == Tiles.Floor) {
//						Debug.Log("Attempted to replace a void tile with a normal tile, request denied.");
							return false;
						}
					break;
					case Tiles.Empty:
					if (tile.type == Tiles.Floor) {
//						Debug.Log("Attempted to replace an empty tile with a normal tile, request denied.");
						return false;
					}
					break;
				}

				if (mapTile.type == Tiles.Floor && (tile.type == Tiles.Wall || tile.type == Tiles.Window)) {
					Debug.Log("Attempted to replace a floor with a wall tile, request denied.");
					return false;
				}
			}

			MapTile tileInstance = UnityEngine.Object.Instantiate(tile) as MapTile;

//			if (tileInstance.type == Tiles.Room) {
//				Debug.Log("Requested to setTile: " + tileInstance.name + " at position: " + position + " with rotation of: " + tileInstance.rotation + " request is valid, setting tile.");
//			}

			if (map.ContainsKey(position)) {
				map[position] = tileInstance;
				return true;
			}
			else {
				map.Add(position, tileInstance);
				return false;
			}
		}

		/// <summary>
		/// Gets the tile located at the given position.
		/// </summary>
		/// <returns>The tile.</returns>
		/// <param name="position">Position.</param>
		public MapTile getTile(Vector3 position)
		{
			if (map.ContainsKey(position)) {
				return map[position];
			}

			return null;
		}

		/// <summary>
		/// Places the tile.
		/// </summary>
		/// <returns><c>true</c>, if tile was placed, <c>false</c> otherwise.</returns>
		/// <param name="position">Position.</param>
		/// <param name="tile">Tile.</param>
		public bool placeTile(Vector3 position)
		{
			MapTile mapTile;
			if (map.TryGetValue(position, out mapTile)) {
				// Tile already exists, replace it
				if (mapTile.instance != null) {
					GameObject.Destroy(mapTile.instance);
				}
				if (mapTile.prefab != null) {
					GameObject newTile = GameObject.Instantiate(mapTile.prefab, mapRoot.transform);
					newTile.transform.position = position;
					newTile.transform.localRotation = Quaternion.Euler(mapTile.rotation);
					mapTile.instance = newTile;
				}
				map[position] = mapTile;
//				if (mapTile.type == Tiles.Room) {
//					Debug.Log("Placed tile: " + mapTile.name + " at location: " + position + " with a rotation of: " + mapTile.rotation);
//				}
				return true;
			}
			Debug.Log("Failed to place tile");
			return false;
		}

		/// <summary>
		/// Removes the tile.
		/// </summary>
		/// <returns><c>true</c>, if tile was removed, <c>false</c> otherwise.</returns>
		/// <param name="position">Position.</param>
		public bool removeTile(Vector3 position)
		{
			MapTile mapTile;
			if (map.TryGetValue(position, out mapTile)) {
				// Tile already exists, replace it
				if (mapTile.instance != null) {
					GameObject.Destroy(mapTile.instance);
				}
				map.Remove(position);
				return true;
			}
			return false;
		}

		/// <summary>
		/// Gets the tile count.
		/// </summary>
		/// <returns>The tile count.</returns>
		public int getTileCount()
		{
			return map.Count;
		}

		public Neighbors getNeighbors(Vector3 position)
		{
			if (getTile(position) != null) {
				Neighbors neighbors = new Neighbors();
				try {
					neighbors.north = getTile(position + (CardinalDirection.north * gridSize));
				}
				catch (NullReferenceException) {
					MapTile tile = MapTile.CreateInstance<MapTile>();
					tile.type = Tiles.Empty;
					neighbors.north = tile;
				}

				try {
					neighbors.east = getTile(position + (CardinalDirection.east * gridSize));
				}
				catch (NullReferenceException) {
					MapTile tile = MapTile.CreateInstance<MapTile>();
					tile.type = Tiles.Empty;
					neighbors.east = tile;
				}

				try {
					neighbors.south = getTile(position + (CardinalDirection.south * gridSize));
				}
				catch (NullReferenceException) {
					MapTile tile = MapTile.CreateInstance<MapTile>();
					tile.type = Tiles.Empty;
					neighbors.south = tile;
				}

				try {
					neighbors.west = getTile(position + (CardinalDirection.west * gridSize));
				}
				catch (NullReferenceException) {
					MapTile tile = MapTile.CreateInstance<MapTile>();
					tile.type = Tiles.Empty;
					neighbors.west = tile;
				}

				try {
					neighbors.up = getTile(position * gridSize + new Vector3(0f, (float)gridHeight, 0f));
				}
				catch (NullReferenceException) {
					MapTile tile = MapTile.CreateInstance<MapTile>();
					tile.type = Tiles.Empty;
					neighbors.up = tile;
				}

				try {
					neighbors.down = getTile(position * gridSize - new Vector3(0f, (float)gridHeight, 0f));
				}
				catch (NullReferenceException) {
					MapTile tile = MapTile.CreateInstance<MapTile>();
					tile.type = Tiles.Empty;
					neighbors.down = tile;
				}

				return neighbors;
			}

			throw new NullReferenceException("Asked for neighbors, but the requested position does not contain a tile");
		}

		public List<Vector3> getPositions()
		{
			return new List<Vector3>(map.Keys);
		}

		public IEnumerator render(float delay)
		{
			List<Vector3> keys = new List<Vector3> (map.Keys);
			GameObject currentObject = GameObject.CreatePrimitive(PrimitiveType.Cube);

			if (delay > 0f) {
				currentObject = GameObject.CreatePrimitive(PrimitiveType.Cube);
				GameObject.Destroy(currentObject.GetComponent<BoxCollider>());
				currentObject.GetComponent<MeshRenderer>().material.color = Color.cyan;
				currentObject.transform.localScale = new Vector3(0.1f, 5f, 0.1f);
			}
			else {
				GameObject.Destroy(currentObject);
			}

			for (int index = 0; index < keys.Count; index++) {
				if (delay > 0f) {
					currentObject.transform.position = keys[index];
					yield return new WaitForSecondsRealtime(delay);
				}
				placeTile(keys[index]);
			}
			yield return null;
		}

		/// <summary>
		/// Releases unmanaged resources and performs other cleanup operations before the <see cref="MapGen.Map"/> is
		/// reclaimed by garbage collection.
		/// </summary>
		~Map()
		{
			map.Clear();
		}
	}
}