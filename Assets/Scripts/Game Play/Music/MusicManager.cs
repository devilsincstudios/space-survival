// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using UnityEngine.Audio;
using System.Collections.Generic;
using System.Collections;

using DI.Core.Behaviours;
using DI.Core.Debug;
using DI.Core.Events;

namespace DI.Music
{
	[AddComponentMenu("Devil's Inc Studios/Managers/Music")]
	public class MusicManager : DI_MonoBehaviourSingleton<MusicManager>
	{
		[Header("Background Music Selection")]
		public List<DI_AudioTrack> backgroundMusic;

		[Header("Audio Source Setup")]
		public AudioSource audioSource;
		public AudioSource audioCueMaster;
		protected Dictionary<DI_AudioTrack, AudioSource> cueSources;
		[Tooltip("It's important to put the snapshots in the right order, Slient first then Normal.")]
		public AudioMixerSnapshot[] crossfadeSnapshots;

		[Header("General Settings")]
		public bool playOnStartup = false;
		public float masterBGMVolume = 0.25f;

		private bool isFading = false;
		public bool isPlayingMusic = true;

		public void OnAwake()
		{
			// If we have another instance active, destroy it and replace it with this one.
			if (instance != null) {
				Destroy(instance);
			}
			makeSingleton(this);
		}

		public IEnumerator delayedStart()
		{
			yield return new WaitForEndOfFrame();
			yield return new WaitForEndOfFrame();
			if (backgroundMusic.Count > 1) {
				DI_Music.playNextRandom();
			}
			else {
				DI_Music.playNext();
			}
			isPlayingMusic = true;
		}

		public void OnEnable()
		{
			DI_Music.clearPlaylist();

			if (backgroundMusic != null) {
				for (int iteration = 0; iteration < backgroundMusic.Count; iteration++) {
					DI_Music.addAudioTrack(backgroundMusic[iteration]);
				}
			}

			DI_Music.setAudioSource(audioSource);

			// There is a delay between start up and the audio system registering new volume settings.
			// Run a coroutine with a slight delay to work around this.
			if (playOnStartup) {
				StartCoroutine(delayedStart());
			}

			audioSource.volume = masterBGMVolume;

			cueSources = new Dictionary<DI_AudioTrack, AudioSource>();
			DI_EventCenter<DI_AudioTrack>.addListener("OnMusicChange", handleMusicChange);
			DI_EventCenter.addListener("OnMusicStart", handleMusicStart);
			DI_EventCenter.addListener("OnMusicStop", handleMusicStop);
			DI_EventCenter<DI_AudioTrack>.addListener("OnAudioCueStart", handleAudioCueStart);
			DI_EventCenter<DI_AudioTrack>.addListener("OnAudioCueEnd", handleAudioCueEnd);
		}

		public void OnDisable()
		{
			DI_EventCenter<DI_AudioTrack>.removeListener("OnMusicChange", handleMusicChange);
			DI_EventCenter.removeListener("OnMusicStart", handleMusicStart);
			DI_EventCenter.removeListener("OnMusicStop", handleMusicStop);
			DI_EventCenter<DI_AudioTrack>.removeListener("OnAudioCueStart", handleAudioCueStart);
			DI_EventCenter<DI_AudioTrack>.removeListener("OnAudioCueEnd", handleAudioCueEnd);
		}

		public void handleMusicStart()
		{
			DI_Music.playNextRandom();
			isPlayingMusic = true;
		}

		public void handleMusicStop()
		{
			audioSource.Stop();
			//isPlayingMusic = false;
		}

		public void handleMusicChange(DI_AudioTrack track)
		{
			DI_Music.playTrack(track);
		}

		public void Update()
		{
			if (audioSource.isPlaying) {
				float audioTrackTimeLeft = audioSource.clip.length - audioSource.time;
				if (DI_Music.currentlyPlaying.shouldCrossFade) {
					if (!isFading) {
						if (DI_Music.currentlyPlaying.outroTime <= audioTrackTimeLeft) {
							DI_Music.fadeOut(crossfadeSnapshots);
							isFading = true;
							StartCoroutine(fadeTimer(DI_Music.currentlyPlaying.outroTime));
						}
						if (DI_Music.currentlyPlaying.introTime >= audioSource.time) {
							DI_Music.fadeIn(crossfadeSnapshots);
							isFading = true;
							StartCoroutine(fadeTimer(DI_Music.currentlyPlaying.introTime));
						}
					}
				}
			}
			else {
				if (isPlayingMusic) {
					DI_Music.playNextRandom();
					if (DI_Music.currentlyPlaying.shouldCrossFade) {
						StartCoroutine(fadeTimer(DI_Music.currentlyPlaying.introTime));
						isFading = true;
					}
				}
			}
		}

		protected void addNewSource(DI_AudioTrack key)
		{
			AudioSource cueSource = gameObject.AddComponent<AudioSource>();
			cueSource.bypassEffects = audioCueMaster.bypassEffects;
			cueSource.bypassListenerEffects = audioCueMaster.bypassListenerEffects;
			cueSource.bypassReverbZones = audioCueMaster.bypassReverbZones;
			cueSource.dopplerLevel = audioCueMaster.dopplerLevel;
			cueSource.ignoreListenerPause = audioCueMaster.ignoreListenerPause;
			cueSource.ignoreListenerVolume = audioCueMaster.ignoreListenerVolume;
			cueSource.loop = audioCueMaster.loop;
			cueSource.maxDistance = audioCueMaster.maxDistance;
			cueSource.minDistance = audioCueMaster.minDistance;
			cueSource.outputAudioMixerGroup = audioCueMaster.outputAudioMixerGroup;
			cueSource.panStereo = audioCueMaster.panStereo;
			cueSource.pitch = audioCueMaster.pitch;
			cueSource.playOnAwake = audioCueMaster.playOnAwake;
			cueSource.priority = audioCueMaster.priority;
			cueSource.reverbZoneMix = audioCueMaster.reverbZoneMix;
			cueSource.rolloffMode = audioCueMaster.rolloffMode;
			cueSource.spatialBlend = audioCueMaster.spatialBlend;
			cueSource.spatialize = audioCueMaster.spatialize;
			cueSource.spread = audioCueMaster.spread;
			cueSource.volume = audioCueMaster.volume;
			if (key.clip.length > audioSource.time) {
				cueSource.time = audioSource.time;
			}
			cueSources.Add(key, cueSource);
		}

		public void handleAudioCueStart(DI_AudioTrack track)
		{
			if (!cueSources.ContainsKey(track)) {
				addNewSource(track);
			}

			cueSources[track].Stop();
			cueSources[track].clip = track.clip;
			cueSources[track].volume = masterBGMVolume;
			cueSources[track].Play();
		}

		public void handleAudioCueEnd(DI_AudioTrack track)
		{
			if (cueSources.ContainsKey(track)) {
				cueSources[track].Stop();
				cueSources[track].clip = null;
				GameObject.Destroy(cueSources[track]);
				cueSources.Remove(track);
			}
		}

		public IEnumerator fadeTimer(float time)
		{
			yield return new WaitForSeconds(time);
			isFading = false;
		}
	}
}