﻿//// Devils Inc Studios
//// Copyright DEVILS INC. STUDIOS LIMITED 2016
////
//// TODO: Include a description of the file here.
////
//
//using System;
//using UnityEngine;
//using UnityEngine.UI;
//
//using DI.Core.StateMachine;
//using DI.Core.Debug;
//using DI.Core;
//using DI.Core.Input;
//using DI.Core.GameState;
//using DI.Mechanics;
//using DI.SFX;
//using DI.Entities.Properties;
//using DI.Core.Pooling;
//using DI.Entities.Player;
//
//namespace DI.Controllers
//{
//	[RequireComponent(typeof(CharacterController))]
//	public class Locomotion : StateMachine
//	{
//		#region Debug - Display
//		protected void OnGUI()
//		{
//			GUI.Box(new Rect(0, 0f, 300f, 30f), this.GetType().ToString() + " Current State: " + currentStateName);
//		}
//		#endregion
//		#region Setup - Movement Settings
//		public DI_CharacterProperties characterProperties;
//		public PlayerAnimator animator;
//		public DI_ClimbingProperty climbingSettings;
//		private float verticalMovementSpeed = 0.0f;
//		private float horizontalMovementSpeed = 0.0f;
//		private Vector3 moveDirection;
//		private float heading;
//		private float turnSpeed;
//		public float jumpFrame = 0f;
//		public bool isEncumbered = false;
//		public float encumberedPenalty = 0.5f;
//		public bool effectedByGravity = true;
//
//		#endregion
//		#region Setup - Aiming
//		public Camera assignedCamera;
//		public GameObject cameraPivot;
//		public Image aimingTarget;
//		public float maxAimingDistance = 100f;
//		public bool isAiming = false;
//		public float aimingTurnSpeed = 0f;
//		public Vector3 aimingWorldPos;
//		public LayerMask aimingIgnoreLayer;
//
//		#endregion
//		#region Setup - Helpers
//		public bool isGrounded { get { return characterProperties.isGrounded; } }
//		public bool inControl { get { return characterProperties.isInControl; } }
//		public CharacterController rigidBody { get { return characterProperties.controller; } }
//		public float verticalMovement { get { return characterProperties.verticalMovement; } }
//		public float horizontalMovement { get { return characterProperties.horizontalMovement; } }
//		public bool isSneaking { get { return characterProperties.isSneaking; } }
//		public bool isWalking { get { return characterProperties.isWalking; } }
//		public bool isRunning { get { return characterProperties.isRunning; } }
//		public bool isJumping { get { return characterProperties.isJumping; } }
//		public bool canDoubleJumped { get { return characterProperties.jumpSettings.canDoubleJump; } }
//		public float jumpFrames { get { return characterProperties.jumpSettings.jumpFrames; } }
//		public bool hasDoubleJumped {
//			get { return characterProperties.jumpSettings.hasDoubleJumped; }
//			set { characterProperties.jumpSettings.hasDoubleJumped = value; }
//		}
//		public bool canJump {
//			get { return characterProperties.jumpSettings.canJump; }
//			set { characterProperties.jumpSettings.canJump = value; }
//		}
//		public bool canMove {
//			get { return characterProperties.canMove(); }
//			set { characterProperties.canMove(value); }
//		}
//		public bool encumbered {
//			get { return isEncumbered; }
//			set { isEncumbered = value; }
//		}
//		public int playerId { get { return characterProperties.getPlayerId(); } }
//		[HideInInspector]
//		public bool initalized = false;
//		#endregion
//		#region Setup - Moving Platform
//		[Header("Moving Platform Settings")]
//		public string platformTag = "Moving Platform";
//		public Rigidbody platformRigidBody;
//		private float lastTouch = 0.0f;
//		#endregion
//		#region Setup - Jumping
//		public bool inJump = false;
//		public float maxFallSpeed = 4f;
//		#endregion
//		#region Setup - Footsteps
//		protected Vector3 leftFootPos;
//		protected Vector3 rightFootPos;
//		protected bool playedLeftFoot;
//		protected bool playedRightFoot;
//		public bool leaveFootPrints = false;
//		public GameObject leftFootPrintDecal;
//		public GameObject rightFootPrintDecal;
//		public Vector3 lastFootPrintPosLeft;
//		public Vector3 lastFootPrintPosRight;
//
//		#endregion
//		#region Setup - Input Settings
//		protected int sneakKeyIndex = 0;
//		protected int horizontalKeyIndex = 0;
//		protected int verticalKeyIndex = 0;
//		protected int jumpKeyIndex = 0;
//		protected int runKeyIndex = 0;
//		#endregion
//		#region Control
//		public void disablePlayerControl()
//		{
//			characterProperties.isInControl = false;
//		}
//		public void enablePlayerControl()
//		{
//			characterProperties.isInControl = true;
//		}
//		#endregion
//
//		#region Animation
//		public void aim()
//		{
//			// TODO Controller support
//			Vector3 aimingPosition = Vector3.zero;
//			// Set the aiming position = to the player's mouse.
//			if (DI_BindManager.getControllerType(characterProperties.getPlayerId()) == DI_ControllerType.KEYBOARD) {
//				aimingPosition = assignedCamera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 3));
//				aimingTarget.rectTransform.anchoredPosition = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
//				// If the player's mouse is in either the left 1/4 of the screen or the right 1/4 of the screen, rotate the player.
//				if (Input.mousePosition.x < (Screen.width/4)) {
//					aimingTurnSpeed = -1f;
//				}
//				else if (Input.mousePosition.x > ((Screen.width/2) + (Screen.width/4))) {
//					aimingTurnSpeed = 1f;
//				}
//				else {
//					aimingTurnSpeed = 0f;
//				}
//			}
//			animator.setLookAtPosition(aimingPosition);
//			RaycastHit hit;
//			Ray ray = assignedCamera.ScreenPointToRay(Input.mousePosition);
//			Debug.DrawRay(ray.origin, ray.direction, Color.red, 0.2f, true);
//			if (Physics.Raycast(ray, out hit, maxAimingDistance, aimingIgnoreLayer)) {
//				Debug.DrawLine(transform.position, hit.point, Color.yellow);
//				aimingWorldPos = hit.point;
//			}
//			else {
//				aimingWorldPos = transform.position + ray.direction * 5f;
//			}
//		}
//		#endregion
//		#region Footsteps
//		public void playFootStep()
//		{
//			if (characterProperties.playMovementSounds) {
//				if (currentStateName == "walking") {
//					if (characterProperties.footstepsWalking.hasSFX) {
//						DI_SFX.playClipAtPoint(gameObject, transform.position, characterProperties.footstepsWalking.sfxs[UnityEngine.Random.Range(0, characterProperties.footstepsWalking.sfxs.Count)]);
//					}
//				}
//				else if (currentStateName == "running") {
//					if (characterProperties.footstepsRunning.hasSFX) {
//						DI_SFX.playClipAtPoint(gameObject, transform.position, characterProperties.footstepsRunning.sfxs[UnityEngine.Random.Range(0, characterProperties.footstepsRunning.sfxs.Count)]);
//					}
//				}
//			}
//		}
//
//		public void footPrints()
//		{
//			// Footsteps / prints
//			if (isGrounded) {
//				if (platformRigidBody == null) {
//					if (!playedLeftFoot) {
//						leftFootPos = animator.getBoneTransform(HumanBodyBones.LeftFoot).position;
//						 if (Vector3.Distance (leftFootPos, characterProperties.getAnimator().pivotPosition) >= characterProperties.strideLength) {
//							if (Vector3.Distance(transform.position, lastFootPrintPosLeft) >= characterProperties.strideLength) {
//								playFootStep();
//								playedLeftFoot = true;
//								playedRightFoot = false;
//								if (leaveFootPrints) {
//									GameObject footPrint = DI_PoolManager.getPooledObject(leftFootPrintDecal);
//									footPrint.transform.position = new Vector3(leftFootPos.x, transform.position.y, leftFootPos.z);
//									footPrint.transform.rotation = transform.rotation;
//									footPrint.SetActive(true);
//								}
//								lastFootPrintPosLeft = new Vector3(leftFootPos.x, transform.position.y, leftFootPos.z); 
//							}
//						}
//					}
//					if (!playedRightFoot) {
//						if (Vector3.Distance (rightFootPos, characterProperties.getAnimator().pivotPosition) >= characterProperties.strideLength) {
//							rightFootPos = animator.getBoneTransform(HumanBodyBones.RightFoot).position;
//							if (Vector3.Distance(transform.position, lastFootPrintPosRight) >= characterProperties.strideLength) {
//								playFootStep();
//								playedRightFoot = true;
//								playedLeftFoot = false;
//								if (leaveFootPrints) {
//									GameObject footPrint = DI_PoolManager.getPooledObject(rightFootPrintDecal);
//									footPrint.transform.position = new Vector3(rightFootPos.x, transform.position.y, rightFootPos.z);
//									footPrint.transform.rotation = transform.rotation;
//									footPrint.SetActive(true);
//								}
//								lastFootPrintPosRight = new Vector3(rightFootPos.x, transform.position.y, rightFootPos.z);
//							}
//						}
//					}
//				}
//			}
//		}
//
//		#endregion
//		#region Input
//		public void updateKeyCache()
//		{
//			sneakKeyIndex = DI_BindManager.getKeyIndex("Sneak", characterProperties.getPlayerId());
//			horizontalKeyIndex = DI_BindManager.getKeyIndex("Horizontal", characterProperties.getPlayerId());
//			verticalKeyIndex = DI_BindManager.getKeyIndex("Vertical", characterProperties.getPlayerId());
//			jumpKeyIndex = DI_BindManager.getKeyIndex("Jump", characterProperties.getPlayerId());
//			runKeyIndex = DI_BindManager.getKeyIndex("Run", characterProperties.getPlayerId());
//			initalized = true;
//		}
//
//		public void updateInput()
//		{
//
//			characterProperties.resetInput();
//			// Update the player's input values.
//			switch (DI_BindManager.getKeyState(verticalKeyIndex, characterProperties.getPlayerId())) {
//			case DI_KeyState.AXIS_HELD_POSITIVE:
//				characterProperties.verticalMovement = DI_BindManager.getAxisValue(verticalKeyIndex, characterProperties.getPlayerId());
//				break;
//			case DI_KeyState.AXIS_PRESSED_POSITIVE:
//				characterProperties.verticalMovement = DI_BindManager.getAxisValue(verticalKeyIndex, characterProperties.getPlayerId());
//				break;
//			case DI_KeyState.AXIS_NOT_PRESSED:
//				characterProperties.verticalMovement = 0.0f;
//				break;
//			case DI_KeyState.AXIS_HELD_NEGATIVE:
//				characterProperties.verticalMovement = DI_BindManager.getAxisValue(verticalKeyIndex, characterProperties.getPlayerId());
//				break;
//			case DI_KeyState.AXIS_PRESSED_NEGATIVE:
//				characterProperties.verticalMovement = DI_BindManager.getAxisValue(verticalKeyIndex, characterProperties.getPlayerId());
//				break;
//			case DI_KeyState.KEY_HELD:
//				characterProperties.verticalMovement = 1.0f;
//				break;
//			case DI_KeyState.KEY_NOT_PRESSED:
//				characterProperties.verticalMovement = 0.0f;
//				break;
//			default:
//				characterProperties.verticalMovement = 0.0f;
//				break;
//			}
//
//			switch (DI_BindManager.getKeyState(horizontalKeyIndex, characterProperties.getPlayerId())) {
//			case DI_KeyState.AXIS_HELD_POSITIVE:
//				characterProperties.horizontalMovement = DI_BindManager.getAxisValue(horizontalKeyIndex, characterProperties.getPlayerId());
//				break;
//			case DI_KeyState.AXIS_PRESSED_POSITIVE:
//				characterProperties.horizontalMovement = DI_BindManager.getAxisValue(horizontalKeyIndex, characterProperties.getPlayerId());
//				break;
//			case DI_KeyState.AXIS_NOT_PRESSED:
//				characterProperties.horizontalMovement = 0.0f;
//				break;
//			case DI_KeyState.AXIS_HELD_NEGATIVE:
//				characterProperties.horizontalMovement = DI_BindManager.getAxisValue(horizontalKeyIndex, characterProperties.getPlayerId());
//				break;
//			case DI_KeyState.AXIS_PRESSED_NEGATIVE:
//				characterProperties.horizontalMovement = DI_BindManager.getAxisValue(horizontalKeyIndex, characterProperties.getPlayerId());
//				break;
//			case DI_KeyState.KEY_HELD:
//				characterProperties.horizontalMovement = 1.0f;
//				break;
//			case DI_KeyState.KEY_NOT_PRESSED:
//				characterProperties.horizontalMovement = 0.0f;
//				break;
//			default:
//				characterProperties.horizontalMovement = 0.0f;
//				break;
//			}
//
//			if (characterProperties.jumpSettings.canJump) {
//				switch (DI_BindManager.getKeyState(jumpKeyIndex, characterProperties.getPlayerId())) {
//				case DI_KeyState.AXIS_PRESSED_POSITIVE:
//					characterProperties.isJumping = true;
//					break;
//				case DI_KeyState.AXIS_PRESSED_NEGATIVE:
//					characterProperties.isJumping = true;
//					break;
//				case DI_KeyState.KEY_PRESSED:
//					characterProperties.isJumping = true;
//					break;
//				default:
//					characterProperties.isJumping = false;
//					break;
//				}
//			}
//
//			switch (DI_BindManager.getKeyState(sneakKeyIndex, characterProperties.getPlayerId())) {
//			case DI_KeyState.AXIS_HELD_POSITIVE:
//				characterProperties.isSneaking = true;
//				break;
//			case DI_KeyState.KEY_HELD:
//				characterProperties.isSneaking = true;
//				break;
//			default: 
//				characterProperties.isSneaking = false;
//				break;
//			}
//				
//			switch (DI_BindManager.getKeyState(runKeyIndex, characterProperties.getPlayerId())) {
//			case DI_KeyState.AXIS_HELD_POSITIVE:
//				characterProperties.isRunning = true;
//				break;
//			case DI_KeyState.KEY_HELD:
//				characterProperties.isRunning = true;
//				break;
//			default:
//				characterProperties.isRunning = false;
//				break;
//			}
//
//			if (horizontalMovement != 0.0f || verticalMovement != 0.0f) {
//				if (!characterProperties.isSneaking && !characterProperties.isRunning) {
//					characterProperties.isWalking = true;
//				}
//			}
//		}
//		#endregion
//		#region Movement Speed
//		// TODO add fall off / slide to the movement so you don't stop on a dime.
//		public Vector2 getSneakingSpeed()
//		{
//			float vertSpeed = Mathf.Clamp(
//				characterProperties.verticalMovement * characterProperties.sneakSpeedSettings.maxMovementSpeed,
//				-1 * characterProperties.sneakSpeedSettings.maxMovementSpeed, 
//				characterProperties.sneakSpeedSettings.maxMovementSpeed
//			);
//			float horizSpeed = Mathf.Clamp(
//				characterProperties.horizontalMovement * characterProperties.sneakSpeedSettings.maxMovementSpeed,
//				-1 * characterProperties.sneakSpeedSettings.maxMovementSpeed, 
//				characterProperties.sneakSpeedSettings.maxMovementSpeed
//			);
//
//			if (isEncumbered) {
//				return (new Vector2(vertSpeed, horizSpeed) * encumberedPenalty);
//			}
//			else {
//				return new Vector2(vertSpeed, horizSpeed);
//			}
//		}
//
//		public Vector2 getWalkingSpeed()
//		{
//			float vertSpeed = Mathf.Clamp(
//				characterProperties.verticalMovement * characterProperties.getMovementSpeed(),
//				-1 * characterProperties.getMaxMovementSpeed(), 
//				characterProperties.getMaxMovementSpeed()
//			);
//			float horizSpeed = Mathf.Clamp(
//				characterProperties.horizontalMovement * characterProperties.getMaxMovementSpeed(),
//				-1 * characterProperties.getMaxMovementSpeed(), 
//				characterProperties.getMaxMovementSpeed()
//			);
//
//			if (isEncumbered) {
//				return (new Vector2(vertSpeed, horizSpeed) * encumberedPenalty);
//			}
//			else {
//				return new Vector2(vertSpeed, horizSpeed);
//			}
//		}
//
//		public Vector2 getRunningSpeed()
//		{
//			float vertSpeed = Mathf.Clamp(
//				characterProperties.verticalMovement * characterProperties.runSpeedSettings.maxMovementSpeed,
//				-1 * characterProperties.runSpeedSettings.maxMovementSpeed,
//				characterProperties.runSpeedSettings.maxMovementSpeed
//			);
//			float horizSpeed = Mathf.Clamp(
//				characterProperties.horizontalMovement * characterProperties.runSpeedSettings.maxMovementSpeed,
//				-1 * characterProperties.runSpeedSettings.maxMovementSpeed,
//				characterProperties.runSpeedSettings.maxMovementSpeed
//			);
//			if (isEncumbered) {
//				return (new Vector2(vertSpeed, horizSpeed) * encumberedPenalty);
//			}
//			else {
//				return new Vector2(vertSpeed, horizSpeed);
//			}
//		}
//
//		public Vector2 getSprintingSpeed()
//		{
//			float vertSpeed = Mathf.Clamp(
//				characterProperties.verticalMovement * characterProperties.runSpeedSettings.maxMovementSpeed,
//				-1 * characterProperties.runSpeedSettings.maxMovementSpeed,
//				characterProperties.runSpeedSettings.maxMovementSpeed
//			);
//			float horizSpeed = Mathf.Clamp(
//				characterProperties.horizontalMovement * characterProperties.sprintSpeedSettings.maxMovementSpeed,
//				-1 * characterProperties.sprintSpeedSettings.maxMovementSpeed,
//				characterProperties.sprintSpeedSettings.maxMovementSpeed
//			);
//			if (isEncumbered) {
//				return (new Vector2(vertSpeed, horizSpeed) * encumberedPenalty);
//			}
//			else {
//				return new Vector2(vertSpeed, horizSpeed);
//			}
//		}
//
//		public void updateMovementSpeed()
//		{
//			Vector2 movementSpeed;
//			if (characterProperties.isSneaking) {
//				movementSpeed = getSneakingSpeed();
//			}
//			else if (characterProperties.isWalking) {
//				movementSpeed = getWalkingSpeed();
//			}
//			else if (characterProperties.isRunning) {
//				movementSpeed = getRunningSpeed();
//			}
//			else {
//				verticalMovementSpeed = 0f;
//				horizontalMovementSpeed = 0f;
//				return;
//			}
//
//			verticalMovementSpeed = movementSpeed.x;
//			horizontalMovementSpeed = movementSpeed.y;
//
//			animator.speed = verticalMovementSpeed;
//			animator.direction = characterProperties.horizontalMovement;
//		}
//		#endregion
//		#region Stamina Drain/Regain
//		public void regenStamina()
//		{
//			// Regen some stamina for using sprint if we don't have unlimited.
//			float staminaRegenRate = characterProperties.staminaSettings.staminaRegenPerTick;
//			switch (currentStateName) {
//			case "idling":
//				staminaRegenRate *= 4;
//				break;
//			case
//				"walking":
//				staminaRegenRate *= 2;
//				break;
//			}
//
//			if (!characterProperties.staminaSettings.hasUnlimitedStamina) {
//				characterProperties.staminaSettings.currentStamina = Mathf.Clamp(
//					characterProperties.staminaSettings.currentStamina + characterProperties.staminaSettings.staminaRegenPerTick * DI_Time.getTimeDelta(),
//					0f,
//					characterProperties.staminaSettings.maxStamina
//				);
//				characterProperties.staminaSettings.currentStamina = Mathf.Round(characterProperties.staminaSettings.currentStamina * 100f) / 100f;
//			}
//		}
//
//		public void drainStamina()
//		{
//			// Drain some stamina for using sprint if we don't have unlimited.
//			if (!characterProperties.staminaSettings.hasUnlimitedStamina) {
//				characterProperties.staminaSettings.currentStamina = Mathf.Clamp(
//					characterProperties.staminaSettings.currentStamina - characterProperties.staminaSettings.staminaDrainPerTick * DI_Time.getTimeDelta(), 
//					0f, 
//					characterProperties.staminaSettings.maxStamina
//				);
//				characterProperties.staminaSettings.currentStamina = Mathf.Round(characterProperties.staminaSettings.currentStamina * 100f) / 100f;
//			}
//		}
//		#endregion
//		#region Movement
//		public void movePlayer()
//		{
//			movePlayer(verticalMovementSpeed, horizontalMovementSpeed);
//		}
//		public void movePlayer(Vector2 speed)
//		{
//			movePlayer(speed.x, speed.y);
//		}
//		public void movePlayer(float verticalSpeed, float horizontalSpeed)
//		{
//			if (characterProperties.isInControl) {
//				if (currentStateName == "climbing") {
//				}
//				else if (currentStateName == "walking_ledge") {
//				}
//				else {
//					// Character Moving
//					Vector3 directionVector = new Vector3(0, 0, verticalSpeed);
//					moveDirection = transform.rotation * directionVector;
//					moveDirection.y -= maxFallSpeed * Time.deltaTime;
//					// Add vertical movement for jumping.
//					if (inJump) {
//						moveDirection.y += characterProperties.jumpSettings.jumpStrength + maxFallSpeed;
//					}
//
//					// Apply Gravity
//					if (effectedByGravity) {
//						moveDirection.y -= maxFallSpeed;
//					}
//
//					// Moving Platform
//					if (platformRigidBody != null) {
//						moveDirection += platformRigidBody.velocity;
//					}
//
//					// Character Turning
//					heading = Mathf.Atan2(horizontalMovement,verticalMovement);
//					// Check if the player is actually trying to turn.
//					// Adjust the speed of the turn based on the position of the joystick.
//					if (Mathf.Abs(horizontalMovement) >= 0.1f) {
//						turnSpeed = characterProperties.getMaxTurnSpeed();
//						// Joystick is in the front half of the stick
//						// Don't adjust the turning.
//						if (heading <= 2.0f && heading >= -2.0f) {
//						}
//
//						// Joystick is in the back half of the stick
//						// Slow down and negate the turn speed, as the player will be moving backwards.
//						else if (heading > 2.0f | heading < -2.0f) {
//							turnSpeed /= 4.0f;
//							heading *= -1f;
//						}
//
//						// The player is likely just trying to turn the character in place.
//						// Cut the turn speed to help out with precision.
//						if (Mathf.Abs(verticalMovement) <= 0.1f) {
//							turnSpeed /= 2.0f;
//						}
//					}
//					else {
//						turnSpeed = 0f;
//					}
//				}
//			}
//		}
//
//		private void applyMovement()
//		{
//			if (inControl) {
//				characterProperties.controller.Move(moveDirection * DI_Time.getTimeDelta());
//			}
//			if (isAiming) {
//				this.transform.Rotate(0, (heading * turnSpeed) + (aimingTurnSpeed * characterProperties.getMaxTurnSpeed()/2), 0);
//			}
//			else {
//				this.transform.Rotate(0, heading * (turnSpeed), 0);
//			}
//		}
//
//		#endregion
//		#region Triggers - Moving Platform
//		// Moving Platforms - Check to see if we are touching a platform, if we are then assign it as the active platform.
//		public void OnTriggerEnter(Collider other)
//		{
//			// Handle moving platforms.
//			if (other.transform.tag == platformTag) {
//				platformRigidBody = other.gameObject.GetComponent<Rigidbody>();
//			}
//		}
//		public void OnTriggerStay(Collider other)
//		{
//			if (characterProperties.isGrounded) {
//				if (other.transform.tag == platformTag) {
//					// lastTouch helps to prevent the jumpyness inherit in character controller physics.
//					lastTouch = 0.0f;
//				}
//			}
//		}
//		public void OnTriggerExit(Collider other)
//		{
//			// Handle moving platforms.
//			if (lastTouch > 0.2f) {
//				if (other.transform.tag == platformTag) {
//					platformRigidBody = null;
//				}
//			}
//		}
//		#endregion
//		#region Updates
//		protected new void EarlyGlobalUpdate()
//		{
//			if (currentStateName != "sprinting") {
//				regenStamina();
//			}
//			else {
//				drainStamina();
//			}
//			updateInput();
//		}
//
//		public new void LateGlobalUpdate()
//		{
//			updateMovementSpeed();
//			movePlayer();
//		}
//		#endregion
//		// State Definitions
//		#region Idling
//		protected void idling_Enter(Enum previous)
//		{
//			DI_Debug.writeLog(DI_DebugLevel.INFO, "Enter " + "idling");
//			animator.changeState(PlayerAnimationState.idling);
//		}
//
//		protected void idling_Update()
//		{
//			//DI_Debug.writeLog(DI_DebugLevel.INFO, "Update " + "idling");
//			// We can only move to one state, tis must be enforced.
//			if (inControl) {
//				if (!isGrounded) {
//					ChangeCurrentState(LocomotionState.falling, false);
//				}
//				else if (isJumping) {
//					ChangeCurrentState(LocomotionState.jumping, false);
//				}
//				else if (isSneaking) {
//					ChangeCurrentState(LocomotionState.sneaking, false);
//				}
//				else if (isWalking) {
//					ChangeCurrentState(LocomotionState.walking, false);
//				}
//				else if (isRunning) {
//					ChangeCurrentState(LocomotionState.running, false);
//				}
//			}
//		}
//
//		#endregion Idling
//		#region Sneaking
//		protected void sneaking_Enter(Enum previous)
//		{
//			DI_Debug.writeLog(DI_DebugLevel.INFO, "Enter " + "sneaking");
//			animator.changeState(PlayerAnimationState.sneaking);
//		}
//
//		protected void sneaking_Update()
//		{
//			if (isEncumbered) {
//				Debug.Log("Change to Walking");
//				ChangeCurrentState(LocomotionState.walking, false);
//				return;
//			}
//
//			if (!inControl) {
//				ChangeCurrentState(LocomotionState.idling, false);
//				return;
//			}
//			else if (!isGrounded) {
//				ChangeCurrentState(LocomotionState.falling, false);
//				return;
//			}
//			else if (isRunning) {
//				ChangeCurrentState(LocomotionState.running, false);
//				return;
//			}
//			else if (isWalking) {
//				ChangeCurrentState(LocomotionState.walking, false);
//				return;
//			}
//			else if (!isSneaking) {
//				ChangeCurrentState(LocomotionState.idling, false);
//				return;
//			}
//			else if (isJumping) {
//				ChangeCurrentState(LocomotionState.jumping, false);
//				return;
//			}
//		}
//		#endregion Sneaking
//		#region Walking
//		protected void walking_Enter(Enum previous)
//		{
//			DI_Debug.writeLog(DI_DebugLevel.INFO, "Enter " + "walking");
//			animator.changeState(PlayerAnimationState.walking);
//			playedLeftFoot = false;
//		}
//
//		protected void walking_Update()
//		{
//			if (!inControl) {
//				ChangeCurrentState(LocomotionState.idling, false);
//				return;
//			}
//			else if (!isGrounded) {
//				ChangeCurrentState(LocomotionState.falling, false);
//				return;
//			}
//			// If we are encumbered, than walking is the only available state.
//			else if (!isEncumbered) {
//				if (isSneaking) {
//					ChangeCurrentState(LocomotionState.sneaking, false);
//					return;
//				}
//				else if (isRunning) {
//					ChangeCurrentState(LocomotionState.running, false);
//					return;
//				}
//				else if (isJumping) {
//					ChangeCurrentState(LocomotionState.jumping, false);
//					return;
//				}
//				else if (!isWalking) {
//					ChangeCurrentState(LocomotionState.idling, true);
//					return;
//				}
//			}
//			else if (!isWalking) {
//				ChangeCurrentState(LocomotionState.idling, true);
//				return;
//			}
//
//			footPrints();
//		}
//		#endregion Walking
//		#region Running
//		protected void running_Enter(Enum previous)
//		{
//			DI_Debug.writeLog(DI_DebugLevel.INFO, "Enter " + "running");
//			animator.changeState(PlayerAnimationState.running);
//			playedLeftFoot = false;
//		}
//
//		protected void running_Update()
//		{
//			if (!inControl) {
//				ChangeCurrentState(LocomotionState.idling, false);
//				return;
//			}
//			else if (!isGrounded) {
//				ChangeCurrentState(LocomotionState.falling, false);
//				return;
//			}
//			else if (isEncumbered) {
//				ChangeCurrentState(LocomotionState.walking, false);
//				return;
//			}
//			else if (isWalking) {
//				ChangeCurrentState(LocomotionState.walking, false);
//				return;
//			}
//			else if (isSneaking) {
//				ChangeCurrentState(LocomotionState.sneaking, false);
//				return;
//			}
//			else if (isJumping) {
//				ChangeCurrentState(LocomotionState.jumping, false);
//				return;
//			}
//			else if (!isRunning) {
//				ChangeCurrentState(LocomotionState.idling, false);
//				return;
//			}
//
//			footPrints();
//		}
//
//		#endregion Running
//		#region Jumping
//		protected void jumping_Enter(Enum previous)
//		{
//			animator.changeState(PlayerAnimationState.jumping);
//
//			inJump = true;
//			jumpFrame = 0f;
//		}
//
//		protected void jumping_Update()
//		{
//			if (jumpFrame < jumpFrames) {
//				jumpFrame += DI_Time.getTimeDelta();
//			}
//			else {
//				ChangeCurrentState(LocomotionState.falling, false);
//				return;
//			}
//
//			if (characterProperties.jumpSettings.canDoubleJump) {
//				if (isJumping) {
//					ChangeCurrentState(LocomotionState.double_jump, false);
//					return;
//				}
//			}
//		}
//		#endregion
//		#region Double Jump
//		protected void double_jump_Enter(Enum previous)
//		{
//			animator.changeState(PlayerAnimationState.double_jump);
//			inJump = true;
//			jumpFrame = 0f;
//			hasDoubleJumped = true;
//		}
//
//		protected void double_jump_Update()
//		{
//			if (jumpFrame < jumpFrames) {
//				jumpFrame += DI_Time.getTimeDelta();
//			}
//			else {
//				ChangeCurrentState(LocomotionState.falling, false);
//			}
//		}
//		#endregion
//		#region Falling
//		protected void falling_Enter(Enum previous)
//		{
//			inJump = false;
//			jumpFrame = 0f;
//			animator.changeState(PlayerAnimationState.falling);
//		}
//
//		protected void falling_Exit(Enum previous)
//		{
//			characterProperties.jumpSettings.canJump = true;
//			hasDoubleJumped = false;
//			footPrints();
//			footPrints();
//		}
//
//		protected void falling_Update()
//		{
//			if (isGrounded) {
//				ChangeCurrentState(LocomotionState.idling, false);
//			}
//			else {
//				if (characterProperties.jumpSettings.canDoubleJump) {
//					if (!hasDoubleJumped && isJumping) {
//						ChangeCurrentState(LocomotionState.double_jump, false);
//					}
//				}
//
//			}
//		}
//		#endregion
//		#region Climbing
//		protected void climbing_Enter(Enum previous)
//		{
//		}
//		protected void climbing_Update()
//		{
//			if (!climbingSettings.isClimbing) {
//				ChangeCurrentState(PlayerState.idling, false);
//			}
//		}
//		protected void climbing_Exit(Enum previous)
//		{
//		}
//		#endregion
//		#region Sleeping
//		protected void sleeping_Enter(Enum previous)
//		{
//			animator.changeState(PlayerAnimationState.sleeping);
//		}
//		#endregion
//
//		#region Awake / Define States
//		protected void Awake() 
//		{
//			updateKeyCache();
//			InitializeStateMachine<LocomotionState>(LocomotionState.idling, true);
//
//			#region Transistions Idling
//			// From idle the player can do what?
//			/*
//			 * Sneak
//			 * Jump
//			 * Fall
//			 * Run
//			 */
//			AddTransitionsToState(
//				LocomotionState.idling,
//				new Enum[] {
//					LocomotionState.walking,
//					LocomotionState.sneaking,
//					LocomotionState.running,
//					LocomotionState.sprinting,
//					LocomotionState.jumping,
//					LocomotionState.falling,
//					LocomotionState.climbing_ladder,
//					LocomotionState.sleeping,
//				}
//			);
//			#endregion
//			#region Transistions Sneaking
//			// From walk the player can do what?
//			/*
//			 * Run
//			 * Idle
//			 * Jump
//			 * Fall
//			*/
//
//			AddTransitionsToState(
//				LocomotionState.sneaking,
//				new Enum[] {
//					LocomotionState.walking,
//					LocomotionState.running,
//					LocomotionState.idling,
//					LocomotionState.jumping,
//					LocomotionState.falling,
//					LocomotionState.climbing_ladder,
//					LocomotionState.sleeping,
//				}
//			);
//			#endregion
//			#region Transistions Walking
//			// From walk the player can do what?
//			/*
//			 * Run
//			 * Idle
//			 * Jump
//			 * Fall
//			*/
//
//			AddTransitionsToState(
//				LocomotionState.walking,
//				new Enum[] {
//					LocomotionState.sneaking,
//					LocomotionState.running,
//					LocomotionState.idling,
//					LocomotionState.jumping,
//					LocomotionState.falling,
//					LocomotionState.climbing_ladder,
//					LocomotionState.sleeping,
//				}
//			);
//			#endregion
//			#region Transistions Running
//			// From Run the player can do what?
//			/*
//			 * Sneak
//			 * Run
//			 * Jump
//			 * Fall
//			*/
//
//			AddTransitionsToState(
//				LocomotionState.running,
//				new Enum[] {
//					LocomotionState.walking,
//					LocomotionState.idling,
//					LocomotionState.sprinting,
//					LocomotionState.jumping,
//					LocomotionState.falling,
//					LocomotionState.climbing_ladder,
//					LocomotionState.sleeping,
//				}
//			);
//			#endregion
//			#region Transistions Runing
//			// From Run the player can do what?
//			/*
//			 * Run
//			 * Jump
//			 * Fall
//			 * Idle
//			*/
//
//			AddTransitionsToState(
//				LocomotionState.sprinting,
//				new Enum[] {
//					LocomotionState.running,
//					LocomotionState.idling,
//					LocomotionState.jumping,
//					LocomotionState.falling,
//					LocomotionState.sleeping,
//				}
//			);
//			#endregion
//			#region Transistions Jumping
//			// From Jumping the player can do what?
//			/*
//			 * Fall
//			*/
//
//			AddTransitionsToState(
//				LocomotionState.jumping,
//				new Enum[] {
//					LocomotionState.falling,
//					LocomotionState.double_jump,
//				}
//			);
//			#endregion
//			#region Transistions Double Jump
//			// From Double Jump the player can do what?
//			/*
//			 * Fall
//			*/
//
//			AddTransitionsToState(
//				LocomotionState.double_jump,
//				new Enum[] {
//					LocomotionState.falling,
//				}
//			);
//			#endregion
//			#region Transistions Falling
//			// From Fall the player can do what?
//			/*
//			 * Idle
//			 * Double Jump
//			*/
//
//			AddTransitionsToState(
//				LocomotionState.falling,
//				new Enum[] {
//					LocomotionState.idling,
//					LocomotionState.double_jump,
//				}
//			);
//			#endregion
//			#region Transistions Climbing Ladder
//			AddTransitionsToState(
//				LocomotionState.climbing_ladder,
//				new Enum[] {
//					LocomotionState.idling,
//					LocomotionState.sleeping,
//				}
//			);
//			#endregion
//			#region Transistions Sneaking Ledge
//			AddTransitionsToState(
//				LocomotionState.walking_ledge,
//				new Enum[] {
//					LocomotionState.idling,
//					LocomotionState.sleeping,
//				}
//			);
//			#endregion
//			#region Transistions Sleeping
//			AddTransitionsToState(
//				LocomotionState.sleeping,
//				new Enum[] {
//					LocomotionState.idling,
//				}
//			);
//			#endregion
//		}
//		#endregion
//		#region Enable/Update/Fixed Update/On Draw Gizmos Methods
//		public void OnEnable()
//		{
//			updateKeyCache();
//			characterProperties.setColliderOffsets(characterProperties.controller.center, characterProperties.controller.height);
//		}
//
//		new public void Update()
//		{
//			EarlyGlobalUpdate();
//			if (lastTouch > 0.2f) {
//				platformRigidBody = null;
//				lastTouch = 0.0f;
//			}
//			lastTouch += Time.deltaTime;
//
//			base.Update();
//			LateGlobalUpdate();
//		}
//
//		new public void FixedUpdate()
//		{
//			RaycastHit hit;
//			if (Physics.Raycast(transform.position, Vector3.down, out hit)) {
//				characterProperties.distanceFromGround = hit.distance;
//				if (hit.distance >= characterProperties.groundFudge) {
//					if (characterProperties.isGrounded != false) {
//						characterProperties.isGrounded = false;
//					}
//				}
//				else {
//					if (characterProperties.isGrounded != true) {
//						characterProperties.isGrounded = true;
//					}
//				}
//			}
//			applyMovement();
//		}
//
//		public void OnDrawGizmos()
//		{
//			Color color;
//			if (characterProperties.isGrounded) {
//				color = Color.cyan;
//			}
//			else {
//				color = Color.yellow;
//			}
//
//			Gizmos.color = color;
//			Gizmos.DrawWireSphere(transform.position, characterProperties.controller.radius);
//		}
//		#endregion
//
//
//		public void requestStateChange(LocomotionState state)
//		{
//			ChangeCurrentState(state, false);
//		}
//	}
//}