// /*
// *
// * 	Devils Inc Studios
// * 	How Long
// * 	Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
// *	
// *	This class controlls how doors function.
// *
// */

using UnityEngine;
using DI.Core.Events;
using DG.Tweening;
using DI.Core.StateMachine;
using System;

namespace DI.Entities.Door
{
	[AddComponentMenu("Interactions/Doors/Door Controller")]
	public class DoorController : StateMachine
	{
		public Vector3 openPosition;
		public Quaternion openRotation;
		public Vector3 closedPosition;
		public Quaternion closedRotation;
		public float movementTime = 0.01f;
		public AudioSource openingSound;
		public AudioSource closingSound;
		public bool isOpen = false;
		[Tooltip("Sliding doors move (Transform.postion) while non-sliding rotate (Transform.Rotation)")]
		public bool isSliding = false;

		// Mechanim helper object
		public GameObject doorHandle;

		[System.NonSerialized]
		public bool isOpening;
		[System.NonSerialized]
		public bool isClosing;
		private Tweener doorTweener;

		#region Setup
		protected void Awake() 
		{
			if (isOpen) {
				InitializeStateMachine<DoorState>(DoorState.open, true);
			}
			else {
				InitializeStateMachine<DoorState>(DoorState.closed, true);
			}

			#region Transistions open
			AddTransitionsToState(
				DoorState.open,
				new Enum[] {
					DoorState.closing,
				}
			);
			#endregion
			#region Transistions opening
			AddTransitionsToState(
				DoorState.opening,
				new Enum[] {
					DoorState.open,
				}
			);
			#endregion
			#region Transistions closed
			AddTransitionsToState(
				DoorState.closed,
				new Enum[] {
					DoorState.opening,
				}
			);
			#endregion
			#region Transistions closing
			AddTransitionsToState(
				DoorState.closing,
				new Enum[] {
					DoorState.closed,
				}
			);
			#endregion
		}
		#endregion

		public void Start() {
			DOTween.Init(false, true, LogBehaviour.ErrorsOnly);
		}

		// State Definitions
		#region Open
		protected void open_Enter(Enum previous)
		{
			isOpen = true;
			isOpening = false;
		}
		#endregion

		#region Opening
		protected void opening_Enter(Enum previous)
		{
			isOpening = true;
			if (!isSliding) {
				doorTweener = transform.DOLocalRotateQuaternion(openRotation, movementTime);
			}
			else {
				doorTweener = transform.DOLocalMove(openPosition, movementTime, false);
			}
			doorTweener.Play();
		}

		protected void opening_Update()
		{
			if (!doorTweener.IsPlaying()) {
				ChangeCurrentState(DoorState.open, false);
			}
		}
		#endregion

		#region Closing
		protected void closing_Enter(Enum previous)
		{
			isClosing = true;
			if (!isSliding) {
				doorTweener = transform.DOLocalRotateQuaternion(closedRotation, movementTime);
			}
			else {
				doorTweener = transform.DOLocalMove(closedPosition, movementTime, false);
			}
			doorTweener.Play();
		}

		protected void closing_Update()
		{
			if (!doorTweener.IsPlaying()) {
				ChangeCurrentState(DoorState.closed, false);
			}
		}
		#endregion

		#region Close
		protected void closed_Enter(Enum previous)
		{
			isOpen = false;
			isClosing = false;
		}
		#endregion

		public void toggleDoor()
		{
			if (!isOpening && !isClosing) {
				if (isOpen) {
					closeDoor();
				}
				else {
					openDoor();
				}
			}
		}

		public void openDoor()
		{
			if (currentStateName == "closed") {
				ChangeCurrentState(DoorState.opening, false);
				DI_EventCenter<GameObject>.invoke("OnDoorOpen", this.gameObject);
				if (!openingSound.isPlaying) {
					openingSound.Play();
				}
			}
		}

		public void closeDoor()
		{
			if (currentStateName == "open") {
				ChangeCurrentState(DoorState.closing, false);
				DI_EventCenter<GameObject>.invoke("OnDoorClose", this.gameObject);
				if (!closingSound.isPlaying) {
					closingSound.Play();
				}
			}
		}
	}
}
