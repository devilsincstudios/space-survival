/*
*
* 	Devils Inc Studios
* 	How Long
* 	Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
*	
*	ENUM for types of movements of doors.
*
*/

namespace DI.Entities.Door {
	public enum DoorState {
		open,
		opening,
		closed,
		closing
	}
}