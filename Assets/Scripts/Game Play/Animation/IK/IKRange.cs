﻿//// Devils Inc Studios
//// Copyright DEVILS INC. STUDIOS LIMITED 2016
////
//// TODO: Include a description of the file here.
////
//
//using DI.Core.Behaviours;
//using DI.Controllers;
//using DI.Entities.Core;
//using DI.Entities.Player;
//using DI.Entities.NPC;
//using DI.Core.Debug;
//
//using UnityEngine;
//using System;
//
//namespace DI.Animation
//{
//	public class IKRange : DI_MonoBehaviour
//	{
//		public GameObject actor;
//		private DI_CharacterProperties entity;
//		public bool isPlayer = false;
//		public bool isNPC = false;
//
//		public void OnEnable()
//		{
//			if (isPlayer) {
//				entity = actor.GetComponent<DI.Controllers.Locomotion>().characterProperties;
//			}
//			if (isNPC) {
//			}
//		}
//
//		public void OnTriggerStay(Collider other)
//		{
//			if (other.tag != "Ground" && other.tag != "Player" && other.tag != "NPC") {
//				//Debug.Log(other.tag);
//				//DI_Debug.DrawMarker(transform.position, 1f, Color.blue, 0.1f, false);
//				//DI_Debug.DrawMarker(other.ClosestPointOnBounds(transform.position), 1f, Color.blue, 0.1f, false);
//
//				Vector3 relativePosition = transform.InverseTransformPoint(other.ClosestPointOnBounds(transform.position));
//
//				if (relativePosition.x > 0) {
//					entity.animationProperties.rightHandIKTarget = other.ClosestPointOnBounds(transform.position);
//					entity.animationProperties.rightHandWeight = 1f;
//					entity.animationProperties.leftHandWeight = 0f;
//					//Debug.Log("Right Hand");
//					// Right
//				}
//				else {
//					// Left
//					entity.animationProperties.leftHandIKTarget = other.ClosestPointOnBounds(transform.position);
//					entity.animationProperties.leftHandWeight = 1f;
//					entity.animationProperties.rightHandWeight = 0f;
//					//Debug.Log("Left Hand");
//				}
//				if (relativePosition.y > 0) {
//					// Above
//				}
//				else {
//					// Below
//				}
//				if (relativePosition.z > 0) {
//					// In Front
//				}
//				else {
//					// Behind
//
//				}
//			}
//		}
//
//		public void OnTriggerExit(Collider other)
//		{
//			if (other.tag != "Ground" && other.tag != "Player" && other.tag != "NPC") {
//				entity.animationProperties.leftHandWeight = 0f;
//				entity.animationProperties.rightHandWeight = 0f;
//			}
//		}
//	}
//}
//
