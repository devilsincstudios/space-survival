﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using UnityEngine.Audio;

namespace DI.Audio
{
	public static class Volume
	{
		public static void setValue(AudioMixer mixer, string channel, float value)
		{
			mixer.SetFloat(channel + "Volume", Mathf.Clamp(value - 80f, -80f, 0f));
			saveValue(mixer, channel, value);
		}

		public static void saveValue(AudioMixer mixer, string channel, float value)
		{
			PlayerPrefs.SetFloat("Settings-Audio-Volume-" + channel, value);
			PlayerPrefs.Save();
		}

		public static float loadValue(AudioMixer mixer, string channel)
		{
			float value = PlayerPrefs.GetFloat("Settings-Audio-Volume-" + channel, 100f);
			setValue(mixer, channel, value);
			return value;
		}
	}
}