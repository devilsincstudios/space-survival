﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using System.Collections.Generic;
using DI.Dialog;
using DI.Core.Behaviours;

namespace DI.Quests
{
	public class QuestGroup : DI_MonoBehaviour
	{
		public int groupId;
		public string groupName;
		public string groupDescription;
		public int currentQuest;
		public List<Quest> groupQuests;
		public DI_DialogScript startDialog;
		public DI_DialogScript finishDialog;

		public void OnEnable()
		{
			groupQuests = new List<Quest>();
			for (int index = 0; index < transform.childCount; index++) {
				Quest quest = transform.GetChild(index).GetComponent<Quest>();
				if (quest != null) {
					groupQuests.Add(quest);
				}
			}
		}

		public Quest getCurrentQuest()
		{
			return groupQuests[currentQuest];
		}

		public bool isQuestGroupComplete()
		{
			for (int index = 0; index < groupQuests.Count; index++) {
				if (!groupQuests[index].isQuestComplete()) {
					return false;
				}
			}
			return true;
		}
	}
}