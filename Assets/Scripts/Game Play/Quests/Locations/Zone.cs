﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using DI.Core.Behaviours;
using DI.Core.Events;
using System.Collections.Generic;

namespace DI.Quests
{
	public class Zone : DI_MonoBehaviour
	{
		public List<GameObject> collidingObjects;

		public void Awake()
		{
			collidingObjects = new List<GameObject>();
		}

		public void OnTriggerEnter(Collider other)
		{
			if (!collidingObjects.Contains(other.gameObject)) {
				collidingObjects.Add(other.gameObject);
				DI_EventCenter<string, GameObject>.invoke("OnEnterZone", gameObject.name, other.gameObject);	
			}
		}

		public void OnTriggerExit(Collider other)
		{
			if (collidingObjects.Contains(other.gameObject)) {
				DI_EventCenter<string, GameObject>.invoke("OnExitZone", gameObject.name, other.gameObject);	
				collidingObjects.Remove(other.gameObject);
			}
		}
	}
}