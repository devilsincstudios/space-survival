﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using System.Collections.Generic;
using DI.Dialog;
using DI.Core.Behaviours;

namespace DI.Quests
{
	public class Quest : DI_MonoBehaviour
	{
		public int questId = 0;
		public string questName;
		public string questDescription;
		public List<Objective> objectives;
		public DI_DialogScript startDialog;
		public DI_DialogScript finishDialog;

		public void OnEnable()
		{
			objectives = new List<Objective>();
			for (int index = 0; index < transform.childCount; index++) {
				Objective objective = transform.GetChild(index).GetComponent<Objective>();
				if (objective != null) {
					objectives.Add(objective);
				}
			}
		}

		public bool isQuestComplete()
		{
			for (int index = 0; index < objectives.Count; index++) {
				if (!objectives[index].isOptional() || !objectives[index].isComplete()) {
					return false;
				}
			}
			return true;
		}
	}
}