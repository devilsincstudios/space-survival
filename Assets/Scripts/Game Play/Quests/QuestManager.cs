﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using DI.Core.Behaviours;
using System.Collections.Generic;
using UnityEngine;
using DI.Core.Events;
using DI.Mechanics.Loot;

namespace DI.Quests
{
	public class QuestManager : DI_MonoBehaviourSingleton<QuestManager>
	{
		public List<QuestGroup> questGroups;
		public int currentQuestGroup = 0;

		public void Awake()
		{
			// If we have another instance active, destroy it and replace it with this one.
			if (instance != null) {
				Destroy(instance);
			}
			makeSingleton(this);
		}

		public QuestGroup getActiveQuestGroup()
		{
			if (currentQuestGroup < questGroups.Count) {
				return questGroups[currentQuestGroup];
			}
			return null;
		}
		public Quest getActiveQuest()
		{
			return getActiveQuest(getActiveQuestGroup());
		}

		public Quest getActiveQuest(QuestGroup group)
		{
			return group.getCurrentQuest();
		}
	}
}