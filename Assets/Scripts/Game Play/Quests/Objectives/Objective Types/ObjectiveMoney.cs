﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using System;
using UnityEngine;
using DI.Core.Events;
using DI.Mechanics.Loot;

namespace DI.Quests
{
	[Serializable]
	public class ObjectiveMoney : Objective
	{
		public ObjectiveTypes objectiveType = ObjectiveTypes.money;
		public float quantityRequired;
		public float quantityPossesed;

		public override bool isComplete()
		{
			return (quantityPossesed >= quantityRequired);
		}
		public override float getProgress()
		{
			return (Mathf.Round((quantityPossesed / quantityRequired) * 10f) / 10f);
		}
		public override ObjectiveTypes getType()
		{
			return objectiveType;
		}

		public void OnEnable()
		{
			DI_EventCenter<float, GameObject>.addListener("OnUpdateMoney", handleUpdateInventory);
		}

		public void OnDisable()
		{
			DI_EventCenter<float, GameObject>.removeListener("OnUpdateMoney", handleUpdateInventory);
		}

		public void handleUpdateInventory(float amount, GameObject inventoryObject)
		{
			if (!completed) {
				if (inventoryObject.tag == "Player") {
					quantityPossesed = amount;
				}
				if (isComplete()) {
					completed = true;
					onComplete();
				}
				else {
					onUpdate();
				}
			}
		}
	}
}