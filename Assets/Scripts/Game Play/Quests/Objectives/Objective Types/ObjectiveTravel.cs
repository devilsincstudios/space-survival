﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using System;
using UnityEngine;
using DI.Core.Events;
using DI.Mechanics.Loot;

namespace DI.Quests
{
	public class ObjectiveTravel : Objective
	{
		public ObjectiveTypes objectiveType = ObjectiveTypes.travel;
		public Zone zone;

		public override bool isComplete()
		{
			return completed;
		}
		public override float getProgress()
		{
			if (isComplete()) {
				return 1f;
			}
			else {
				return 0f;
			}
		}
		public override ObjectiveTypes getType()
		{
			return objectiveType;
		}

		public void OnEnable()
		{
			DI_EventCenter<string, GameObject>.addListener("OnEnterZone", handleEnterZone);
		}

		public void OnDisable()
		{
			DI_EventCenter<string, GameObject>.removeListener("OnEnterZone", handleEnterZone);
		}

		public void handleEnterZone(string zone, GameObject entity)
		{
			if (!completed) {
				if (zone != null) {
					if (this.zone != null) {
						if (zone == this.zone.gameObject.name) {
							if (entity.tag == "Player") {
								completed = true;
								onComplete();
							}
						}
					}
				}
			}
		}
	}
}