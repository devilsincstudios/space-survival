﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using System;
using UnityEngine;
using DI.Core.Events;
using DI.Mechanics.Interactions;

namespace DI.Quests
{
	[Serializable]
	public class ObjectiveUse : Objective
	{
		public ObjectiveTypes objectiveType = ObjectiveTypes.use;
		public Mechanism mechanism;

		public override bool isComplete()
		{
			return completed;
		}
		public override float getProgress()
		{
			if (completed) {
				return 1f;
			}
			else {
				return 0f;
			}
		}

		public override ObjectiveTypes getType()
		{
			return objectiveType;
		}

		public void OnEnable()
		{
			DI_EventCenter<Mechanism>.addListener("OnUseMechanism", handleUseMechanism);
		}

		public void OnDisable()
		{
			DI_EventCenter<Mechanism>.removeListener("OnUseMechanism", handleUseMechanism);
		}

		public void handleUseMechanism(Mechanism entity)
		{
			if (!completed) {
				if (mechanism == entity) {
					completed = true;
	
					if (isComplete()) {
						onComplete();
					}
				}
			}
		}
	}
}