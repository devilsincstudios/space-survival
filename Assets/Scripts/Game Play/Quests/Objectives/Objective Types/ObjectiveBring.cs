﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using System;
using UnityEngine;
using DI.Core.Events;
using DI.Mechanics.Loot;

namespace DI.Quests
{
	public class ObjectiveBring : Objective
	{
		public ObjectiveTypes objectiveType = ObjectiveTypes.bring;
		public string zoneName;
		public QuestEntity entity;

		public override bool isComplete()
		{
			return completed;
		}
		public override float getProgress()
		{
			if (isComplete()) {
				return 1f;
			}
			else {
				return 0f;
			}
		}
		public override ObjectiveTypes getType()
		{
			return objectiveType;
		}

		public void OnEnable()
		{
			DI_EventCenter<string, GameObject>.addListener("OnEnterZone", handleEnterZone);
		}

		public void OnDisable()
		{
			DI_EventCenter<string, GameObject>.removeListener("OnEnterZone", handleEnterZone);
		}

		public void handleEnterZone(string zone, GameObject entity)
		{
			if (!completed) {
				if (zone == zoneName) {
					if (entity.tag == "Quest Entity") {
						QuestEntity questEntity = entity.GetComponent<QuestEntity>();
						if (questEntity != null) {
							if (questEntity.entityName == this.entity.entityName) {
								completed = true;
								onComplete();
							}
						}
					}
				}
			}
		}
	}
}