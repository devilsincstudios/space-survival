﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using System;
using UnityEngine;
using DI.Core.Events;
using DI.Mechanics.Puzzles;

namespace DI.Quests
{
	[Serializable]
	public class ObjectivePuzzle: Objective
	{
		public ObjectiveTypes objectiveType = ObjectiveTypes.puzzle;
		public Puzzle puzzle;

		public override bool isComplete()
		{
			return completed;
		}

		public override float getProgress()
		{
			if (completed) {
				return 1f;
			}
			else {
				return 0f;
			}
		}

		public override ObjectiveTypes getType()
		{
			return objectiveType;
		}

		public void OnEnable()
		{
			DI_EventCenter<Puzzle>.addListener("OnCompletePuzzle", handleCompletePuzzle);
		}

		public void OnDisable()
		{
			DI_EventCenter<Puzzle>.removeListener("OnCompletePuzzle", handleCompletePuzzle);
		}

		public void handleCompletePuzzle(Puzzle entity)
		{
			if (!completed) {
				if (puzzle == entity) {
					completed = true;

					if (isComplete()) {
						onComplete();
					}
				}
			}
		}
	}
}