﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using System;
using UnityEngine;
using DI.Core.Events;
using DI.Mechanics.Loot;

namespace DI.Quests
{
	[Serializable]
	public class ObjectiveCollect : Objective
	{
		public ObjectiveTypes objectiveType = ObjectiveTypes.collect;
		public LootItem objectiveItem;
		public int quantityRequired;
		public int quantityPossesed;

		public override bool isComplete()
		{
			return (quantityPossesed >= quantityRequired);
		}
		public override float getProgress()
		{
			return (Mathf.Round((quantityPossesed / quantityRequired) * 10f) / 10f);
		}
		public override ObjectiveTypes getType()
		{
			return objectiveType;
		}

		public void OnEnable()
		{
			DI_EventCenter<LootItem, GameObject>.addListener("OnAddItem", handleAddToInventory);
			DI_EventCenter<LootItem, GameObject>.addListener("OnRemoveItem", handleRemoveFromInventory);
		}
		public void OnDisable()
		{
			DI_EventCenter<LootItem, GameObject>.removeListener("OnAddItem", handleAddToInventory);
			DI_EventCenter<LootItem, GameObject>.removeListener("OnRemoveItem", handleRemoveFromInventory);
		}

		public void handleAddToInventory(LootItem item, GameObject inventoryObject)
		{
			if (!completed) {
				if (inventoryObject.tag == "Player") {
					if (item.name == objectiveItem.name) {
						quantityPossesed += 1;
					}
				}

				if (isComplete()) {
					completed = true;
					onComplete();
				}
				else {
					onUpdate();
				}
			}
		}
		public void handleRemoveFromInventory(LootItem item, GameObject inventoryObject)
		{
			if (!completed) {
				if (inventoryObject.tag == "Player") {
					quantityPossesed += 1;
				}

				if (isComplete()) {
					onComplete();
				}
				else {
					onUpdate();
				}
			}
		}
	}
}