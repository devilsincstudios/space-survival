﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using System;
using UnityEngine;
using DI.Core.Events;
using DI.Mechanics.Interactions;

namespace DI.Quests
{
	[Serializable]
	public class ObjectiveMechanism : Objective
	{
		public ObjectiveTypes objectiveType = ObjectiveTypes.mechanism;
		public Mechanism mechanism;

		public override bool isComplete()
		{
			return completed;
		}
		public override float getProgress()
		{
			if (completed) {
				return 1f;
			}
			else {
				return 0f;
			}
		}

		public override ObjectiveTypes getType()
		{
			return objectiveType;
		}

		public void OnEnable()
		{
			DI_EventCenter<Mechanism, bool>.addListener("OnMechanismStateChange", handleUseMechanism);
		}

		public void OnDisable()
		{
			DI_EventCenter<Mechanism, bool>.removeListener("OnMechanismStateChange", handleUseMechanism);
		}

		public void handleUseMechanism(Mechanism entity, bool isActive)
		{
			if (mechanism == entity) {
				if (isActive) {
					completed = true;
				}
				else {
					completed = false;
				}
				onUpdate();
			}
		}
	}
}