﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using System;
using DI.Core;
using UnityEngine;

namespace DI.Core.Cheat
{
	public static class Cheat_MapSeed
	{
		public static bool active = false;

		public static void activate(string seed)
		{
			PlayerPrefs.SetString("Seed", seed);
			active = true;
		}

		public static void deactivate()
		{
			PlayerPrefs.DeleteKey("Seed");
			active = false;
		}
	}
}