﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using System;
using DI.Core;
using UnityEngine;

namespace DI.Core.Cheat
{
	public static class Cheat_UnlimitedAmmo
	{
		public static bool active = false;

		public static void activate()
		{
			Management.instance.unlimitedAmmo = true;
			active = true;
		}

		public static void deactivate()
		{
			Management.instance.unlimitedAmmo = false;
			active = false;
		}
	}
}