﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using Zenject;

using DI.Entities.Properties;
using UnityEngine;
using DI.Mechanics.Loot;
using DI.Entities.Core;

namespace DI.Entities
{
	public class PlaceableEntity : DI_Entity
	{
		public float buildCost;
		public float buildTime;
		public Texture2D icon;
		public string description = "Description not yet populated";
		public GameObject prefab;
	}
}