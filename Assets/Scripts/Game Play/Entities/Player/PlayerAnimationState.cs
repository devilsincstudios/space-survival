﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

namespace DI.Entities.Player
{
	public enum PlayerAnimationState
	{
		idling,
		double_jump,
		falling,
		jumping,
		running,
		sneaking,
		sprinting,
		climbing_ladder,
		climbing_rope,
		walking_ledge,		
		walking,
		interacting,
		cinema,
		custom,
		sleeping,
	}
}