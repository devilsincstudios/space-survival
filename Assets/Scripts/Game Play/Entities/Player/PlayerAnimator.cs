﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using DI.Entities;
using UnityEngine;
using System;
using DI.Core.Debug;
using DI.Entities.Properties;

namespace DI.Entities.Player
{
	public class PlayerAnimator : EntityAnimator
	{
		[HideInInspector]
		public float speed;
		[HideInInspector]
		public float direction;
		[HideInInspector]
		public float distanceFromGround;
		[HideInInspector]
		public bool crouching = false;
		[SerializeField]
		protected DI_AnimationProperty animationProperties;

		public void setLookAtPosition(Vector3 position)
		{
			animationProperties.headLookTarget = position;
		}
		public void setLookAtWeight(float weight)
		{
			animationProperties.headLookWeight = weight;
		}
		public void setSpineRotation(Vector3 rotation)
		{
			animationProperties.spineIKRotation = rotation;
		}

		public new void OnEnable()
		{
			base.OnEnable();
			animationProperties.animator = animator;
		}

		public void setIKPosition(AvatarIKGoal goal, Vector3 position)
		{
			switch (goal)
			{
				case AvatarIKGoal.LeftFoot:
					animationProperties.leftFootIKTarget = position;
					break;
				case AvatarIKGoal.LeftHand:
					animationProperties.leftHandIKTarget = position;
					break;
				case AvatarIKGoal.RightFoot:
					animationProperties.rightFootIKTarget = position;
					break;
				case AvatarIKGoal.RightHand:
					animationProperties.rightHandIKTarget = position;
					break;
			}
		}

		public void setIKWeight(AvatarIKGoal goal, float weight)
		{
			switch (goal)
			{
				case AvatarIKGoal.LeftFoot:
					animationProperties.leftFootWeight = weight;
					break;
				case AvatarIKGoal.LeftHand:
					animationProperties.leftHandWeight = weight;
					break;
				case AvatarIKGoal.RightFoot:
					animationProperties.rightFootWeight = weight;
					break;
				case AvatarIKGoal.RightHand:
					animationProperties.rightHandWeight = weight;
					break;
			}
		}

		public void OnAnimatorIK()
		{
			animator.SetLookAtPosition(animationProperties.headLookTarget);
			animator.SetLookAtWeight(animationProperties.headLookWeight);
			animator.SetIKPosition(AvatarIKGoal.LeftHand, animationProperties.leftHandIKTarget);
			animator.SetIKPosition(AvatarIKGoal.RightHand, animationProperties.rightHandIKTarget);
			animator.SetIKPositionWeight(AvatarIKGoal.LeftHand, animationProperties.leftHandWeight);
			animator.SetIKPositionWeight(AvatarIKGoal.RightHand, animationProperties.rightHandWeight);
			animator.SetBoneLocalRotation(HumanBodyBones.Spine, Quaternion.Euler(animationProperties.spineIKRotation));
		}

		public void startFall()
		{
			animator.SetTrigger("Fall");
		}

		public void roll()
		{
			animator.SetTrigger("Roll");
		}

		public void startAim()
		{
			animator.SetLayerWeight(1, 1f);
		}

		public void stopAim()
		{
			animator.SetLayerWeight(1, 0f);
		}

		private void resetState()
		{
			animator.SetFloat("Speed", speed);
			animator.SetFloat("Direction", direction);
			animator.SetFloat("Distance From Ground", distanceFromGround);
			animator.SetFloat("Fall Height", distanceFromGround);
			animator.SetBool("Crouching", crouching);
		}

		public void Update()
		{
			animator.SetFloat("Speed", speed);
			animator.SetFloat("Direction", direction);
			animator.SetBool("Crouching", crouching);
			animator.SetFloat("Distance From Ground", distanceFromGround);
			animator.SetFloat("Fall Height", distanceFromGround);
		}
	}
}