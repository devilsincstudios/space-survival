﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using Zenject;
using UnityEngine;
using DI.Core.Behaviours;
using DI.Core.Input;
using UnityEngine.UI;
using DI.Mechanics.Loot;
using DI.Mechanics.Weapons;
using DI.Core.Pooling;
using System.Collections;
using DI.Entities.Enemy;
using DI.Entities.Limbs;
using DI.Core.Events;
using DI.Entities.Properties;
using DI.SFX;
using DI.Core;
using DI.Entities.Items;

/*
 * TODO: Break up PlayerController into more sub components
 * This class is a mess, its way to monolithic in scope and needs to be broken up into smaller chunks later.
*/

namespace DI.Entities.Player
{
	public class PlayerController : DI_MonoBehaviour
	{
		// Serialized for debugging in inspector
		[Inject]
		[SerializeField]
		protected PlayerAnimator playerAnimator;

		// Serialized for debugging in inspector
		[Inject]
		[SerializeField]
		protected Player playerSettings;

		// Serialized for debugging in inspector
		[Inject]
		[SerializeField]
		protected PlayerMotor playerMotor;

		// Serialized for debugging in inspector
		[SerializeField]
		private Vector3 speed;
		public Vector3 bulletSpawnPoint;

		[Header("Animation Settings")]
		public GameObject cameraPivot;
		public GameObject aimingTarget;
		public Vector3 aimingOffsets;
		public Transform spine;
		public GameObject spineLookHelper;
		public GameObject playerWeapon;

		[Header("Game Settings")]
		public LayerMask enemyMask;
		public float maxWeaponRange = 100f;

		[Header("Motor Settings")]
		public CapsuleCollider playerCollider;

		[Header("Loot Settings")]
		public WeaponInventory inventory;

		[Header("Control Settings")]
		public bool inControl = true;
		public bool canFire = true;
		// Serialized for debugging in inspector
		[Inject]
		[SerializeField]
		protected MouseLookSettings mouseLookSettings;

		[SerializeField]
		protected float rotationX;
		[SerializeField]
		protected float rotationY;
		[SerializeField]
		protected float sensitivityX = 5f;
		[SerializeField]
		protected float sensitivityY = 5f;
		protected bool sprintRecovery = false;
		public GameObject bloodSplatterPrefab;

		[Header("Sound FX Settings")]
		public DI_SFXProperty painSounds;

		[Header("Visible for Debug")]
		public float horizontal;
		public float vertical;
		public bool walk;
		public bool primaryFire;
		public bool secondaryFire;
		public bool isAlive = true;
		public float flinchTime = 0.2f;
		public float flinchTimeRemaining = 0f;
		public float primaryShotTimeRemaining = 0.0f;
		public float secondaryShotTimeRemaining = 0.0f;
		public bool isGrounded = false;

		[Header("Debug UI Settings")]
		public Image crosshair;
		public float points;

		private bool processingPhysics = false;
		private bool initalized = false;

		public void reset()
		{
			playerSettings.setHealth(playerSettings.getMaxHealth());
			points = 0f;
			isAlive = true;
			inControl = true;
		}

		public void moveToSpawn()
		{
			Debug.Log("Searching for respawn point.");
			transform.position = GameObject.FindGameObjectWithTag("Respawn").transform.position;
		}

		public void initalize()
		{
			if (!initalized) {
				initalized = true;
				if (!PlayerPrefs.HasKey("Settings-Input-Mouse_Sensitivity_X")) {
					PlayerPrefs.SetFloat("Settings-Input-Mouse_Sensitivity_X", sensitivityX);
				}
				else {
					sensitivityX = PlayerPrefs.GetFloat("Settings-Input-Mouse_Sensitivity_X", sensitivityX);
				}
				if (!PlayerPrefs.HasKey("Settings-Input-Mouse_Sensitivity_Y")) {
					PlayerPrefs.SetFloat("Settings-Input-Mouse_Sensitivity_Y", sensitivityY);
				}
				else {
					sensitivityY = PlayerPrefs.GetFloat("Settings-Input-Mouse_Sensitivity_Y", sensitivityY);
				}
				Cursor.lockState = CursorLockMode.Locked;
				Cursor.visible = false;

				aimingOffsets = new Vector3(Screen.width/2 + aimingOffsets.x, Screen.height/2 + aimingOffsets.y, aimingOffsets.z);
				StartCoroutine(startPhysics());
				spine = playerAnimator.getBoneTransform(HumanBodyBones.Spine);
				DI_EventCenter<DI.Entities.Enemy.Enemy, Vector3>.addListener("OnHitPlayer", handleOnHitPlayer);
			}
		}

		public void OnDisable()
		{
			DI_EventCenter<DI.Entities.Enemy.Enemy, Vector3>.removeListener("OnHitPlayer", handleOnHitPlayer);
		}

		public IEnumerator startPhysics()
		{
			yield return new WaitForSeconds(0.1f);
			processingPhysics = true;
			playerMotor.playerRadius = playerCollider.radius;
		}

		public void FixedUpdate()
		{
			if (processingPhysics) {
				if (!playerMotor.isGrounded) {
					if (playerAnimator.distanceFromGround < playerMotor.distanceFromGround) {
						playerAnimator.distanceFromGround = playerMotor.distanceFromGround;
					}
				}
				else {
					playerAnimator.distanceFromGround = playerMotor.distanceFromGround;
				}
				isGrounded = playerMotor.isGrounded;
				playerAnimator.setBool("Falling", !isGrounded);
			}

			bulletSpawnPoint = Camera.main.ScreenToWorldPoint(aimingOffsets);
		}

		public void LateUpdate()
		{
			#if UNITY_EDITOR
			if (Input.GetKeyDown(KeyCode.Pause)) {
				UnityEditor.EditorApplication.isPaused = !UnityEditor.EditorApplication.isPaused;
			}
			#endif
			if (!initalized) {
				initalize();
			}

			if (inControl) {
				characterLogic();
			}

			if (flinchTimeRemaining > 0.0f) {
				flinchTimeRemaining -= DI.Core.DI_Time.getTimeDelta();
			}
		}

		public void Update()
		{
			if (inControl) {
				updateInput();
			}
			else {
				nullPlayerInputs();
			}
		}

		public void addAmmo(float amount)
		{
			inventory.addAmmo(inventory.selectedWeapon.primaryAmmo, amount);
		}

		public float getAmmo()
		{
			return inventory.getAmmo(inventory.selectedWeapon.primaryAmmo);
		}

		public void addPoints(float amount)
		{
			points += amount;
		}

		public void addHealth(float amount)
		{
			playerSettings.addHealth(amount);
			DI_EventCenter.invoke("HealthChange");
		}

		public void setHealth(float amount)
		{
			if (amount == 0f) {
				playerSettings.setHealth(playerSettings.getMaxHealth());
			}
			else {
				DI_EventCenter.invoke("HealthChange");
				playerSettings.setHealth(amount);
			}
		}

		public void updateInput()
		{
			horizontal = Input.GetAxis("Horizontal");
			vertical = Input.GetAxis("Vertical");
			walk = Input.GetButton("Walk");
			primaryFire = Input.GetButton("Primary Fire");
			if (!primaryFire && Mathf.Abs(Input.GetAxis("Primary Fire")) > 0.1f) {
				primaryFire = true;
			}

//			secondaryFire = Input.GetButtonDown("Secondary Fire");
//			if (!secondaryFire && Mathf.Abs(Input.GetAxis("Secondary Fire")) > 0.1f) {
//				secondaryFire = true;
//			}

			if (primaryShotTimeRemaining > 0f) {
				primaryShotTimeRemaining -= DI.Core.DI_Time.getTimeDelta();
			}
			if (secondaryShotTimeRemaining > 0f) {
				secondaryShotTimeRemaining -= DI.Core.DI_Time.getTimeDelta();
			}
		}

		public void nullPlayerInputs()
		{
			playerAnimator.speed = 0f;
			playerAnimator.direction = 0f;
			playerMotor.currentSpeed = 0f;
			playerMotor.updateMovement(Vector3.zero);
		}

		// Mouse X turns the player's body left / right
		// Mouse Y rotates the camera up or down
		// The player needs to "point" at the crosshair
		public void characterLogic()
		{
			if (playerMotor.isGrounded) {
				if (walk) {
					speed.x = horizontal * playerSettings.walkingSpeed.maxSpeed;
					speed.z = vertical * playerSettings.walkingSpeed.maxSpeed;
					playerAnimator.speed = 1f * vertical;
					playerAnimator.direction = horizontal;
					playerMotor.currentSpeed = vertical * playerSettings.walkingSpeed.maxSpeed;
				}
				else {
					speed.x = horizontal * playerSettings.runningSpeed.maxSpeed;
					speed.z = vertical * playerSettings.runningSpeed.maxSpeed;
					playerAnimator.speed = 2f * vertical;
					playerAnimator.direction = horizontal * 2f;
					playerMotor.currentSpeed = vertical * playerSettings.runningSpeed.maxSpeed;
				}

				// Player is just moving horizontally
				// Apply a movement speed decrease to make controlling it easier.
				if (Mathf.Abs(speed.z) <= Mathf.Epsilon) {
					speed.x *= playerSettings.strafeSpeedPenalty;
				}
			}
			playerMotor.updateMovement(speed);

			rotationX = transform.localEulerAngles.y + Input.GetAxis("Mouse X") * sensitivityX;
			rotationY += Input.GetAxis("Mouse Y") * sensitivityY;

			rotationY = Mathf.Clamp (rotationY, mouseLookSettings.minimumY, mouseLookSettings.maximumY);

			transform.localEulerAngles = new Vector3(0f, rotationX, 0f);
			cameraPivot.transform.localEulerAngles = new Vector3(-rotationY, cameraPivot.transform.localEulerAngles.y, cameraPivot.transform.localEulerAngles.z);
			playerAnimator.setLookAtPosition(aimingTarget.transform.position);
			playerAnimator.setLookAtWeight(1f);

			if (canFire) {
				if (primaryFire) {
					if (fireWeapon(inventory.selectedWeapon.primaryAmmo, primaryShotTimeRemaining)) {
						primaryShotTimeRemaining = inventory.selectedWeapon.timeBetweenPrimaryShots;
					}
				}
				else if (secondaryFire) {
					if (fireWeapon(inventory.selectedWeapon.secondaryAmmo, secondaryShotTimeRemaining)) {
						secondaryShotTimeRemaining = inventory.selectedWeapon.timeBetweenSecondaryShots;
					}
				}
			}
		}

		public bool fireWeapon(LootAmmo ammo, float timeRemaining)
		{
			if (!Management.instance.inMenu) {
				if (timeRemaining <= 0.0f) {
					if (inventory.getAmmo(ammo) > 0 || Management.instance.unlimitedAmmo) {
						if (!Management.instance.unlimitedAmmo) {
							inventory.removeAmmo(ammo, 1f);
						}
					}
					else {
						// Player does not have enough ammo.
						// TODO play an out of ammo sound for this case and remove this log entry.
						Debug.Log("Not enough ammo");
						return false;
					}

					// Player has fired their weapon and a bullet will be consumed.
					// Fire an onFireWeapon event
					DI_EventCenter<LootWeapon, LootAmmo>.invoke("OnFireWeapon", inventory.selectedWeapon, ammo);

					if (ammo.raycastBullet) {
						RaycastHit hit;
						aimingTarget.transform.rotation = Quaternion.Euler(cameraPivot.transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y, transform.rotation.eulerAngles.z);
						Ray ray = new Ray(bulletSpawnPoint, aimingTarget.transform.forward);
						if (ammo.bulletSounds.hasSFX) {
							SFX.DI_SFX.playClipAtPoint(gameObject, transform.position, ammo.bulletSounds.sfxs[UnityEngine.Random.Range(0, ammo.bulletSounds.sfxs.Count)]);
						}
						if (Physics.Raycast(ray, out hit, maxWeaponRange, enemyMask)) {
							//Debug.Log("Hit: " + hit.collider.gameObject);
							if (hit.collider.CompareTag("Enemy")) {
								Limb limb = hit.collider.gameObject.GetComponent<Limb>();
								if (limb != null) {
									limb.rootNode.GetComponent<EnemyController>().takeDamage(limb, Weapon.getDamageAmount(inventory.selectedWeapon, ammo), ammo);
								}
							}
							else {
								if (ammo.bulletMissSounds.hasSFX) {
									SFX.DI_SFX.playClipAtPoint(gameObject, hit.point, ammo.bulletHitSounds.sfxs[UnityEngine.Random.Range(0, ammo.bulletHitSounds.sfxs.Count)]);
								}
								if (ammo.bulletMissEffects != null) {
									GameObject hitEffect = DI_PoolManager.getPooledObject(ammo.bulletMissEffects);
									hitEffect.transform.position = hit.point;
									hitEffect.SetActive(true);
								}
							}
						}
					}
					else {
						GameObject bullet = DI_PoolManager.getPooledObject(ammo.bulletPrefab);
						bullet.transform.position = bulletSpawnPoint;
						bullet.transform.rotation = Quaternion.Euler(cameraPivot.transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y, transform.rotation.eulerAngles.z);
						Bullet projectile = bullet.GetComponent<Bullet>();
						projectile.initalize();
						projectile.damage = Mathf.CeilToInt(UnityEngine.Random.Range(ammo.minDamage, ammo.maxDamage) + UnityEngine.Random.Range(inventory.selectedWeapon.minDamage, inventory.selectedWeapon.maxDamage));
						projectile.ammoType = ammo;
						projectile.firingWeapon = inventory.selectedWeapon;

						bullet.SetActive(true);
						bullet.GetComponent<Rigidbody>().AddForce(bullet.transform.forward * ammo.bulletSpeed, ForceMode.VelocityChange);
						if (ammo.bulletSounds.hasSFX) {
							SFX.DI_SFX.playClipAtPoint(bullet, bullet.transform.position, ammo.bulletSounds.sfxs[UnityEngine.Random.Range(0, ammo.bulletSounds.sfxs.Count)]);
						}
					}

					GameObject muzzleFlash = DI_PoolManager.getPooledObject(inventory.selectedWeapon.muzzleFlashPrefab);
					muzzleFlash.transform.position = playerWeapon.transform.position;
					muzzleFlash.transform.rotation = playerWeapon.transform.rotation;
					muzzleFlash.SetActive(true);

					return true;
				}
			}
			return false;
		}
	
		/// <summary>
		/// Returns the current health of the player
		/// </summary>
		/// <returns>The player's health</returns>
		public float getHealth()
		{
			return playerSettings.getHealth();
		}

		/// <summary>
		/// Returns the max health of the player
		/// </summary>
		/// <returns>The player's max health</returns>
		public float getMaxHealth()
		{
			return playerSettings.getMaxHealth();
		}

		/// <summary>
		/// Returns the current stamina of the player
		/// </summary>
		/// <returns>The player's stamina</returns>
		public float getStamina()
		{
			return playerSettings.staminaSettings.currentStamina;
		}

		/// <summary>
		/// Returns the max stamina of the player
		/// </summary>
		/// <returns>The player's max stamina</returns>
		public float getMaxStamina()
		{
			return playerSettings.staminaSettings.maxStamina;
		}

		public void takeDamage(float amount)
		{
			if (!Management.instance.godMode) {
				if (flinchTimeRemaining <= 0f) {
					amount = Mathf.RoundToInt(amount);
					//Debug.Log("Took " + amount + " damage!");
					playerSettings.removeHealth(amount);
					DI_EventCenter<Player>.invoke("OnHit", playerSettings);
					if (playerSettings.getHealth() == 0f) {
						playerSettings.onDespawn();
						playerAnimator.ragdoll(true);
						DI_EventCenter<Player>.invoke("OnPlayerDeath", playerSettings);
						inControl = false;
						isAlive = false;
					}
					flinchTimeRemaining = flinchTime;

					if (painSounds.hasSFX) {
						DI_SFX.playRandomClipAtPoint(gameObject, transform.position, painSounds);
					}
				}
			}
		}

		public void handleOnHitPlayer(DI.Entities.Enemy.Enemy enemy, Vector3 hitPoint)
		{
			if (isAlive) {
				takeDamage(UnityEngine.Random.Range(enemy.minDamage, enemy.maxDamage));
				GameObject splatter = DI_PoolManager.getPooledObject(bloodSplatterPrefab);
				splatter.transform.position = hitPoint;
				splatter.SetActive(true);
			}
		}

		public void OnTriggerEnter(Collider other)
		{
			if (other.CompareTag("Pickup")) {
				PowerupEntity powerup = other.GetComponent<PowerupEntity>();
				powerup.consume();
			}
		}

		public void OnDrawGizmos()
		{
			if (processingPhysics) {
				Gizmos.color = Color.yellow;
				Gizmos.DrawWireCube(playerMotor.hitPoint, new Vector3(0.3f, 0.3f, 0.3f));
			}
		}
	}
}