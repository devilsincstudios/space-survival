﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using Zenject;
using UnityEngine;
using DG;
using DI.Entities;
using System;
using DI.Core;

namespace DI.Entities.Player
{
	[Serializable]
	public class PlayerMotor : EntityMotor
	{
		protected Player playerSettings;
		protected Vector3 velocityChange;
		public bool isCrouched = false;
		public bool isGrounded = true;
		public float currentSpeed;
		public float playerRadius = 0.25f;
		public float distanceFromGround = 0f;
		public Vector3 hitPoint;

		public PlayerMotor(Player _playerSettings)
		{
			playerSettings = _playerSettings;
		}

		public void updateMovement(Vector3 targetVelocity)
		{
			try {
				targetVelocity = playerSettings.getEntity().transform.TransformDirection(targetVelocity);
				// Apply a force that attempts to reach our target velocity
				Vector3 velocity = playerSettings.getEntityBody().velocity;
				velocityChange = (targetVelocity - velocity);
				velocityChange.x = Mathf.Clamp(velocityChange.x, -10f, 10f);
				velocityChange.z = Mathf.Clamp(velocityChange.z, -10f, 10f);
				velocityChange.y = targetVelocity.y;
			}
			catch (Exception) {
			}
		}

		public void move()
		{
			if (playerSettings.getEntityBody() != null) {
				RaycastHit hit;
				if (playerSettings.getEntityBody().SweepTest(playerSettings.getEntity().transform.forward, out hit, currentSpeed * DI_Time.getTimeDelta() + playerRadius)) {
					if (hit.point.y - playerSettings.getEntity().transform.position.y <= playerSettings.stepHeight) {
						playerSettings.getEntity().transform.position = new Vector3(
							playerSettings.getEntity().transform.position.x,
							hit.point.y,
							playerSettings.getEntity().transform.position.z
						);
					}
				}
				else {
					if (playerSettings.getEntity() != null) {
						if (Physics.Raycast(playerSettings.getEntity().transform.position + (Vector3.up * 0.1f), -playerSettings.getEntity().transform.up, out hit)) {
							if ((hit.distance - 0.1f) <= playerSettings.groundCheckDistance) {
								isGrounded = true;
							}
							else {
								isGrounded = false;
							}
							distanceFromGround = (hit.distance - 0.1f);
						}
						else {
							isGrounded = false;
							distanceFromGround = -1f;
						}
					}
				}

				playerSettings.getEntityBody().AddForce(velocityChange, ForceMode.VelocityChange);
				playerSettings.getEntityBody().AddForce(new Vector3 (0, 2 * Physics.gravity.y * playerSettings.getEntityBody().mass, 0), ForceMode.Force);
			}
		}

		public override void LateTick()
		{
			//move();
		}

		public override void FixedTick()
		{
			move();
		}
	}
}