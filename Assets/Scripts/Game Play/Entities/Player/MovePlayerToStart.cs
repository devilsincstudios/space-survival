﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using DI.Core.Behaviours;
using UnityEngine;
using System.Collections;

namespace DI.Entities.Player
{
	public class MovePlayerToStart : DI_MonoBehaviour
	{
		public MapGen.MapGenerator generator;
		public GameObject player;

		public void OnEnable()
		{
			StartCoroutine(delayedStart());
		}

		public IEnumerator delayedStart()
		{
			while (!generator.buildComplete()) {
				yield return new WaitForSecondsRealtime(0.1f);
			}

			GameObject spawnPoint = GameObject.FindGameObjectWithTag("Respawn");
			player.transform.position = spawnPoint.transform.position;
		}
	}
}