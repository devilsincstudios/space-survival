﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using Zenject;

using DI.Entities.NPC;
using DI.Entities.Properties;
using UnityEngine;

namespace DI.Entities.Enemy
{
	public class Enemy : DI_NPCEntity
	{
		public DI_MovementProperty walkingSpeed;
		public DI_MovementProperty runningSpeed;
		public DI_MovementProperty sprintingSpeed;
		public bool isMelee = true;
		public float meleeRange = 0.2f;
		public float attackRange = 0.5f;
		public float bodyLingerTime = 20f;

		[Header("Damage Multiplers")]
		public float bodyDamage = 1.0f;
		public float headDamage = 5.0f;
		public float limbDamage = 0.5f;

		[Header("Attack Damage")]
		public float minDamage = 0f;
		public float maxDamage = 10f;
		public float attackDelay = 1f;
		public float attackAnimationDelay = 1f;
		public DI_SFXProperty attackSoundFX;
		public DI_SFXProperty painSoundFX;

		[Header("Scoring")]
		public float pointValue = 25f;
		public float headShotMultipler = 2f;
		public float bodyShotMultipler = 1f;
		public float armShotMultipler = 1f;
		public float legShotMultipler = 1f;
		public float limbShotMultipler = 0.25f;

		[Header("Misc")]
		public Vector2 staggerTime;
		public Vector2 crippleTime;
	}
}