﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using System;
using System.Collections;
using UnityEngine;
using UnityEngine.AI;
using DI.Entities;
using DI.Core.Behaviours;

namespace DI.Entities.Enemy
{
	[Serializable]
	public class EnemyMotor : DI_MonoBehaviour
	{
		public Enemy settings;
		public bool initalized = false;
		public NavMeshAgent agent;
		public bool isGrounded = true;
		public float distanceFromGround = 0.0f;
		public Vector3 wanderTarget;
		public bool hasWanderTarget = false;
		public bool calculatingWanderTarget = false;

		public void initalize()
		{
			initalized = true;
			settings = GameObject.Instantiate(settings);
			agent = GetComponent<NavMeshAgent>();
		}

		public void OnEnable()
		{
			if (!initalized) {
				initalize();
			}
		}

		public void moveRandom(float speed, float radius)
		{
			StartCoroutine(generateWanderTarget(radius));
			if (hasWanderTarget) {
				move(wanderTarget, speed);
			}
		}

		public void move(Vector3 target, float speed)
		{
			agent.destination = target;
			agent.speed = speed;
		}

		public void LateUpdate()
		{
			//Debug.Log("Has Path:" + agent.hasPath);
			//NavMeshPath path = new NavMeshPath();
			//agent.CalculatePath(agent.destination, path);
			//Debug.Log(path.status.ToString());

//			if (settings.getEntity() != null) {
//				RaycastHit hit;
//
//				if (Physics.Raycast(settings.getEntity().transform.position + (Vector3.up * 0.1f), -settings.getEntity().transform.up, out hit)) {
//					if ((hit.distance - 0.1f) <= settings.groundCheckDistance) {
//						isGrounded = true;
//					}
//					else {
//						isGrounded = false;
//					}
//					distanceFromGround = (hit.distance - 0.1f);
//				}
//				else {
//					isGrounded = false;
//					distanceFromGround = -1f;
//				}
//			}
		}

		public IEnumerator generateWanderTarget(float wanderRadius)
		{
			calculatingWanderTarget = true;
			UnityEngine.AI.NavMeshHit hit;
			for (int attempt = 0; attempt < 30; attempt++) {
				Vector3 point = transform.position + UnityEngine.Random.insideUnitSphere * wanderRadius;
				if (UnityEngine.AI.NavMesh.SamplePosition(point, out hit, 1.0f, UnityEngine.AI.NavMesh.AllAreas)) {
					wanderTarget = hit.position;
					hasWanderTarget = true;
					calculatingWanderTarget = false;
				}
				yield return new WaitForSecondsRealtime(0.1f);
			}
			calculatingWanderTarget = false;
			hasWanderTarget = false;
		}

		public void OnDrawGizmosSelected()
		{
			if (agent.hasPath) {
				if (agent.path.corners != null) {
					for (int corner = 0; corner < agent.path.corners.Length; corner++) {
						Gizmos.color = Color.blue;
						Gizmos.DrawWireCube(agent.path.corners[corner], new Vector3(0.2f, 0.2f, 0.2f));
						if (corner != agent.path.corners.Length - 1) {
							Gizmos.color = Color.cyan;
							Gizmos.DrawLine(agent.path.corners[corner], agent.path.corners[corner + 1]);
						}
					}
				}
			}
		}
	}
}