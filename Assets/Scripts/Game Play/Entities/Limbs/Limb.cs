﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using DI.Core.Behaviours;

namespace DI.Entities.Limbs
{
	public class Limb : DI_MonoBehaviour
	{
		public HumanBodyBones bone;
		public GameObject rootNode;

		public bool isHead()
		{
			return (bone == HumanBodyBones.Head);
		}

		public bool isBody()
		{
			return (bone == HumanBodyBones.Spine || bone == HumanBodyBones.Chest || bone == HumanBodyBones.Hips);
		}

		public bool isArm()
		{
			return (bone == HumanBodyBones.RightUpperArm || bone == HumanBodyBones.RightLowerArm || bone == HumanBodyBones.LeftUpperArm || bone == HumanBodyBones.LeftLowerArm);
		}

		public bool isLeg()
		{
			return (bone == HumanBodyBones.RightUpperLeg || bone == HumanBodyBones.RightLowerLeg || bone == HumanBodyBones.LeftUpperLeg || bone == HumanBodyBones.LeftLowerLeg);
		}

		public bool isLimb()
		{
			return (!isHead() && !isBody());
		}
	}
}