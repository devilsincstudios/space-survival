﻿
// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using UnityEngine.AI;
using System.Collections;
using Zenject;

using DI.Core.Behaviours;
using DI.Mechanics.Weapons;
using DI.Entities.Limbs;
using DI.Entities.Player;
using DI.Core.Events;
using DI.SFX;
using DI.Core;
using DI.Mechanics.Loot;
using System.Collections.Generic;
using DI.Entities.Enemy;
using DI.Core.Pooling;
using DI.AI;
using System;

namespace DI.Entities.Turrets
{
	[RequireComponent(typeof(TurretAnimator))]
	public class TurretController : DI_MonoBehaviour
	{
		public Turret settings;
		public TurretAnimator turretAnimator;
		protected bool initalized = false;
		public float aiDelay = 0.2f;

		// Serialized for debug view
		[SerializeField]
		protected float currentHealth = 0f;
		public float attackDelay;
		public float attackDelayRemaining;

		public LayerMask targetMask;
		public LayerMask enemyMask;

		public EnemyController currentTarget;
		public GameObject currentTargetObject;
		public Transform bulletSpawnPoint;
		public LineRenderer laserSight;

		// Keep these around to avoid adding more memory
		private float closestTarget;
		private GameObject potentialTarget;
		private float potentialTargetDistance;
		private RaycastHit hit;

		public FieldOfView fov;

		public void initalize()
		{
			initalized = true;
			turretAnimator = GetComponent<TurretAnimator>();
			settings = Instantiate(settings);
			settings.setEntityBody(this.GetComponent<Rigidbody>());
			attackDelay = settings.attackDelay;
		}

		public void OnEnable()
		{
			if (!initalized) {
				initalize();
			}
			// Make our own copy of the settings so we can change them as needed without editing the master copy.

			settings.onSpawn();
			DI_EventCenter<Turret>.invoke("OnSpawn", settings);
			settings.setHealth(settings.getMaxHealth());
			currentHealth = settings.getHealth();
			laserSight.SetPosition(0, bulletSpawnPoint.transform.position);
			enemyMask = ~enemyMask;
			StartCoroutine(evaluateTargets());
			DI_EventCenter<GameObject, GameObject>.addListener("OnTargetLeftView", handleOnTargetLeftView);
		}

		public void OnDisable()
		{
			StopCoroutine(evaluateTargets());
			DI_EventCenter<GameObject, GameObject>.removeListener("OnTargetLeftView", handleOnTargetLeftView);
		}

		public void handleOnTargetLeftView(GameObject caller, GameObject target)
		{
			if (caller == fov) {
				if (target == currentTarget.gameObject) {
					currentTarget = null;
				}
			}
		}

		public IEnumerator evaluateTargets()
		{
			yield return new WaitForSecondsRealtime(UnityEngine.Random.Range(0, aiDelay));
			while (true) {
				if (fov.objectsInView.Count > 0) {
					GameObject[] objectsInView = fov.objectsInView.ToArray();

					closestTarget = settings.attackRange + 1f;

					for (int index = 0; index < objectsInView.Length; index++) {
						potentialTargetDistance = Vector3.Distance(objectsInView[index].transform.position, transform.position);
						if (potentialTargetDistance < closestTarget) {
							closestTarget = potentialTargetDistance;
							potentialTarget = objectsInView[index];
						}
					}

					if (potentialTarget != currentTargetObject) {
						currentTarget = potentialTarget.GetComponent<EnemyController>();
					}
				}

				yield return new WaitForSecondsRealtime(aiDelay);
			}
		}

		public void LateUpdate()
		{
			if (hasTarget()) {
				laserSight.SetPosition(1, currentTarget.enemyCollider.bounds.center);
				if (attackDelayRemaining <= 0.0f) {
					if (settings.ammo.raycastBullet) {
						currentTarget.takeDamage(currentTarget.chest, (float)Mathf.CeilToInt(UnityEngine.Random.Range(settings.ammo.minDamage, settings.ammo.maxDamage)), settings.ammo);
					}
					else {
						GameObject bullet = DI_PoolManager.getPooledObject(settings.ammo.bulletPrefab);
						bullet.transform.position = bulletSpawnPoint.position;
						bullet.transform.LookAt(currentTarget.enemyCollider.bounds.center);
						Bullet projectile = bullet.GetComponent<Bullet>();
						projectile.initalize();
						projectile.damage = Mathf.CeilToInt(UnityEngine.Random.Range(settings.ammo.minDamage, settings.ammo.maxDamage));
						projectile.ammoType = settings.ammo;
					}
					DI_SFX.playRandomClipAtPoint(gameObject, transform.position, Vector3.Distance(Camera.main.transform.position, transform.position), settings.ammo.bulletSounds);
					GameObject muzzleFlash = DI_PoolManager.getPooledObject(settings.muzzleFlashPrefab);
					muzzleFlash.transform.position = bulletSpawnPoint.transform.position;
					muzzleFlash.transform.rotation = bulletSpawnPoint.transform.rotation;
					muzzleFlash.SetActive(true);
					attackDelayRemaining = attackDelay;
				}
			}
			else {
				laserSight.enabled = false;
			}

			if (attackDelayRemaining > 0.0f) {
				attackDelayRemaining -= DI.Core.DI_Time.getTimeDelta();
			}
		}

		public bool hasTarget()
		{
			if (currentTarget != null) {
				if (currentTarget.isAlive()) {
					return true;
				}
				else {
					currentTarget = null;
					return false;
				}
			}
			return false;
		}
	}
}