﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using Zenject;

using DI.Entities.NPC;
using DI.Entities.Properties;
using UnityEngine;
using DI.Mechanics.Loot;

namespace DI.Entities.Turrets
{
	public class Turret : PlaceableEntity
	{
		public float attackRange = 0.5f;
		public float viewAngle = 180f;
		public LootAmmo ammo;
		public float attackDelay = 0.2f;
		public GameObject muzzleFlashPrefab;
	}
}