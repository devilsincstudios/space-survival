﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using DI.Entities;
using UnityEngine;
using System;
using DI.Core.Debug;
using DI.Entities.Properties;

namespace DI.Entities.Turrets
{
	public class TurretAnimator : EntityAnimator
	{
		[HideInInspector]
		public float speed;
		[SerializeField]
		protected DI_AnimationProperty animationProperties;
		public bool updateIK = false;

		public void setLookAtPosition(Vector3 position)
		{
			animationProperties.headLookTarget = position;
		}

		public void setLookAtWeight(float weight)
		{
			animationProperties.headLookWeight = weight;
		}

		public new void OnEnable()
		{
			base.OnEnable();
			animationProperties.animator = animator;
		}

		public void animatorEnabled(bool enabled)
		{
			animationProperties.animator.enabled = enabled;
		}

		public void setIKPosition(AvatarIKGoal goal, Vector3 position)
		{
			switch (goal)
			{
				case AvatarIKGoal.LeftFoot:
					animationProperties.leftFootIKTarget = position;
					break;
				case AvatarIKGoal.LeftHand:
					animationProperties.leftHandIKTarget = position;
					break;
				case AvatarIKGoal.RightFoot:
					animationProperties.rightFootIKTarget = position;
					break;
				case AvatarIKGoal.RightHand:
					animationProperties.rightHandIKTarget = position;
					break;
			}
		}

		public void setIKWeight(AvatarIKGoal goal, float weight)
		{
			switch (goal)
			{
				case AvatarIKGoal.LeftFoot:
					animationProperties.leftFootWeight = weight;
					break;
				case AvatarIKGoal.LeftHand:
					animationProperties.leftHandWeight = weight;
					break;
				case AvatarIKGoal.RightFoot:
					animationProperties.rightFootWeight = weight;
					break;
				case AvatarIKGoal.RightHand:
					animationProperties.rightHandWeight = weight;
					break;
			}
		}

		public void OnAnimatorIK()
		{
			if (updateIK) {
				animator.SetLookAtPosition(animationProperties.headLookTarget);
				animator.SetLookAtWeight(animationProperties.headLookWeight);
				animator.SetIKPosition(AvatarIKGoal.LeftHand, animationProperties.leftHandIKTarget);
				animator.SetIKPosition(AvatarIKGoal.RightHand, animationProperties.rightHandIKTarget);
				animator.SetIKPositionWeight(AvatarIKGoal.LeftHand, animationProperties.leftHandWeight);
				animator.SetIKPositionWeight(AvatarIKGoal.RightHand, animationProperties.rightHandWeight);
			}
		}

		private void resetState()
		{
			animator.SetFloat("Speed", speed);
		}

		public void Update()
		{
			animator.SetFloat("Speed", speed);
		}

		public void setTrigger(string trigger)
		{
			animator.SetTrigger(trigger);
		}

		public void setLayerWeight(int layerIndex, float weight)
		{
			animator.SetLayerWeight(layerIndex, weight);
		}
	}
}