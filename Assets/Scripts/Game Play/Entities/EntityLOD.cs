// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using DI.Core.Behaviours;
using System.Collections.Generic;
using System.Collections;
using UnityEngine;

namespace DI.Entities
{
	public class EntityLOD : DI_MonoBehaviour
	{
		public bool updatingLOD = true;
		public float lodUpdateTime = 0.2f;

		public List<float> drawDistances;
		public List<Mesh> lodMeshes;

		public SkinnedMeshRenderer mesh;
		public int currentLOD = 0;

		public IEnumerator updateLOD()
		{
			yield return new WaitForSecondsRealtime(Random.Range(0, lodUpdateTime));

			while (true) {
				if (updatingLOD) {
					applyLOD();
				}
				yield return new WaitForSecondsRealtime(lodUpdateTime);
			}
		}

		protected void applyLOD()
		{
			for (int index = (drawDistances.Count - 1); index >= 0; index--) {
				if (drawDistances[index] <= Vector3.Distance(transform.position, Camera.main.transform.position)) {
					mesh.sharedMesh = lodMeshes[currentLOD];
					currentLOD = index;
					break;
				}
			}
		}

		public void OnEnable()
		{
			StartCoroutine(updateLOD());
		}

		public void OnDisable()
		{
			StopCoroutine(updateLOD());
		}

		public void OnBecameVisible()
		{
			updatingLOD = true;
			updateLOD();
		}

		public void OnBecameInvisible()
		{
			updatingLOD = false;
			mesh.sharedMesh = lodMeshes[lodMeshes.Count - 1];
		}
	}
}