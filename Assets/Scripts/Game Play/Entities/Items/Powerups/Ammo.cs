﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using DI.Core;
using UnityEngine;

namespace DI.Entities.Items
{
	public class Ammo : Powerup
	{
		public float amount = 5;

		public override void onTouch()
		{
			// add time
			Management.instance.player.addAmmo(amount);
			base.onDespawn();
		}
	}
}