﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using DI.Core;
using UnityEngine;

namespace DI.Entities.Items
{
	public class Time : Powerup
	{
		public float amount = 15f;

		public override void onTouch()
		{
			// add time
			Management.instance.timer.addTime(amount);
			base.onDespawn();
		}
	}
}