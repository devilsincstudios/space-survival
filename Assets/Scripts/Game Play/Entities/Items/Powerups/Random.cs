﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using DI.Core;
using UnityEngine;
using DI.Core.Events;

namespace DI.Entities.Items
{
	public class Random : Powerup
	{
		public float amount = 500f;

		public override void onTouch()
		{
			// For now, just add points
			DI_EventCenter<float>.invoke("OnPointsIncrease", amount);
			base.onDespawn();
		}
	}
}