﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using DI.Core.Behaviours;
using UnityEngine;

namespace DI.Entities.Items
{
	public class PowerupEntity : DI_MonoBehaviour
	{
		public Powerup power;

		public void OnEnable()
		{
			power.setEntity(gameObject);
		}

		public void consume()
		{
			power.onTouch();
			gameObject.SetActive(false);
		}
	}
}