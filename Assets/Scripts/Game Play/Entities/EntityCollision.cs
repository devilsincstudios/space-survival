﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using DI.Core.Behaviours;

namespace DI.Entities
{
	public class EntityCollision : DI_MonoBehaviour
	{
		public HumanBodyBones bone;
		public void OnCollisionEnter(Collision other)
		{
			for (int index = 0; index < other.contacts.Length; index++) {
				Debug.Log(other.contacts[index].thisCollider.name + " collided with: " + other.contacts[index].otherCollider.name);
			}
		}
	}
}