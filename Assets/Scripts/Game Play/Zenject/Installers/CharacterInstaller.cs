using Zenject;
using DI.Entities.Player;
using UnityEngine;
using System.Collections;
using DI.Core.Input;

public class CharacterInstaller : MonoInstaller
{
	public Player playerSettings;
	public PlayerAnimator playerAnimator;
	public MouseLookSettings mouseLookSettings;

	public override void InstallBindings()
	{
		playerSettings = Instantiate(playerSettings);
		Container.Bind<Rigidbody>().WithId("Player Rigidbody").FromInstance(GameObject.FindGameObjectWithTag("Player").GetComponent<Rigidbody>()).AsCached();
		Container.Bind<Player>().FromInstance(playerSettings).AsTransient();
		Container.Bind<MouseLookSettings>().FromInstance(mouseLookSettings).AsCached();
		Container.Bind<PlayerAnimator>().FromInstance(playerAnimator).AsTransient();
		Container.Bind<PlayerMotor>().FromNew().AsSingle();
		Container.BindAllInterfaces<PlayerController>().To<PlayerController>().AsSingle();
		Container.BindAllInterfaces<PlayerMotor>().To<PlayerMotor>().FromResolve();
		StartCoroutine(updateEntity());
	}

	public IEnumerator updateEntity()
	{
		yield return null;
		Container.Resolve<Player>().setEntityBody(Container.Resolve<Rigidbody>("Player Rigidbody"));
	}
}