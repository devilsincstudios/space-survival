// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine.SceneManagement;
using DI.Core.Behaviours;

namespace DI.Core.Scene
{
	public class LoadSceneAfterDelay : DI_MonoBehaviour
	{
		public string nextScene;
		public float delayInSeconds = 5.0f;
		private float timeElapsed = 0.0f;

		public void Update()
		{
			if (timeElapsed < delayInSeconds) {
				timeElapsed += DI_Time.getTimeDelta();
			}
			else {
				SceneManager.LoadScene(nextScene, LoadSceneMode.Single);
			}
		}
	}
}