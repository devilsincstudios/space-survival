// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine;

[AddComponentMenu("Effects/Lights/Fire Flicker")]
public class FireFlicker : MonoBehaviour
{
	public float curveTime = 0.0f;
	public AnimationCurve curve;

	public void OnEnable()
	{
		curveTime = UnityEngine.Random.Range(1.0f, 9.0f);
	}

	public void Update()
	{
		curveTime += Time.deltaTime;
		GetComponent<Light>().intensity = curve.Evaluate(curveTime);
		if (curveTime >= curve.length) {
			curveTime = 0.0f;
		}
	}
}