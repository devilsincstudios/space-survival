﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using System.Collections.Generic;
using System;

namespace DI.Core.GameState
{
	[Serializable]
	public class GameState
	{
		[SerializeField]
		protected float points;
		[SerializeField]
		protected float health;
		[SerializeField]
		protected List<string> mapSeeds = new List<string>();
		[SerializeField]
		protected bool gameOver = false;
		[SerializeField]
		protected float ammo;

		public void setGameOver(bool value)
		{
			gameOver = value;
		}

		public bool getGameOver()
		{
			return gameOver;
		}

		public void addPoints(float amount)
		{
			points += amount;
		}

		public float getPoints()
		{
			return points;
		}

		public void setAmmo(float amount)
		{
			ammo = amount;
		}

		public float getAmmo()
		{
			return ammo;
		}

		public void setHealth(float amount)
		{
			health = amount;
		}

		public float getHealth()
		{
			return health;
		}


		public List<string> getSeeds()
		{
			return mapSeeds;
		}

		public void addMapSeed(string seed)
		{
			mapSeeds.Add(seed);
		}

		public void reset()
		{
			points = 0f;
			health = 0f;
			ammo = 15f;
			//mapSeeds.Clear();
			gameOver = true;
		}
	}
}