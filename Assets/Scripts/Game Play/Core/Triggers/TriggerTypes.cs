﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

namespace DI.Core.Triggers
{
	public enum TriggerTypes
	{
		collision,
		interval,
		time,
		levelLoad,
		startLevel,
		completeLevel,
		failLevel,
		playerDeath,
		playerHit,
		enemyDeath,
		enemyHit,
		scoreIncrease,
		achievementUnlocked,
		achievementProgress,
		notification,
		unset,
		enterCombat,
		exitCombat,
	}
}