﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using DI.Core.Behaviours;
using DI.Core.Events;

using DI.Entities.Enemy;
using DI.Entities.Player;
using DI.Entities.Limbs;
using DI.Core.Actions;

namespace DI.Core.Triggers
{
	[Serializable]
	public class TriggerListener : DI_MonoBehaviour
	{
		public List<Trigger> triggers;
		public MapGen.MapGenerator generator;

		public void OnEnable()
		{
			DI_EventCenter.addListener("LevelLoad", levelLoad);
			DI_EventCenter.addListener("LevelStart", levelStart);
			DI_EventCenter.addListener("LevelCompleted", levelCompleted);
			DI_EventCenter.addListener("LevelFailed", levelFailed);
			DI_EventCenter.addListener("OnTimeExpired", levelFailed);
			DI_EventCenter<Player>.addListener("OnPlayerDeath", playerDeath);
			DI_EventCenter<Player>.addListener("OnHit", playerHit);
			DI_EventCenter<Enemy, Limb>.addListener("OnKill", enemyDeath);
			DI_EventCenter<Enemy, Limb>.addListener("OnHit", enemyHit);
			DI_EventCenter<float>.addListener("OnPointsIncrease", scoreIncrease);
			DI_EventCenter.addListener("OnEnterCombat", enterCombat);
			DI_EventCenter.addListener("OnExitCombat", exitCombat);
			startTimeBasedTriggers();
			StartCoroutine(checkLoadingStatus());
		}

		public void OnDisable()
		{
			DI_EventCenter.removeListener("LevelLoad", levelLoad);
			DI_EventCenter.removeListener("LevelStart", levelStart);
			DI_EventCenter.removeListener("LevelCompleted", levelCompleted);
			DI_EventCenter.removeListener("LevelFailed", levelFailed);
			DI_EventCenter.removeListener("OnTimeExpired", levelFailed);
			DI_EventCenter<Player>.removeListener("OnPlayerDeath", playerDeath);
			DI_EventCenter<Player>.removeListener("OnHit", playerHit);
			DI_EventCenter<Enemy, Limb>.removeListener("OnKill", enemyDeath);
			DI_EventCenter<Enemy, Limb>.removeListener("OnHit", enemyHit);
			DI_EventCenter<float>.removeListener("OnPointsIncrease", scoreIncrease);
			DI_EventCenter.removeListener("OnEnterCombat", enterCombat);
			DI_EventCenter.removeListener("OnExitCombat", exitCombat);
			StopAllCoroutines();
		}

		public IEnumerator checkLoadingStatus()
		{
			while (!generator.buildComplete()) {
				yield return new WaitForSecondsRealtime(0.1f);
				DI_EventCenter.invoke("LevelLoad");
			}
		}

		public List<Trigger> getTriggersOfType(TriggerTypes type)
		{
			List<Trigger> results = new List<Trigger>();
			for (int index = 0; index < triggers.Count; index++) {
				if (triggers[index].type == type) {
					results.Add(triggers[index]);
				}
			}
			return results;
		}
		public void runTriggersOfType(TriggerTypes type)
		{
			List<Trigger> selection = getTriggersOfType(type);
			for (int index = 0; index < selection.Count; index++) {
				selection[index].doAction();
			}
		}

		#region Time Based Triggers
		public void startTimeBasedTriggers()
		{
			List<Trigger> selectedTriggers = getTriggersOfType(TriggerTypes.interval);
			for (int index = 0; index < selectedTriggers.Count; index++) {
				StartCoroutine(interval(selectedTriggers[index]));
			}

			selectedTriggers = getTriggersOfType(TriggerTypes.interval);
			for (int index = 0; index < selectedTriggers.Count; index++) {
				StartCoroutine(time(selectedTriggers[index]));
			}
		}

		public IEnumerator interval(Trigger trigger)
		{
			while (true) {
				yield return new WaitForSeconds(trigger.time);
				trigger.doAction();
			}
		}
		public IEnumerator time(Trigger trigger)
		{
			yield return new WaitForSeconds(trigger.time);
			trigger.doAction();
		}
		#endregion

		#region Level Triggers
		public void levelLoad()
		{
			runTriggersOfType(TriggerTypes.levelLoad);
		}
		public void levelStart()
		{
			runTriggersOfType(TriggerTypes.startLevel);
		}
		public void levelCompleted()
		{
			runTriggersOfType(TriggerTypes.completeLevel);
		}
		public void levelFailed()
		{
			runTriggersOfType(TriggerTypes.failLevel);
		}
		#endregion

		#region Player Triggers
		public void playerDeath(Player player)
		{
			runTriggersOfType(TriggerTypes.playerDeath);
		}
		// The player has taken damage.
		public void playerHit(Player player)
		{
			runTriggersOfType(TriggerTypes.playerHit);
		}
		#endregion

		#region Enemy Triggers
		public void enemyDeath(Enemy enemy, Limb limb)
		{
			runTriggersOfType(TriggerTypes.enemyDeath);
		}
		// An enemy has taken damage.
		public void enemyHit(Enemy enemy, Limb limb)
		{
			float points = 0f;
			if (limb.isHead()) {
				points = enemy.pointValue * enemy.headShotMultipler;
			}
			else if (limb.isBody()) {
				points = enemy.pointValue * enemy.bodyShotMultipler;
			}
			else if (limb.isArm()) {
				points = enemy.pointValue * enemy.armShotMultipler;
			}
			else if (limb.isLeg()) {
				points = enemy.pointValue * enemy.legShotMultipler;
			}
			UnityEngine.Debug.Log("Calling points increase: " + points);
			DI_EventCenter<float>.invoke("OnPointsIncrease", points);

			runTriggersOfType(TriggerTypes.enemyHit);
		}
		#endregion

		#region Score Trigger
		public void scoreIncrease(float amount)
		{
			UnityEngine.Debug.Log("Score Increase: " + amount);
			List<Trigger> scoreTriggers = getTriggersOfType(TriggerTypes.scoreIncrease);
			for (int index = 0; index < scoreTriggers.Count; index++) {
				Trigger scoreTrigger = scoreTriggers[index];
				for (int actionIndex = 0; actionIndex < scoreTrigger.actions.Count; actionIndex++) {
					if (scoreTrigger.actions[actionIndex].getType() == DI.Core.Actions.ActionTypes.score_add_points) {
						ActionAddPoints addPoints = (ActionAddPoints)scoreTrigger.actions[actionIndex];
						UnityEngine.Debug.Log("Score Increase: " + amount + " calling doAction()");
						addPoints.points = amount;
						addPoints.doAction();
					}
					else {
						scoreTrigger.doAction();
					}
				}
			}
		}
		#endregion

		#region Combat Triggers
		public void enterCombat()
		{
			runTriggersOfType(TriggerTypes.enterCombat);
		}
		public void exitCombat()
		{
			runTriggersOfType(TriggerTypes.exitCombat);
		}
		#endregion

		#region Achievement Triggers
		public void achievementUnlocked()
		{
			runTriggersOfType(TriggerTypes.achievementUnlocked);
		}
		public void achievementProgress()
		{
			runTriggersOfType(TriggerTypes.achievementProgress);
		}
		#endregion

		#region Notification Trigger
		public void notification()
		{
			runTriggersOfType(TriggerTypes.notification);
		}
		#endregion
	}
}