﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using System;
using System.Collections.Generic;
using DI.Core.Actions;

namespace DI.Core.Triggers
{
	[Serializable]
	public class Trigger
	{
		public bool isSingleUse = false;
		public bool hasFired = false;
		public List<DI.Core.Actions.Action> actions;
		public TriggerTypes type;
		public float time = 10f;

		public virtual TriggerTypes getType()
		{
			return type;
		}

		public virtual void doAction()
		{
			for (int index = 0; index < actions.Count; index++) {
				actions[index].doAction();
			}
		}
	}
}
