﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using System.Collections.Generic;
using DI.Core.Behaviours;
using DI.Core.Actions;

namespace DI.Core.Triggers
{
	public enum TriggerType
	{
		Enter,
		Exit,
		Stay
	}

	public class ActionsTrigger : DI_MonoBehaviour
	{
		public List<string> targetTags;
		public List<Action> actions;
		public TriggerType type;
		public bool singleUse = true;
		[SerializeField]
		protected bool triggered = false;

		public void OnTriggerEnter(Collider other)
		{
			if (type == TriggerType.Enter) {
				fireTrigger(other);
			}
		}
		public void OnTriggerExit(Collider other)
		{
			if (type == TriggerType.Exit) {
				fireTrigger(other);
			}
		}
		public void OnTriggerStay(Collider other)
		{
			if (type == TriggerType.Stay) {
				fireTrigger(other);
			}
		}

		private void fireTrigger(Collider other)
		{
			if (!singleUse || (singleUse && triggered == false)) {
				for (int index = 0; index < targetTags.Count; index++) {
					if (other.CompareTag(targetTags[index])) {
						for (int actionIndex = 0; actionIndex < actions.Count; actionIndex++) {
							actions[actionIndex].doAction();
						}
						triggered = true;
						break;
					}
				}
			}
		}
	}
}