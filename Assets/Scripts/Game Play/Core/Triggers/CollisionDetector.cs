// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine;

using DI.Core.Behaviours;
using DI.Core.Debug;
using DI.Core.Events;

namespace DI.Core.Triggers
{
	[AddComponentMenu("Devil's Inc Studios/Triggers/CollisionDetector")]
	public class CollisionDetector : DI_MonoBehaviour
	{
		public void OnCollisionEnter(Collision other)
		{
			DI_EventCenter<Collision>.invoke("OnCollisionEnter", other);
			DI_Debug.writeLog(DI_DebugLevel.INFO, "OnCollisionEnter: " + other.gameObject.name);
		}

		public void OnCollisionExit(Collision other)
		{
			DI_EventCenter<Collision>.invoke("OnCollisionExit", other);
			DI_Debug.writeLog(DI_DebugLevel.INFO, "OnCollisionExit: " + other.gameObject.name);
		}

		public void OnCollisionStay(Collision other)
		{
			DI_EventCenter<Collision>.invoke("OnCollisionStay", other);
			DI_Debug.writeLog(DI_DebugLevel.INFO, "OnCollisionStay: " + other.gameObject.name);
		}
	}
}