﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

using DI.Core.Behaviours;
using DI.Entities.Player;
using DI.Mechanics.Timer;
using MapGen;
using UnityEngine.UI;
using DI.Core.Events;

namespace DI.Core
{
	[AddComponentMenu("Devil's Inc Studios/Managers/Management")]
	public class Management : DI_MonoBehaviourSingleton<Management>
	{
		public PlayerController player;
		public LevelTimer timer;
		public GameState.GameState state;
		public float timerValue = 90f;
		public float ammoAwardedOnLevelComplete = 15f;
		public bool inMenu = false;

		[Header("UI")]
		public Image loadingBarFill;
		public Image loadingBarBackground;
		public Text loadingBarText;
		public GameObject inGameMenu;

		[Header("Cheats")]
		public bool aiEnabled = true;
		public bool godMode = false;
		public bool unlimitedAmmo = false;
		public bool unlimitedTime = false;
		public GameObject cheatMenu;

		public void OnEnable()
		{
			// If we have another instance active, destroy it and replace it with this one.
			if (instance != null) {
				Destroy(instance);
			}

			makeSingleton(this);
			string stateData = PlayerPrefs.GetString("GameState", "");
			if (stateData != "") {
				state = JsonUtility.FromJson<GameState.GameState>(stateData);
				if (state.getGameOver()) {
					state.reset();
				}
				state.setGameOver(true);
			}
			else {
				state = new DI.Core.GameState.GameState();
				state.reset();
			}
			SceneManager.sceneLoaded += OnSceneLoaded;
		}

		public void onFailLevel()
		{
			UnityEngine.Debug.Log("OnFailLevel");
			state.reset();
		}

		public void onCompleteLevel()
		{
			UnityEngine.Debug.Log("OnCompleteLevel");
			//float points = 
			setHealth(player.getHealth());
			state.setAmmo(15f);
			setGameOver(false);
		}

		public IEnumerator delayedStart()
		{
			while (!MapGenerator.instance.buildComplete()) {
				yield return new WaitForSecondsRealtime(0.1f);
				loadingBarFill.fillAmount = MapGenerator.instance.buildProgress();
				loadingBarText.text = MapGenerator.instance.currentStatus() + " : " + (MapGenerator.instance.buildProgress() * 100f) + "% complete";
			}
			player.moveToSpawn();
			loadingBarBackground.gameObject.SetActive(false);
			yield return null;
		}

		public void OnDisable()
		{
			SceneManager.sceneLoaded -= OnSceneLoaded;
			PlayerPrefs.SetString("GameState", JsonUtility.ToJson(state));
		}

		public void setGameOver(bool gameover)
		{
			state.setGameOver(gameover);
		}

		private void OnSceneLoaded(UnityEngine.SceneManagement.Scene scene, LoadSceneMode mode)
		{
			timer.setTime(timerValue);
			player.setHealth(state.getHealth());
			StartCoroutine(delayedStart());
		}

		public float getPoints()
		{
			return state.getPoints();
		}

		public void addPoints(float points)
		{
			state.addPoints(points);
			UnityEngine.Debug.Log("Adding points: " + points);
		}

		public float getHealth()
		{
			return state.getHealth();
		}

		public void setHealth(float amount)
		{
			state.setHealth(amount);
		}

		public void closeMenu()
		{
			inGameMenu.SetActive(false);
			inMenu = false;
			Cursor.lockState = CursorLockMode.Locked;
			Cursor.visible = false;
			player.inControl = true;
		}

		public void LateUpdate()
		{
			if (UnityEngine.Input.GetKeyDown(KeyCode.Escape)) {
				if (inMenu && inGameMenu.activeInHierarchy) {
					inGameMenu.SetActive(false);
					inMenu = false;
					Cursor.lockState = CursorLockMode.Locked;
					Cursor.visible = false;
					player.inControl = true;
				}
				else {
					cheatMenu.SetActive(false);
					inGameMenu.SetActive(true);
					inMenu = true;
					Cursor.lockState = CursorLockMode.None;
					Cursor.visible = true;
					player.inControl = false;
				}
			}

			if (UnityEngine.Input.GetKeyDown(KeyCode.Keypad9)) {
				if (cheatMenu.activeInHierarchy) {
					cheatMenu.SetActive(false);
					Cursor.lockState = CursorLockMode.Locked;
					Cursor.visible = false;
					inMenu = false;
					player.inControl = true;
				}
				else {
					if (!inGameMenu.activeInHierarchy) {
						cheatMenu.SetActive(true);
						Cursor.lockState = CursorLockMode.None;
						Cursor.visible = true;
						inMenu = true;
						player.inControl = false;
					}
				}
			}
		}
	}
}