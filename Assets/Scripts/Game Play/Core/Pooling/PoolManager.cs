// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

using DI.Core.Behaviours;

namespace DI.Core.Pooling
{
	[AddComponentMenu("Devil's Inc Studios/Managers/Pooling")]
	public class PoolManager : DI_MonoBehaviourSingleton<PoolManager>
	{
		public List<GameObject> prefabsToManage;
		public bool willGrow = true;
		public int startingSize = 1;

		public void Awake()
		{
			// If we have another instance active, destroy it and replace it with this one.
			if (instance != null) {
				Destroy(instance);
			}
			makeSingleton(this);
		}

		public void reload()
		{
			DI_PoolManager.reset();
			DI_PoolManager.setWillGrow(willGrow);
			DI_PoolManager.setStartingSize(startingSize);
			DI_PoolManager.initalize();

			for (int objectId = 0; objectId < prefabsToManage.Count; objectId++) {
				DI_PoolManager.addObjectToManagedPool(prefabsToManage[objectId]);
			}
		}

		public void OnEnable()
		{
			log(DI.Core.Debug.DI_DebugLevel.MEDIUM, "OnEnable: " + this.name);
			reload();
			SceneManager.sceneLoaded += OnLevelFinishedLoading;
		}

		public void OnLevelFinishedLoading(UnityEngine.SceneManagement.Scene scene, LoadSceneMode mode)
		{
			reload();
		}
	}
}