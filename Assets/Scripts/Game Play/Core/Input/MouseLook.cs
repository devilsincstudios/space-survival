using UnityEngine;
using System.Collections;

[AddComponentMenu("Camera-Control/Mouse Look")]
public class MouseLook : MonoBehaviour {
	public GameObject cameraPivot;
	public GameObject aimingTarget;
	public GameObject spine;
	public GameObject spineLookHelper;

	public float sensitivityX = 4F;
	public float sensitivityY = 4F;

	public float minimumX = -360F;
	public float maximumX = 360F;

	public float minimumY = -30F;
	public float maximumY = 30F;
	float rotationY = 0F;
	float rotationX = 0f;
	void OnEnable()
	{
//		if (Input.GetJoystickNames().Length > 0) {
//			usingController = true;
//		}
		//DI_Events.EventCenter.addListener("OnOptionsChanged", handleOnOptionsChanged);
		//sensitivityX = PlayerPrefs.GetFloat("Mouse Sensitivity", 7.0f);
		//sensitivityY = PlayerPrefs.GetFloat("Mouse Sensitivity", 7.0f);
	}

	public void OnDisable()
	{
		//DI_Events.EventCenter.removeListener("OnOptionsChanged", handleOnOptionsChanged);
	}

	public void handleOnOptionsChanged()
	{
		//sensitivityX = PlayerPrefs.GetFloat("Mouse Sensitivity", 7.0f);
		//sensitivityY = PlayerPrefs.GetFloat("Mouse Sensitivity", 7.0f);
	}
	
	public void LateUpdate()
	{
		rotationX = transform.localEulerAngles.y + Input.GetAxis("Mouse X") * sensitivityX;
		rotationY += Input.GetAxis("Mouse Y") * sensitivityY;

		rotationY = Mathf.Clamp (rotationY, minimumY, maximumY);
		
		transform.localEulerAngles = new Vector3(0f, rotationX, 0f);
		cameraPivot.transform.localEulerAngles = new Vector3(-rotationY, cameraPivot.transform.localEulerAngles.y, cameraPivot.transform.localEulerAngles.z);
		spineLookHelper.transform.LookAt(aimingTarget.transform.position);
		//spine.transform.LookAt(aimingTarget.transform.transform);
		spine.transform.localEulerAngles = new Vector3(
			spine.transform.localEulerAngles.x + spineLookHelper.transform.localEulerAngles.x,
			spine.transform.localEulerAngles.y,
			spine.transform.localEulerAngles.z
		);
	}
}