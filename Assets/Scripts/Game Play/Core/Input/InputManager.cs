// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using System.Collections.Generic;
using System;

using DI.Core.Behaviours;
using DI.Core.Events;
using DI.Core.Debug;
using UnityEngine.SceneManagement;

namespace DI.Core.Input
{
	[AddComponentMenu("Devil's Inc Studios/Managers/Input")]
	public class InputManager : DI_MonoBehaviourSingleton<InputManager>
	{
		[Header("Debug bindings.")]
		public List<DI_KeyBind> bindingsPlayerOne;
		public List<DI_KeyBind> bindingsPlayerTwo;
		public List<DI_ControllerType> controllerTypes;

		[Header("Input Processing")]
		[Tooltip("Should we be processing input updates?")]
		public bool updatingInput = true;

		public void Awake()
		{
			// If we have another instance active, destroy it and replace it with this one.
			if (instance != null) {
				Destroy(instance);
			}
			makeSingleton(this);
		}

		public void OnEnable()
		{
			initPlayerBindings(0);
			initPlayerBindings(1);

			bindingsPlayerOne = DI_BindManager.bindings.boundKeys[0];
			bindingsPlayerTwo = DI_BindManager.bindings.boundKeys[1];
			controllerTypes = DI_BindManager.bindings.controllerTypes;
			SceneManager.sceneLoaded += OnLevelFinishedLoading;
		}

		public void OnLevelFinishedLoading(UnityEngine.SceneManagement.Scene scene, LoadSceneMode mode)
		{
			updatingInput = false;
			initPlayerBindings(0);
			initPlayerBindings(1);

			bindingsPlayerOne = DI_BindManager.bindings.boundKeys[0];
			bindingsPlayerTwo = DI_BindManager.bindings.boundKeys[1];
			controllerTypes = DI_BindManager.bindings.controllerTypes;
			updatingInput = true;
		}

		public void OnDisable()
		{
			DI_BindManager.bindings.boundKeys[0] = bindingsPlayerOne;
			DI_BindManager.bindings.boundKeys[1] = bindingsPlayerTwo;
			DI_BindManager.saveBoundKeys();
		}

		public void initPlayerBindings(int playerId)
		{
			IDictionary<string, string> defaultKeyBindTypes = new Dictionary<string, string>()
			{
				{ "Vertical", "BIND_AXIS" },
				{ "Horizontal", "BIND_AXIS" },
				{ "Sneak", "BIND_AXIS" },
				{ "Jump", "BIND_AXIS" },
				{ "Run", "BIND_AXIS" },
				{ "Interact", "BIND_AXIS" },
				{ "Fire", "BIND_AXIS" },
				{ "Aim", "BIND_AXIS" },
				{ "Select Ability One", "BIND_AXIS" },
				{ "Select Ability Two", "BIND_AXIS" },
				{ "Select Ability Three", "BIND_AXIS" },
				{ "Select Ability Four", "BIND_AXIS" },
			};

			foreach (KeyValuePair<string, string> defaultType in defaultKeyBindTypes)
			{
				// Is there a binding already saved?
				if (!DI_BindManager.loadBoundKey(defaultType.Key, playerId))
				{
					// If not, add the default binding.
					DI_BindManager.setKey(new DI_KeyBind(defaultType.Key, DI_KeyBind.typeStringToEnum(defaultType.Value), defaultType.Key, playerId));
				}
			}
			DI_BindManager.setControllerType(DI_ControllerType.KEYBOARD, playerId);
		}

		public void Update()
		{
			if (updatingInput) {

				// TODO check what players are actually playing and ignore events from players not in the game.
				for (int playerIndex = 0; playerIndex < DI_BindManager.bindings.boundKeys.Count; playerIndex++) {
					for (int bindIndex = 0; bindIndex < DI_BindManager.bindings.boundKeys[playerIndex].Count; bindIndex++) {
						switch (DI_BindManager.bindings.boundKeys[playerIndex][bindIndex].bindType) {
						case DI_KeyBindType.BIND_AXIS:
							float axisTilt = UnityEngine.Input.GetAxis(DI_BindManager.bindings.boundKeys[playerIndex][bindIndex].bindKey);
							if (axisTilt > 0.0f) {
								if ( DI_BindManager.getKeyState(bindIndex, playerIndex) == DI_KeyState.AXIS_NOT_PRESSED ||  DI_BindManager.getKeyState(bindIndex, playerIndex) == DI_KeyState.AXIS_HELD_NEGATIVE) {
									DI_BindManager.setKeyState(bindIndex, playerIndex, DI_KeyState.AXIS_PRESSED_POSITIVE);
								}
								else if (DI_BindManager.getKeyState(bindIndex, playerIndex) == DI_KeyState.AXIS_PRESSED_POSITIVE) {
									DI_BindManager.setKeyState(bindIndex, playerIndex, DI_KeyState.AXIS_HELD_POSITIVE);
								}
								else {
									DI_BindManager.setKeyState(bindIndex, playerIndex, DI_KeyState.AXIS_HELD_POSITIVE);
								}
							}
							else if (axisTilt < 0.0f) {
								if ( DI_BindManager.getKeyState(bindIndex, playerIndex) == DI_KeyState.AXIS_NOT_PRESSED ||  DI_BindManager.getKeyState(bindIndex, playerIndex) == DI_KeyState.AXIS_HELD_NEGATIVE) {
									DI_BindManager.setKeyState(bindIndex, playerIndex, DI_KeyState.AXIS_PRESSED_NEGATIVE);
								}
								else if (DI_BindManager.getKeyState(bindIndex, playerIndex) == DI_KeyState.AXIS_PRESSED_NEGATIVE) {
									DI_BindManager.setKeyState(bindIndex, playerIndex, DI_KeyState.AXIS_HELD_NEGATIVE);
								}
								else {
									DI_BindManager.setKeyState(bindIndex, playerIndex, DI_KeyState.AXIS_HELD_NEGATIVE);
								}
							}
							else {
								DI_BindManager.setKeyState(bindIndex, playerIndex, DI_KeyState.AXIS_NOT_PRESSED);
							}
							break;
						case DI_KeyBindType.BIND_BUTTON:
							bool buttonDown = UnityEngine.Input.GetButtonDown(DI_BindManager.bindings.boundKeys[playerIndex][bindIndex].bindKey);
							if (buttonDown && DI_BindManager.bindings.boundKeys[playerIndex][bindIndex].bindState == DI_KeyState.KEY_NOT_PRESSED) {
								DI_BindManager.bindings.boundKeys[playerIndex][bindIndex].bindState = DI_KeyState.KEY_PRESSED;
							}
							else if (buttonDown && DI_BindManager.bindings.boundKeys[playerIndex][bindIndex].bindState == DI_KeyState.KEY_PRESSED) {
								DI_BindManager.bindings.boundKeys[playerIndex][bindIndex].bindState = DI_KeyState.KEY_HELD;
							}
							else {
								DI_BindManager.bindings.boundKeys[playerIndex][bindIndex].bindState = DI_KeyState.KEY_NOT_PRESSED;
							}
							break;
						case DI_KeyBindType.BIND_KEY:
							bool keyDown = UnityEngine.Input.GetKeyDown(DI_BindManager.bindings.boundKeys[playerIndex][bindIndex].bindKey);
							if (keyDown && DI_BindManager.bindings.boundKeys[playerIndex][bindIndex].bindState == DI_KeyState.KEY_NOT_PRESSED) {
								DI_BindManager.bindings.boundKeys[playerIndex][bindIndex].bindState = DI_KeyState.KEY_PRESSED;
							}
							else if (keyDown && DI_BindManager.bindings.boundKeys[playerIndex][bindIndex].bindState == DI_KeyState.KEY_PRESSED) {
								DI_BindManager.bindings.boundKeys[playerIndex][bindIndex].bindState = DI_KeyState.KEY_HELD;
							}
							else {
								DI_BindManager.bindings.boundKeys[playerIndex][bindIndex].bindState = DI_KeyState.KEY_NOT_PRESSED;
							}
							break;
						default:
							log(DI_DebugLevel.CRITICAL, "Hit unexpected bind type when updating input.");
							break;
						}
					}
				}
			}
		}
	}
}