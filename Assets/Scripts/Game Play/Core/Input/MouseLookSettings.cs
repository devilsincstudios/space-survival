﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine;

namespace DI.Core.Input {
	public class MouseLookSettings : ScriptableObject
	{
		public float minimumX = -360F;
		public float maximumX = 360F;
		public float minimumY = -30F;
		public float maximumY = 30F;
	}
}
