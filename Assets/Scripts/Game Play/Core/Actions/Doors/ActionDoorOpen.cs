﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using DI.Core.Behaviours;
using DI.Core.Events;
using DI.SFX;
using DI.Entities.Door;

namespace DI.Core.Actions
{
	public class ActionDoorOpen : Action
	{
		public DoorController door;

		public override ActionTypes getType()
		{
			return ActionTypes.door_open;
		}
		public override void doAction()
		{
			door.openDoor();
		}
	}
}