﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using DI.Core.Behaviours;
using DI.Core.Events;
using DI.SFX;
using DI.Mechanics.Interactions;

namespace DI.Core.Actions
{
	public class ActionDoorLock : Action
	{
		public LockedDoor door;

		public override ActionTypes getType()
		{
			return ActionTypes.door_unlock;
		}
		public override void doAction()
		{
			door.isLocked = false;
			DI_SFX.playClipAtPoint(door.gameObject, door.transform.position, door.unlockSound);
		}
	}
}