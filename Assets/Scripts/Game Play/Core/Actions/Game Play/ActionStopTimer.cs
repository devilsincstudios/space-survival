﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using DI.Core.Behaviours;
using DI.Mechanics.Timer;

namespace DI.Core.Actions
{
	public class ActionStopTimer : Action
	{
		public LevelTimer timer;

		public override ActionTypes getType()
		{
			return ActionTypes.gameplay;
		}

		public override void doAction()
		{
			// TODO replace with static instance or proper DI usage.
			timer = GameObject.Find("Management").GetComponent<LevelTimer>();
			timer.stopTimer();
		}
	}
}