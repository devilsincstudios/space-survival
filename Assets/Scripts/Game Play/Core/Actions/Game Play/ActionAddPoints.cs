﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using DI.Core.Behaviours;
using DI.Core.Events;

namespace DI.Core.Actions
{
	public class ActionAddPoints : Action
	{
		public float points = 500f;

		public override ActionTypes getType()
		{
			return ActionTypes.score_add_points;
		}

		public override void doAction()
		{

			Management.instance.addPoints(points);
		}
	}
}