﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using DI.Core.Behaviours;
using DI.Mechanics.Waves;

using UnityEngine;

namespace DI.Core.Actions
{
	public class ActionStartSpawning : Action
	{
		public EnemySpawner spawner;

		public override ActionTypes getType()
		{
			return ActionTypes.gameplay;
		}

		public override void doAction()
		{
			// TODO replace with static instance or proper DI usage.
			spawner = GameObject.Find("Enemy Manager").GetComponent<EnemySpawner>();
			spawner.canSpawn = true;
		}
	}
}