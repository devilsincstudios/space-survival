﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using DI.Core.Behaviours;

namespace DI.Core.Actions
{
	public class ActionDisableMonoBehaviour : Action
	{
		public MonoBehaviour target;
		public override ActionTypes getType()
		{
			return ActionTypes.behaviour_disable;
		}
		public override void doAction()
		{
			target.enabled = false;
		}
	}
}