﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

namespace DI.Core.Actions
{
	public enum ActionTypes
	{
		audiocue_start,
		audiocue_end,
		behaviour_disable,
		behaviour_enable,
		dialog_play,
		dialog_stop,
		door_lock,
		door_unlock,
		door_open,
		door_close,
		music_change,
		music_play,
		music_stop,
		gameplay,
		object_create,
		object_destory,
		object_disable,
		object_enable,
		objective_add,
		objective_hide,
		objective_remove,
		objective_reveal,
		sfx_play,
		ui_fade_in,
		ui_fade_out,
		score_add_points,
		unset,
	}
}