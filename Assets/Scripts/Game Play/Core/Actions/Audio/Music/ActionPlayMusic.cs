﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using DI.Core.Behaviours;
using DI.Music;
using DI.Core.Events;

namespace DI.Core.Actions
{
	public class ActionPlayMusic : Action
	{
		public override ActionTypes getType()
		{
			return ActionTypes.music_play;
		}
		public override void doAction()
		{
			DI_EventCenter.invoke("OnMusicPlay");
		}
	}
}