﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using DI.Core.Behaviours;
using DI.Music;
using DI.Core.Events;

namespace DI.Core.Actions
{
	public class ActionStopMusic : Action
	{
		public override ActionTypes getType()
		{
			return ActionTypes.music_stop;
		}
		public override void doAction()
		{
			DI_EventCenter.invoke("OnMusicStop");
		}
	}
}