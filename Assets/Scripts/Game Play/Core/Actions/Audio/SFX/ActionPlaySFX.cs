﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using DI.Core.Behaviours;
using DI.SFX;

namespace DI.Core.Actions
{
	public class ActionPlaySFX : Action
	{
		public DI_SFXClipProperties sfx;
		public Transform location;

		public override ActionTypes getType()
		{
			return ActionTypes.sfx_play;
		}
		public override void doAction()
		{
			DI_SFX.playClipAtPoint(gameObject, location.position, sfx);
		}
	}
}