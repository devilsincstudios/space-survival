﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using System;
using DI.Core.Behaviours;
using UnityEngine;

namespace DI.Core.Actions
{
	public class Action : DI_MonoBehaviour
	{
		public virtual ActionTypes getType()
		{
			return ActionTypes.unset;
		}

		public virtual void doAction()
		{
			UnityEngine.Debug.Log(this.GetType().ToString());
		}
	}
}