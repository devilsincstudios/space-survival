﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using DI.Core.Behaviours;

namespace DI.Core.Actions
{
	public class ActionCreateObject : Action
	{
		public GameObject target;
		public Transform targetLocation;

		public override ActionTypes getType()
		{
			return ActionTypes.object_create;
		}
		public override void doAction()
		{
			GameObject newGO = GameObject.Instantiate(target);
			newGO.name = target.name;
			newGO.transform.position = targetLocation.transform.position;
			newGO.transform.rotation = targetLocation.transform.rotation;
		}
	}
}