﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using DI.Core.Behaviours;
using DI.Core.Events;
using UnityEngine.SceneManagement;
using System.Collections;

namespace DI.Core.Actions
{
	public class ActionLoadLevel : Action
	{
		public string levelName;
		public float waitTime = 3f;
		public bool completedLevel = true;

		public override ActionTypes getType()
		{
			return ActionTypes.gameplay;
		}

		public override void doAction()
		{
			StartCoroutine(delay());
		}

		public IEnumerator delay()
		{
			yield return new WaitForSecondsRealtime(waitTime);
			if (completedLevel) {
				Management.instance.onCompleteLevel();
			}
			else {
				Management.instance.onFailLevel();
			}

			SceneManager.LoadScene(levelName, LoadSceneMode.Single);
		}
	}
}