﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using DI.Core.Behaviours;
using DI.UI;
using DI.Core.Events;

namespace DI.Core.Actions
{
	public class ActionFadeOut : Action
	{
		public Fade fade;

		public override ActionTypes getType()
		{
			return ActionTypes.ui_fade_out;
		}

		public override void doAction()
		{
			DI_EventCenter<string>.invoke("OnStartFade", fade.name);
		}
	}
}