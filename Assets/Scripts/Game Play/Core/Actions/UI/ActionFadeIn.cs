﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using DI.Core.Behaviours;
using DI.UI;
using DI.Core.Events;

namespace DI.Core.Actions
{
	public class ActionFadeIn : Action
	{
		public Fade fade;

		public override ActionTypes getType()
		{
			return ActionTypes.ui_fade_in;
		}

		public override void doAction()
		{
			DI_EventCenter<string>.invoke("OnStartFade", fade.fadeName);
		}
	}
}