﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using System.Collections.Generic;
using System.Collections;

using DI.Core.Behaviours;
using DI.Core.Events;
using DI.Entities.Enemy;
using DI.Core.Actions;

namespace DI.Mechanics
{
	public class CombatManager : DI_MonoBehaviour
	{
		public List<Enemy> enemiesHuntingPlayer = new List<Enemy>();

		public void OnEnable()
		{
			DI_EventCenter<Enemy>.addListener("OnSeenPlayer", playerSpotted);
			DI_EventCenter<Enemy>.addListener("OnDespawn", enemyLost);
		}

		public void OnDisable()
		{
			DI_EventCenter<Enemy>.removeListener("OnSeenPlayer", playerSpotted);
			DI_EventCenter<Enemy>.removeListener("OnDespawn", enemyLost);
		}
		
		public void playerSpotted(Enemy enemy)
		{
			if (!isBeingHunted()) {
				DI_EventCenter.invoke("OnEnterCombat");
			}

			if (!enemiesHuntingPlayer.Contains(enemy)) {
				enemiesHuntingPlayer.Add(enemy);
			}
		}

		public void enemyLost(Enemy enemy)
		{
			if (enemiesHuntingPlayer.Contains(enemy)) {
				enemiesHuntingPlayer.Remove(enemy);
			}

			if (!isBeingHunted()) {
				DI_EventCenter.invoke("OnExitCombat");
			}
		}

		public bool isBeingHunted()
		{
			return (enemiesHuntingPlayer.Count > 0);
		}
	}
}