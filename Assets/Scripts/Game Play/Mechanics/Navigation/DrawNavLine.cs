﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using DI.Core.Behaviours;
using UnityEngine;
using UnityEngine.AI;

using System.Collections;
using System.Collections.Generic;

namespace DI.Mechanics.Navigation
{
	public class DrawNavLine : DI_MonoBehaviour
	{
		public LineRenderer navLine;
		public float navUpdateDelay = 0.2f;
		public GameObject agent;
		public GameObject pathing;
		public GameObject goal;
		public float heightOffset = 0.2f;
		protected NavMeshPath path;
		public bool updatingNavLine = true;
		public MapGen.MapGenerator generator;
		public List<Vector3> navPositions;
		public int navLineDetail = 5;
		public LayerMask terrainLayer;

		protected Vector3 lastAgentPos;
		protected Vector3 offset;

		public void OnEnable()
		{
			StartCoroutine(delayedStart());
		}

		public IEnumerator delayedStart()
		{
			while (!generator.buildComplete()) {
				yield return new WaitForSecondsRealtime(0.1f);
			}

			agent = GameObject.Find("Player");
			pathing = GameObject.Find("Pathing");
			goal = GameObject.Find("Exit Nav Point");
			path = new NavMeshPath();
			lastAgentPos = agent.transform.position;
			navPositions = new List<Vector3>();
			offset = new Vector3(0f, heightOffset, 0f);

			yield return StartCoroutine(updateNavLine());
		}

		public List<Vector3> addNavLineDetail(Vector3 startingPoint, Vector3 endingPoint)
		{
			List<Vector3> detailPoints = new List<Vector3>();
			if (startingPoint.y != endingPoint.y) {
				for (int detailPass = 1; detailPass <= navLineDetail; detailPass++) {
					Vector3 postion = Vector3.Lerp(startingPoint, endingPoint, (float)((float)detailPass/(float)navLineDetail));
					postion += offset;
					RaycastHit hit;

					if (Physics.Raycast(postion, Vector3.down, out hit, 5f, terrainLayer)) {
						postion = hit.point + offset;
						detailPoints.Add(postion);
					}
				}
			}
			return detailPoints;
		}

		public IEnumerator updateNavLine()
		{
			while (updatingNavLine) {
				yield return new WaitForSecondsRealtime(navUpdateDelay);
				navPositions.Clear();
				NavMesh.CalculatePath(pathing.transform.position, goal.transform.position, NavMesh.AllAreas, path);

				navPositions.Add(lastAgentPos + offset);
				navPositions.Add(agent.transform.position + offset);
				for (int index = 0; index < path.corners.Length; index++) {
					if (index != path.corners.Length - 1) {
						navPositions.AddRange(addNavLineDetail(navPositions[navPositions.Count - 1], path.corners[index] + offset));
						navPositions.Add(path.corners[index] + offset);
					}
					else {
						navPositions.Add(path.corners[index] + offset);
					}
				}
				navLine.positionCount = navPositions.Count;
				navLine.SetPositions(navPositions.ToArray());
				lastAgentPos = agent.transform.position;
			}
		}
	}
}