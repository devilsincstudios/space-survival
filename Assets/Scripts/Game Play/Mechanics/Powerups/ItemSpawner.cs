﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using System.Collections.Generic;
using System.Collections;

using DI.Core.Behaviours;
using DI.Core.Events;
using DI.Entities.Items;
using DI.Entities.Core;
using DI.Core;
using DI.Core.Pooling;

namespace DI.Mechanics.Powerups
{
	public class ItemSpawner : DI_MonoBehaviour
	{
		public List<GameObject> powerups;
		[Range(0, 100)]
		public int itemsPerLevel = 20;
		public bool initalized = false;
		public List<GameObject> spawnPoints;
		public bool canSpawn = false;

		public void OnEnable()
		{
			StartCoroutine(delayedStart());
		}

		public IEnumerator delayedStart()
		{
			while (!MapGen.MapGenerator.instance.buildComplete()) {
				yield return new WaitForSecondsRealtime(0.1f);
			}

			var spawns = GameObject.FindGameObjectsWithTag("Item Spawn Point");
			for (int index = 0; index < spawns.Length; index++) {
				spawnPoints.Add(spawns[index]);
			}

			if (itemsPerLevel > spawnPoints.Count) {
				itemsPerLevel = spawnPoints.Count;
			}

			while (!canSpawn) {
				yield return new WaitForSecondsRealtime(0.1f);
			}

			yield return StartCoroutine(spawn());
		}

		public IEnumerator spawn()
		{
			if (!initalized) {
				if (itemsPerLevel == spawnPoints.Count) {
					for (int index = 0 ; index < itemsPerLevel; index++) {
						GameObject powerup = DI_PoolManager.getPooledObject(powerups[UnityEngine.Random.Range(0, powerups.Count)]);

						if (powerup.GetComponent<PowerupEntity>().power.type == PowerupTypes.Random) {
							powerups.Remove(powerup);
						}

						powerup.transform.position = spawnPoints[index].transform.position;
						powerup.SetActive(true);
					}
				}
				else {
					List<GameObject> usedPoints = new List<GameObject>();
					int spawnedItems = 0;

					while (spawnedItems < itemsPerLevel) {
						GameObject spawnpoint = spawnPoints[UnityEngine.Random.Range(0, spawnPoints.Count)];
						if (!usedPoints.Contains(spawnpoint)) {
							GameObject powerup = DI_PoolManager.getPooledObject(powerups[UnityEngine.Random.Range(0, powerups.Count)]);
							powerup.transform.position = spawnpoint.transform.position;
							powerup.SetActive(true);
							usedPoints.Add(spawnpoint);
							spawnedItems++;
						}
					}
				}
				initalized = true;

			}
			yield return null;
		}
	}
}