﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using System.Collections.Generic;

using DI.Quests;
using DI.Core.Behaviours;
using DI.Core.Events;
using DI.Core.Actions;

namespace DI.Mechanics.Puzzles
{
	public class Puzzle : DI_MonoBehaviour
	{
		public List<Objective> objectives;
		public bool completed = false;

		public void OnEnable()
		{
			DI_EventCenter<Objective>.addListener("OnObjectiveUpdate", handleOnObjectiveUpdate);
			DI_EventCenter<Objective>.addListener("OnObjectiveComplete", handleOnObjectiveUpdate);
		}

		public void OnDisable()
		{
			DI_EventCenter<Objective>.removeListener("OnObjectiveUpdate", handleOnObjectiveUpdate);
			DI_EventCenter<Objective>.removeListener("OnObjectiveComplete", handleOnObjectiveUpdate);
		}

		public void handleOnObjectiveUpdate(Objective objective)
		{
			if (!completed) {
				if (objectives.Contains(objective)) {
					Debug.Log("Contains Objective");
					if (isCompleted()) {
						completed = true;
						DI_EventCenter<Puzzle>.invoke("OnCompletePuzzle", this);
					}
				}
				else {
					Debug.Log("!Contains Objective");
				}
			}
		}

		public bool isCompleted()
		{
			for (int index = 0; index < objectives.Count; index++) {
				Debug.Log("Objective[" + index + "] Status: " + objectives[index].isComplete());
				if (!objectives[index].isComplete()) {
					if (!objectives[index].isOptional()) {
						Debug.Log("Checked for complete, returning false");
						return false;
					}
				}
			}
			Debug.Log("Checked for complete, returning true");
			return true;
		}
	}
}