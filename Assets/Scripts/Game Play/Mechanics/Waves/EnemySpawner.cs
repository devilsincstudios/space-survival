﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using Zenject;
using DI.Core.Events;
using UnityEngine;
using DI.Entities.Enemy;
using System.Collections.Generic;
using DI.Core.Behaviours;
using DI.Core.Pooling;
using System.Collections;

namespace DI.Mechanics.Waves
{
	public class EnemySpawner : DI_MonoBehaviour
	{
		public List<GameObject> activeEnemies;
		public int enemiesSpawned = 0;
		public int round = 0;
		public int maxEnemiesOnScreen = 100;
		public bool initalized = false;
		public WaveSettings settings;
		public List<GameObject> spawnPoints;
		public float spawnCheckTimer = 0.1f;
		public MapGen.MapGenerator generator;
		public bool canSpawn = false;

		public void OnEnable()
		{
			DI_EventCenter<Enemy>.addListener("OnDespawn", handleOnDeSpawn);
			StartCoroutine(delayedStart());
		}

		public void OnDisable()
		{
			DI_EventCenter<Enemy>.removeListener("OnDespawn", handleOnDeSpawn);
			StopCoroutine(spawn());
		}

		public IEnumerator delayedStart()
		{
			while (!generator.buildComplete()) {
				yield return new WaitForSecondsRealtime(0.1f);
			}

			var spawns = GameObject.FindGameObjectsWithTag("Spawn Point");
			for (int index = 0; index < spawns.Length; index++) {
				spawnPoints.Add(spawns[index]);
			}

			while (!canSpawn) {
				yield return new WaitForSecondsRealtime(0.1f);
			}

			yield return StartCoroutine(spawn());
		}

		public IEnumerator spawn()
		{
			while (enemiesSpawned < settings.enemies) {
				if (activeEnemies.Count < maxEnemiesOnScreen) {
					GameObject enemy = DI_PoolManager.getPooledObject(settings.enemy);
					if (enemy != null) {
						enemy.transform.position = getSpawnPoint();
						enemy.SetActive(true);
						activeEnemies.Add(enemy);
						enemiesSpawned++;
					}
				}
				else {
					initalized = true;
					yield return new WaitForSecondsRealtime(0.1f);
				}

				if (initalized) {
					yield return new WaitForSecondsRealtime(spawnCheckTimer);
				}
			}
		}

		public Vector3 getSpawnPoint()
		{
			return spawnPoints[Random.Range(0, spawnPoints.Count)].transform.position;
		}

		public void handleOnDeSpawn(Enemy enemy)
		{
			if (activeEnemies.Contains(enemy.getEntity())) {
				activeEnemies.Remove(enemy.getEntity());
			}
		}

		public int getEnemyCount()
		{
			return activeEnemies.Count;
		}

		public void spawnEnemies()
		{
			round += 1;
		}
	}
}