﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//
using System;

namespace DI.Mechanics.Interactions
{
	public interface InteractionInterface
	{
		float getInteractionTime();
		string getInteractionAnimationName();
		void interact();
		bool isInteractable();
	}
}
