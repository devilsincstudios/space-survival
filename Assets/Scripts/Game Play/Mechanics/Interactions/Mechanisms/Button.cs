﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using DI.Core.Behaviours;
using System.Collections;
using UnityEngine;
using DI.Core;
using DI.Core.Events;
using DI.SFX;

namespace DI.Mechanics.Interactions.Mechanisms
{
	public class Button : Mechanism
    {
		public bool readyToUse = true;
		public float cooldownTime = 5.0f;
		private bool startingState;

		public void OnEnable()
		{
			DI_EventCenter<string>.addListener("OnResetPuzzle", handleOnResetPuzzle);
			startingState = isActive;
		}
		
		public void OnDisable()
		{
			DI_EventCenter<string>.removeListener("OnResetPuzzle", handleOnResetPuzzle);
		}

        void handleOnResetPuzzle(string name)
        {
			if (this.name == name) {
				isActive = startingState;
			}
        }

		public override void interact()
		{
			if (readyToUse) {
				// Play Sound
				if (useSound.hasSFX) {
					DI_SFX.playClipAtPoint(gameObject, transform.position, useSound.sfxs[Random.Range(0, useSound.sfxs.Count)]);
				}
				// Play Animation
				StartCoroutine("ResetButton");
				isActive = true;
				animator.SetBool("Pressed", true);
				DI_EventCenter<Mechanism, bool>.invoke("OnMechanismStateChange", this, isActive);
			}
		}

		public override bool isInteractable()
		{
			if (singleUse && hasBeenUsed) {
				return false;
			}
			if (cooldownTime <= 0f) {
				return true;
			}
			return false;
		}

        IEnumerator ResetButton()
        {
			readyToUse = false;
            yield return new WaitForSeconds(cooldownTime);
			isActive = false;
			animator.SetBool("Pressed", false);
			DI_EventCenter<Mechanism, bool>.invoke("OnMechanismStateChange", this, isActive);
			readyToUse = true;
        }
    }
}
