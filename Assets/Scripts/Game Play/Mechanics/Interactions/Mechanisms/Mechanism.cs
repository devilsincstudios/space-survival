﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using DI.Core.Behaviours;
using UnityEngine;
using System;
using DI.Core.Events;
using DI.SFX;
using DI.Entities.Properties;

namespace DI.Mechanics.Interactions
{
	public class Mechanism : DI.Core.Behaviours.DI_MonoBehaviour, InteractionInterface
	{
		public float interactionTime;
		public string animationName;
		public DI_SFXProperty useSound;
		public bool singleUse = true;
		public bool hasBeenUsed = false;
		public bool isActive = false;
		public Animator animator;

		public float getInteractionTime()
		{
			return interactionTime;
		}

		public string getInteractionAnimationName()
		{
			return animationName;
		}

		public virtual void interact()
		{
			if (singleUse) {
				if (!hasBeenUsed) {
					hasBeenUsed = true;
					DI_EventCenter<Mechanism>.invoke("OnUseMechanism", this);
				}
			}
			else {
				DI_EventCenter<Mechanism>.invoke("OnUseMechanism", this);
			}
		}

		public virtual bool isInteractable()
		{
			if (singleUse && hasBeenUsed) {
				return false;
			}
			return true;
		}
	}
}
