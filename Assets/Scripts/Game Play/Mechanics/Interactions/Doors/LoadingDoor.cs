﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using DI.Core.Behaviours;
using UnityEngine;
using DI.Entities.Door;
using DI.Mechanics.Loot;
using System;
using DI.Core.Events;
using DI.SFX;
using UnityEngine.SceneManagement;

using System.Collections.Generic;

namespace DI.Mechanics.Interactions
{
	public class  LoadingDoor : LockedDoor
	{
		public string sceneToLoad;

		public override void interact()
		{
			base.interact();

			if (!isLocked) {
				SceneManager.LoadScene(sceneToLoad, LoadSceneMode.Single);
			}
		}
	}
}