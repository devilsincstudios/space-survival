﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using System.Collections;
using DI.Core.Behaviours;
using DI.Core.Events;
using DI.SFX;
using DI.Entities.Properties;

namespace DI.SFX
{
	[RequireComponent(typeof(Rigidbody))]
    public class CollisionSounds : DI_MonoBehaviour
    {
        public float minVelocityForSound=0;
        public float relativeVelocity;

		public DI_SFXProperty collisionSounds;
        
        void OnCollisionEnter(Collision col)
        {
			if (collisionSounds.hasSFX) {
            	relativeVelocity = col.relativeVelocity.magnitude;
            	if(relativeVelocity > minVelocityForSound) {
					DI_SFX.playClipAtPoint(gameObject, transform.position, collisionSounds.sfxs[UnityEngine.Random.Range(0, collisionSounds.sfxs.Count)]);
            	}
			}
        }
    }
}
