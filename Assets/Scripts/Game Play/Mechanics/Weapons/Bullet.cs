﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using DI.Core.Behaviours;
using DI.Core.Events;
using DI.Mechanics.Loot;
using UnityEngine;
using DI.Core.Pooling;
using DI.Entities.Properties;
using DI.Entities.Limbs;
using DI.Entities.Enemy;

namespace DI.Mechanics.Weapons
{
	public class Bullet : DI_MonoBehaviour
	{
		public float damage;
		public LootAmmo ammoType;
		public bool initalized = false;
		public LootWeapon firingWeapon;
		public bool hit = false;

		public void initalize()
		{
			initalized = true;
		}

		public void OnCollisionEnter(Collision other)
		{
			for (int index = 0; index < other.contacts.Length; index++) {
				if (other.collider.CompareTag("Enemy")) {
					Limb limb = other.collider.GetComponent<Limb>();
					if (limb != null) {
						limb.rootNode.GetComponent<EnemyController>().takeDamage(limb, Weapon.getDamageAmount(firingWeapon, ammoType), ammoType);
						if (ammoType.bulletHitSounds.hasSFX) {
							DI.SFX.DI_SFX.playClipAtPoint(gameObject, other.contacts[0].point, ammoType.bulletHitSounds.sfxs[UnityEngine.Random.Range(0, ammoType.bulletHitSounds.sfxs.Count)]);
						}

						if (ammoType.bulletHitEffects != null) {
							GameObject hitEffect = DI_PoolManager.getPooledObject(ammoType.bulletHitEffects);
							hitEffect.transform.position = other.contacts[0].point;
							hitEffect.SetActive(true);
						}
						hit = true;
					}
				}
			}

			if (!hit) {
				if (ammoType.bulletMissSounds.hasSFX) {
					DI.SFX.DI_SFX.playClipAtPoint(gameObject, other.contacts[0].point, ammoType.bulletHitSounds.sfxs[UnityEngine.Random.Range(0, ammoType.bulletHitSounds.sfxs.Count)]);
				}

				if (ammoType.bulletMissEffects != null) {
					GameObject hitEffect = DI_PoolManager.getPooledObject(ammoType.bulletMissEffects);
					hitEffect.transform.position = other.contacts[0].point;
					hitEffect.SetActive(true);
				}
			}

			gameObject.SetActive(false);
		}

		public void OnDisable()
		{
			if (initalized) {
				this.GetComponent<Rigidbody>().velocity = Vector3.zero;
			}
		}
	}
}