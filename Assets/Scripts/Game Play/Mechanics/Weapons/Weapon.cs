﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using DI.Mechanics.Weapons;
using DI.Mechanics.Loot;

using UnityEngine;

public static class Weapon
{
	public static float getDamageAmount(LootWeapon weapon, LootAmmo ammo)
	{
		return Mathf.RoundToInt(Random.Range(weapon.minDamage, weapon.maxDamage) + Random.Range(ammo.minDamage, ammo.maxDamage));
	}
}