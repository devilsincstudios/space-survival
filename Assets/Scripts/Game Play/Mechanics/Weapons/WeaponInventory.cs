﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using DI.Mechanics.Loot;
using System.Collections.Generic;
using System;

namespace DI.Mechanics.Weapons
{
	[Serializable]
	public class WeaponInventory : LootInventory
	{
		public LootWeapon selectedWeapon;
		public List<Ammo> ammoInventory;

		public float getAmmo(LootAmmo ammoType)
		{
			for (int index = 0; index < ammoInventory.Count; index++) {
				if (ammoInventory[index].ammoType.name == ammoType.name) {
					return ammoInventory[index].count;
				}
			}
			return -1f;
		}

		public void addAmmo(LootAmmo ammoType, float amount)
		{
			for (int index = 0; index < ammoInventory.Count; index++) {
				if (ammoInventory[index].ammoType.name == ammoType.name) {
					ammoInventory[index].count += amount;
				}
			}
		}

		public void removeAmmo(LootAmmo ammoType, float amount)
		{
			for (int index = 0; index < ammoInventory.Count; index++) {
				if (ammoInventory[index].ammoType.name == ammoType.name) {
					ammoInventory[index].count -= amount;
				}
			}
		}
	}
}