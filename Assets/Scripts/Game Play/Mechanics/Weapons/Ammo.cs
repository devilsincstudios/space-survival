﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using System;
using DI.Mechanics.Loot;

namespace DI.Mechanics.Weapons
{
	[Serializable]
	public class Ammo
	{
		public LootAmmo ammoType;
		public float count = 0f;

		public void addAmmo(float amount)
		{
			ammoType.amount += amount;
		}

		public void removeAmmo(float amount)
		{
			ammoType.amount -= amount;
			if (ammoType.amount < 0f) {
				ammoType.amount = 0f;
			}
		}
	}
}