﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

namespace DI.Mechanics.Weapons
{
	public enum WeaponTypes
	{
		Assault_Rifle,
		Grenade_Launcher,
		Melee,
		Pistol,
		Rocket_Launcher,
		Shotgun,
		Sniper_Rifle,
		Sub_Machine_Gun,
		Turret
	}
}