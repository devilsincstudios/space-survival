﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using DI.Core.Behaviours;
using DI.Core.Events;
using DI.Mechanics.Loot;
using UnityEngine;
using DI.Core.Pooling;
using System.Collections.Generic;
using DI.Entities.Limbs;
using DI.Entities.Enemy;

namespace DI.Mechanics.Weapons
{
	public class Grenade : Bullet
	{
		protected float spawnTime;

		public void OnEnable()
		{
			spawnTime = DI.Core.DI_Time.getTimeSinceLevelLoad();
		}

		public new void OnCollisionEnter(Collision other)
		{
			// If the grenade has traveled for at least 0.2 seconds explode it on contact
			if (DI.Core.DI_Time.getTimeSinceLevelLoad() - spawnTime >= 0.5f) {
				gameObject.SetActive(false);
			}
		}

		public void explode()
		{
			if (ammoType.bulletHitEffects != null) {
				GameObject explosion = DI_PoolManager.getPooledObject(ammoType.bulletHitEffects);
				explosion.transform.position = transform.position;
				explosion.SetActive(true);
			}
			
			if (ammoType.aoeDamage) {
				List<GameObject> entities = new List<GameObject>();
				
				var colliders = Physics.OverlapSphere(transform.position, ammoType.aoeRadius);
				for (int index = 0; index < colliders.Length; index++) {
					if (colliders[index].CompareTag("Enemy")) {
						Limb limb = colliders[index].GetComponent<Limb>();
						if (limb != null) {
							entities.Add(limb.rootNode);
						}
					}
				}
				
				for (int index = 0; index < entities.Count; index++) {
					entities[index].GetComponent<EnemyController>().takeDamage(Weapon.getDamageAmount(firingWeapon, ammoType), ammoType.damageType);
					entities[index].GetComponent<Rigidbody>().AddExplosionForce(ammoType.force, transform.position, ammoType.aoeRadius, ammoType.force, ForceMode.VelocityChange);
				}
			}
		}

		public new void OnDisable()
		{
			if (initalized) {
				base.OnDisable();
				explode();
			}
		}
	}
}