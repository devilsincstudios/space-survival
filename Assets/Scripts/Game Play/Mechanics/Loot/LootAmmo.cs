﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using System;
using DI.Mechanics.Weapons;
using DI.Entities.Properties;

namespace DI.Mechanics.Loot
{
	[Serializable]
	public class LootAmmo : LootItem
	{
		public float minDamage;
		public float maxDamage;
		public float bulletSpeed = 30f;
		public bool raycastBullet = true;

		public WeaponDamageTypes damageType;
		public GameObject bulletPrefab;
		public DI_SFXProperty bulletSounds;
		public DI_SFXProperty bulletHitSounds;
		public GameObject bulletHitEffects;
		public GameObject bulletMissEffects;
		public DI_SFXProperty bulletMissSounds;
		public WeaponTypes weaponType;

		public bool aoeDamage = false;
		public float aoeRadius = 0f;
		public float minBurnDamage = 0f;
		public float maxBurnDamage = 0f;
		public float burnTime = 0f;
		public float ignitionChance = 20f;
		public float force = 20f;
	}
}