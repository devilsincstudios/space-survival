﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using System;

namespace DI.Mechanics.Loot
{
	public enum LootQuality
	{
		junk,
		common,
		uncommon,
		superior,
		exquisite,
		masterpiece
	}
}