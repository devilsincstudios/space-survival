﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using System;
using System.Collections.Generic;

namespace DI.Mechanics.Loot
{
	[Serializable]
	public class LootPool : ScriptableObject
	{
		public List<LootPoolEntry> contents;
		public LootMultipler lootValueMultipler;

		private LootPoolEntry selectedItem;

		public LootItem generateItem()
		{
			LootItem newItem = null;

			if (contents.Count > 0) {
				int index = UnityEngine.Random.Range(0, contents.Count);
				selectedItem = contents[index];
				newItem = UnityEngine.Object.Instantiate(selectedItem.item) as LootItem;
			}

			newItem.rarity = generateRarity();

			for (int index = 0; index < lootValueMultipler.rarityValues.Count; index++) {
				if (lootValueMultipler.rarityValues[index].rarity == newItem.rarity) {
					//Debug.Log("Adding value for rarity: " + lootValueMultipler.rarityValues[index].multipler);
					newItem.value *= lootValueMultipler.rarityValues[index].multipler;
					break;
				}
			}

			newItem.quality = generateQuality();

			for (int index = 0; index < lootValueMultipler.rarityValues.Count; index++) {
				if (lootValueMultipler.qualityValues[index].quality == newItem.quality) {
					//Debug.Log("Adding value for quality: " + lootValueMultipler.qualityValues[index].multipler);
					newItem.value *= lootValueMultipler.qualityValues[index].multipler;
					break;
				}
			}

			newItem.amount = generateAmount();
			return newItem;
		}

		private float generateAmount()
		{
			return UnityEngine.Random.Range(selectedItem.minAmount, selectedItem.maxAmount);
		}

		private LootRarity generateRarity()
		{
			// No need to roll if its a set rarity
			if (selectedItem.maxRarity == selectedItem.minRarity) {
				return selectedItem.maxRarity;
			}

			float roll = UnityEngine.Random.Range(0f, 100f);
			LootRarity pickedRarity = LootRarity.common;

			for (int index = 0; index < selectedItem.rarityChances.Count; index++) {
				if (selectedItem.rarityChances[index].chance >= roll) {
					if (pickedRarity < selectedItem.rarityChances[index].rarity) {
						//Debug.Log("Replacing previously selected rarity of: " + pickedRarity.ToString() + " with higher rarity of: " + selectedItem.rarityChances[index].rarity.ToString());
						pickedRarity = selectedItem.rarityChances[index].rarity;
					}
				}
				else {
					//Debug.Log("Roll failed for: " + selectedItem.rarityChances[index].rarity + " rolled: " + roll);
				}
			}

			if (pickedRarity > selectedItem.maxRarity) {
				//Debug.Log("Bad luck, the picked rarity: " + pickedRarity.ToString() + " was downgraded to the max rarity of: " + selectedItem.maxRarity.ToString());
				pickedRarity = selectedItem.maxRarity;
			}

			//Debug.Log("Rarity of: " + pickedRarity + " selected");
			return pickedRarity;
		}

		private LootQuality generateQuality()
		{
			// No need to roll if its a set quality
			if (selectedItem.maxQuality == selectedItem.minQuality) {
				return selectedItem.maxQuality;
			}

			float roll = UnityEngine.Random.Range(0f, 100f);
			LootQuality pickedQuality = LootQuality.common;

			for (int index = 0; index < selectedItem.qualityChances.Count; index++) {
				if (selectedItem.qualityChances[index].chance >= roll) {
					if (pickedQuality < selectedItem.qualityChances[index].quality) {
						//Debug.Log("Replacing previously selected quality of: " + pickedQuality.ToString() + " with higher quality of: " + selectedItem.qualityChances[index].quality.ToString());
						pickedQuality = selectedItem.qualityChances[index].quality;
					}
				}
				else {
					//Debug.Log("Roll failed for: " + selectedItem.qualityChances[index].quality + " rolled: " + roll);
				}
			}

			if (pickedQuality > selectedItem.maxQuality) {
				//Debug.Log("Bad luck, the picked rarity: " + pickedQuality.ToString() + " was downgraded to the max rarity of: " + selectedItem.maxQuality.ToString());
				pickedQuality = selectedItem.maxQuality;
			}

			//Debug.Log("Quality of: " + pickedQuality + " selected");
			return pickedQuality;
		}
	}
}