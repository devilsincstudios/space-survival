﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using System;

namespace DI.Mechanics.Loot
{
	[Serializable]
	public struct LootMultiplierRarity
	{
		public LootRarity rarity;
		public float multipler;
	}
}