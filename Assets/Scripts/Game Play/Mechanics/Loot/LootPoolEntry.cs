﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using System;
using System.Collections.Generic;

namespace DI.Mechanics.Loot
{
	[Serializable]
	public struct LootPoolEntry
	{
		public LootItem item;
		public float minAmount;
		public float maxAmount;
		public LootQuality minQuality;
		public LootQuality maxQuality;
		public List<LootQualityChance> qualityChances;
		public LootRarity minRarity;
		public LootRarity maxRarity;
		public List<LootRarityChance> rarityChances;
	}
}