﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using System;

namespace DI.Mechanics.Loot
{
	[Serializable]
	public class LootItem : ScriptableObject
	{
		//public string itemName;
		public float value;
		public float amount;
		public bool stacks = false;
		public Texture2D icon;
		public LootTypes type;
		public LootQuality quality;
		public LootRarity rarity;
	}
}