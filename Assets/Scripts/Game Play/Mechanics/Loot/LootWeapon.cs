﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using System;
using DI.Mechanics.Weapons;

namespace DI.Mechanics.Loot
{
	[Serializable]
	public class LootWeapon : LootItem
	{
		public float minDamage;
		public float maxDamage;
		public LootAmmo primaryAmmo;
		public LootAmmo secondaryAmmo;
		public float timeBetweenPrimaryShots = 0.2f;
		public float timeBetweenSecondaryShots = 0.2f;
		public WeaponTypes weaponType;
		public GameObject muzzleFlashPrefab;
	}
}