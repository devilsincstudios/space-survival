﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using System;

namespace DI.Mechanics.Loot
{
	[Serializable]
	public enum LootRarity
	{
		common,
		uncommon,
		rare,
		epic,
		legendary
	}
}