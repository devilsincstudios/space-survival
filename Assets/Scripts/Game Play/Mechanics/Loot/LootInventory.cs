﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using System;
using System.Collections.Generic;
using DI.Core.Behaviours;
using DI.Core.Events;

namespace DI.Mechanics.Loot
{
	[Serializable]
	public class LootInventory : DI_MonoBehaviour
	{
		public List<LootItem> items;
		public LootPool lootPool;
		public bool randomInventory = false;
		public int randomInventoryItemsMin = 0;
		public int randomInventoryItemsMax = 5;
		public bool randomMoney = false;
		public float randomMoneyMin = 0f;
		public float randomMoneyMax = 0f;
		[SerializeField]
		protected float money = 0f;
		[SerializeField]
		protected float itemValue = 0f;

		public void addItem(LootItem item)
		{
			if (hasItem(item)) {
				for (int index = 0; index < items.Count; index++) {
					if (items[index].name == item.name) {
						items[index].amount += item.amount;
					}
				}
			}
			else {
				items.Add(item);
			}

			//Debug.Log("Adding item: " + item.itemName + " with quality of: " + item.quality.ToString() + " and a rarity of: " + item.rarity.ToString() + " worth: " + item.value);
			itemValue += item.value * item.amount;
			DI_EventCenter<LootItem, GameObject>.invoke("OnAddItem", item, gameObject);
		}

		public void removeItem(LootItem item)
		{
			if (!item.stacks) {
				items.Remove(item);
			}
			else {
				for (int index = 0; index < items.Count; index++) {
					if (items[index].name == item.name) {
						items[index].amount -= item.amount;
					}
				}
			}

			//Debug.Log("Removing item: " + item.itemName + " with quality of: " + item.quality.ToString() + " and a rarity of: " + item.rarity.ToString() + " worth: " + item.value);
			itemValue -= item.value * item.amount;
			DI_EventCenter<LootItem, GameObject>.invoke("OnRemoveItem", item, gameObject);
		}

		public bool hasItem(LootItem item)
		{
			for (int index = 0; index < items.Count; index++) {
				if (items[index].name == item.name) {
					return true;
				}
			}

			return false;
		}

		public float getMoney()
		{
			return money;
		}

		public void addMoney(float amount)
		{
			money += amount;
			DI_EventCenter<float, GameObject>.invoke("OnUpdateMoney", money, gameObject);
		}

		public void removeMoney(float amount)
		{
			money -= amount;
			DI_EventCenter<float, GameObject>.invoke("OnUpdateMoney", money, gameObject);
		}

		public void removeAllItems()
		{
			for (int index = 0; index < items.Count; index++) {
				removeItem(items[index]);
			}
			itemValue = 0f;
		}

		public void sellAllItems()
		{
			addMoney(itemValue);
			removeAllItems();
		}

		public void OnEnable()
		{
			if (randomInventory) {
				int numItems = UnityEngine.Random.Range(randomInventoryItemsMin, randomInventoryItemsMax);
				for (int index = 0; index < numItems; index++) {
					addItem(lootPool.generateItem());
				}
			}
			if (randomMoney) {
				float money = Mathf.RoundToInt(UnityEngine.Random.Range(randomMoneyMin, randomMoneyMax) * 10) / 10;
				addMoney(money);
			}
		}
	}
}