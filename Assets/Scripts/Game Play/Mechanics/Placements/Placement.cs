﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using DI.Core.Behaviours;
using UnityEngine;
using System.Collections.Generic;
using System;
using DI.Core.Pooling;
using DI.Core.Events;

namespace DI.Mechanics.Placements
{
	public class Placement : DI_MonoBehaviour
	{
		public List<Renderer> meshRenderers;
		public List<Material> originalMaterials;
		public bool isUpdating = true;
		public bool isPlacing = false;
		public Transform placementLocation;
		private bool lastFrameCollision = false;

		public PlaceableEntity placeable;

		public GameObject selectedPlaceable;
		public List<GameObject> placeablePrefabs;
		[Tooltip("What object should the newly placed objects be parented to?")]
		public Transform placeablesHolder;

		public void OnEnable()
		{
			DI_EventCenter<DI.Entities.PlaceableEntity>.addListener("OnSelectPlacement", handleOnSelectPlacement);
		}

		public void OnDisable()
		{
			DI_EventCenter<DI.Entities.PlaceableEntity>.addListener("OnSelectPlacement", handleOnSelectPlacement);
		}

		public void handleOnSelectPlacement(DI.Entities.PlaceableEntity entity)
		{
			selectPlacement(entity.prefab);
		}

		public bool isColliding()
		{
			if (placeable.isColliding) {
				return true;
			}
			else if (!placeable.isColliding && lastFrameCollision) {
				return true;
			}
			else {
				return placeable.isColliding;
			}
		}

		public void Update()
		{
			if (isPlacing) {
				if (isUpdating) {
					if (isColliding()) {
						setMaterials(placeable.invalidMaterial);
					}
					else {
						setMaterials(placeable.validMaterial);
					}
					lastFrameCollision = placeable.isColliding;
				}
			}
			else {
				if (isUpdating) {
					try {
						cancelPlacement();
					}
					catch (Exception) {};
				}
			}
		}

		public void setMaterials(Material material)
		{
			for (int index = 0; index < meshRenderers.Count; index++) {
				meshRenderers[index].material = material;
			}
		}

		public void deployPlacement()
		{
			if (!placeable.isColliding) {
				placeable.transform.parent = placeablesHolder;
				isUpdating = false;
				isPlacing = false;
	
				for (int index = 0; index < meshRenderers.Count; index++) {
					meshRenderers[index].material = originalMaterials[index];
				}
				placeable.onPlace();
				GameObject tmpPrefab = placeable.entity.prefab;
				selectedPlaceable = null;
				selectPlacement(tmpPrefab);
			}
		}

		public void cancelPlacement()
		{
			placeable.onCancel();
			placeable.gameObject.SetActive(false);

			isUpdating = false;
			isPlacing = false;
			selectedPlaceable.SetActive(false);
		}

		public void selectPlacement(GameObject prefab)
		{
			if (selectedPlaceable != null) {
				cancelPlacement();
			}

			selectedPlaceable = DI_PoolManager.getPooledObject(prefab);
			selectedPlaceable.transform.parent = placementLocation;
			selectedPlaceable.transform.localRotation = placementLocation.localRotation;
			selectedPlaceable.transform.localPosition = placementLocation.localPosition;
			selectedPlaceable.SetActive(true);
			placeable = selectedPlaceable.GetComponent<PlaceableEntity>();
			meshRenderers.Clear();
			var renderers = placeable.gameObject.GetComponentsInChildren<Renderer>();
			for (int index = 0; index < renderers.Length; index++) {
				meshRenderers.Add(renderers[index]);
				originalMaterials.Add(meshRenderers[index].material);
			}

			isUpdating = true;
			isPlacing = true;
			placeable.gameObject.SetActive(true);
			placeable.onSelect();
		}
	}
}