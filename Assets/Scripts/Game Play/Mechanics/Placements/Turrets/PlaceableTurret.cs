﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using DI.Core.Behaviours;
using UnityEngine;
using DI.Entities.Turrets;

namespace DI.Mechanics.Placements
{
	public class PlaceableTurret : PlaceableEntity
	{
		public TurretController controller;

		public override void onPlace()
		{
			base.onPlace();
			controller.fov.updatingViewCone = false;
			controller.fov.coneFilter.gameObject.GetComponent<MeshRenderer>().enabled = false;
			controller.enabled = true;
		}

		public override void onSelect()
		{
			base.onSelect();
			controller.fov.updatingViewCone = true;
			controller.fov.coneFilter.gameObject.GetComponent<MeshRenderer>().enabled = true;;
			controller.enabled = false;
		}

		public override void onCancel()
		{
			base.onCancel();
			controller.fov.updatingViewCone = false;
			controller.fov.coneFilter.gameObject.GetComponent<MeshRenderer>().enabled = false;
			controller.enabled = false;
		}
	}
}