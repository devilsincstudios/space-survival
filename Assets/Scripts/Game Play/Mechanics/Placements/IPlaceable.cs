﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

namespace DI.Mechanics.Placements
{
	public interface IPlaceable
	{
		void onPlace();
		void onSelect();
		void onCancel();
	}
}