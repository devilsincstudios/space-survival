﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using DI.Core.Behaviours;
using UnityEngine;
using DI.Entities.Core;

namespace DI.Mechanics.Placements
{
	public class PlaceableEntity : DI_MonoBehaviour, IPlaceable
	{
		public DI.Entities.PlaceableEntity entity;
		public Material validMaterial;
		public Material invalidMaterial;
		public bool isColliding = false;
		public bool isPlacing = false;
		public GameObject constructionEffects;

		public void OnCollisionStay(Collision other)
		{
			if (isPlacing) {
				if (!other.gameObject.CompareTag("Ground")) {
					isColliding = true;
				}
			}
		}

		public void OnCollisionExit(Collision other)
		{
			if (isPlacing) {
				if (!other.gameObject.CompareTag("Ground")) {
					isColliding = false;
				}
			}
		}

		public virtual void onPlace()
		{
			constructionEffects.SetActive(false);
			entity.onSpawn();
			isPlacing = false;
		}

		public virtual void onSelect()
		{
			constructionEffects.SetActive(false);
			isPlacing = true;
		}

		public virtual void onStartBuild()
		{
			constructionEffects.SetActive(true);
		}

		public virtual void onInterruptBuild()
		{
			constructionEffects.SetActive(false);
		}

		public virtual void onFinishBuild()
		{
			constructionEffects.SetActive(false);
		}

		public virtual void onCancel()
		{
			constructionEffects.SetActive(false);
			isPlacing = false;
		}
	}
}