// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// Disables the mesh renderer for editor only objects once the game starts.
// This allows them to be seen while in editor mode, but not while in game.
//

using UnityEngine;
using System;

using DI.Core.Behaviours;

namespace DI.Tools
{
	[AddComponentMenu("Devil's Inc Studios/Tools/Editor Only Object")]
	public class EditorOnlyObject : DI_MonoBehaviour
	{
		public bool destoryObject = false;

		public void Start()
		{
			if (destoryObject) {
				Destroy(this.gameObject);
			}
			else {
				try {
					this.gameObject.GetComponent<MeshRenderer>().enabled = false;
				}
				catch (Exception) {
				}
				try {
					this.gameObject.GetComponent<SpriteRenderer>().enabled = false;
				}
				catch (Exception) {
				}
				try {
					this.gameObject.GetComponent<SkinnedMeshRenderer>().enabled = false;
				}
				catch (Exception) {
				}
			}
		}
	}
}
