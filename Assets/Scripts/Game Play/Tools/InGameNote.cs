﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using UnityEngine.UI;

using DI.Core.Behaviours;

namespace DI.Tools
{
	[AddComponentMenu("Devil's Inc Studios/Tools/In Game Note")]
	public class InGameNote : DI_MonoBehaviour
	{
		public string message;
		public Text noteTextBox;
		public Texture2D noteImage;
		public RawImage noteImageHolder;
		public GameObject collapsedGroup;
		public GameObject hintGroup;

		public void OnEnable()
		{
			hintGroup.SetActive(true);
			collapsedGroup.SetActive(false);
			noteTextBox.text = message;
			noteImageHolder.texture = noteImage;
		}

		public void OnTriggerStay(Collider other)
		{
			if (other.tag == "Player") {
				collapsedGroup.SetActive(true);
				hintGroup.SetActive(false);
			}
		}

		public void OnTriggerExit(Collider other)
		{
			if (other.tag == "Player") {
				collapsedGroup.SetActive(false);
				hintGroup.SetActive(true);
			}
		}
	}
}