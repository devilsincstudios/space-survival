﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine;

using DI.Core.Behaviours;

namespace DI.Tools
{
	public class EnableOnEnable : DI_MonoBehaviour
	{
		public GameObject target;

		public void OnEnable()
		{
			target.SetActive(true);
		}
	}
}