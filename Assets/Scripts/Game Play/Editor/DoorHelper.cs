// 	Devils Inc Studios
// 	How Long
// 	Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
//
// 	TODO: Include a description of the file here.
//

using UnityEditor;
using UnityEngine;
using System.Collections;
using DI.Core.Helpers;
using DI.Entities.Door;

namespace DI_Editor {
	[CustomEditor(typeof(DoorController))]
	public class DoorHelper : Editor {
		private DoorController controller;
		public void Awake() {
			controller = (DoorController)target;
			if (controller.doorHandle == null) {
				GameObject handle = new GameObject();
				handle.transform.parent = controller.gameObject.transform;
				handle.transform.localPosition = controller.openPosition;
				handle.gameObject.name = "Handle";
				controller.doorHandle = handle;
			}
			if (controller.openingSound == null) {
				// Add objects for opening sound.
				GameObject openingSound = new GameObject("Opening Sound");
				openingSound.gameObject.transform.parent = controller.doorHandle.transform;
				openingSound.gameObject.AddComponent<AudioSource>();
				openingSound.gameObject.transform.localPosition = new Vector3(0,0,0);
				openingSound.GetComponent<AudioSource>().volume = 0.5f;
				openingSound.GetComponent<AudioSource>().playOnAwake = false;

				// Add objects for closing sound.
				GameObject closingSound = new GameObject("Closing Sound");
				closingSound.gameObject.AddComponent<AudioSource>();
				closingSound.gameObject.transform.parent = controller.doorHandle.transform;
				closingSound.gameObject.transform.localPosition = new Vector3(0,0,0);
				closingSound.GetComponent<AudioSource>().volume = 0.5f;
				closingSound.GetComponent<AudioSource>().playOnAwake = false;
				
				// Assign the sounds to the controller.
				controller.openingSound = openingSound.GetComponent<AudioSource>();
				controller.closingSound = closingSound.GetComponent<AudioSource>();
			}
		}
		public override void OnInspectorGUI() {
			EditorGUILayout.BeginVertical();
			//====================================================================================================
			// Open Door Settings
			EditorGUILayout.BeginHorizontal();
			if (GUILayout.Button("Open Door")) {
				if (controller.isSliding) {
					controller.transform.localPosition = controller.openPosition;
					controller.isOpen = true;
				}
				else {
					controller.transform.localRotation = controller.openRotation;
					controller.isOpen = true;
				}
			}
			if (GUILayout.Button("Set Open Position")) {
				if (controller.isSliding) {
					controller.openPosition = controller.transform.localPosition;
				}
				else {
					controller.openRotation = controller.transform.localRotation;
				}
			}
			EditorGUILayout.EndHorizontal();
			//====================================================================================================

			//====================================================================================================
			// Close Door Settings
			EditorGUILayout.BeginHorizontal();
			if (GUILayout.Button("Close Door")) {
				if (controller.isSliding) {
					controller.transform.localPosition = controller.closedPosition;
					controller.isOpen = false;
				}
				else {
					controller.transform.localRotation = controller.closedRotation;
					controller.isOpen = false;
				}
			}
			if (GUILayout.Button("Set Closed Position")) {
				if (controller.isSliding) {
					controller.closedPosition = controller.transform.localPosition;
				}
				else {
					controller.closedRotation = controller.transform.localRotation;
				}
			}
			EditorGUILayout.EndHorizontal();
			//====================================================================================================

			//====================================================================================================
			// Door Params
			EditorGUILayout.BeginHorizontal();
			controller.isOpen = GUILayout.Toggle(controller.isOpen, "Is this door open?");
			controller.isSliding = GUILayout.Toggle(controller.isSliding, "Is this a sliding door?");
			EditorGUILayout.EndHorizontal();
			EditorGUILayout.BeginHorizontal();
			controller.movementTime = (float) EditorGUILayout.FloatField("Movement Time", controller.movementTime);
			controller.movementTime = Mathf.Clamp(controller.movementTime, 0.0f, 100.0f);
			EditorGUILayout.EndHorizontal();

			// Opening Sound
			EditorGUILayout.BeginHorizontal();
			controller.openingSound.volume = (float) EditorGUILayout.FloatField("Opening Volume", controller.openingSound.volume);
			controller.openingSound.volume = Mathf.Clamp(controller.openingSound.volume, 0.0f, 1.0f);
			controller.openingSound.clip = (AudioClip)EditorGUILayout.ObjectField(controller.openingSound.clip, typeof(AudioClip), true);
			EditorGUILayout.EndHorizontal();

			// Closing Sound
			EditorGUILayout.BeginHorizontal();
			controller.closingSound.volume = (float) EditorGUILayout.FloatField("Closing Volume", controller.closingSound.volume);
			controller.closingSound.volume = Mathf.Clamp(controller.closingSound.volume, 0.0f, 1.0f);
			controller.closingSound.clip = (AudioClip)EditorGUILayout.ObjectField(controller.closingSound.clip, typeof(AudioClip), true);
			EditorGUILayout.EndHorizontal();
			//====================================================================================================
			EditorGUILayout.EndVertical();
		}
	}
}

