﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine;

namespace DI.Graphics
{
	public static class AnisotropicFiltering
	{
		public static void setValue(float value)
		{
			switch ((int)value) {
				case 0:
					QualitySettings.anisotropicFiltering = UnityEngine.AnisotropicFiltering.Disable;
					break;
				case 1:
					QualitySettings.anisotropicFiltering = UnityEngine.AnisotropicFiltering.ForceEnable;
					break;
			}
			saveValue(value);
		}

		public static void saveValue(float value)
		{
			PlayerPrefs.SetFloat("Settings-Graphics-AnisotropicFiltering", value);
			PlayerPrefs.Save();
		}

		public static float loadValue()
		{
			float value = PlayerPrefs.GetFloat("Settings-Graphics-AnisotropicFiltering", 1f);
			setValue(value);
			return value;
		}
	}
}