﻿//// Devils Inc Studios
//// Copyright DEVILS INC. STUDIOS LIMITED 2016
////
//// TODO: Include a description of the file here.
////
//
using DI.Core.Behaviours;

namespace DI.Graphics
{
	public class GraphicsSetting : DI_MonoBehaviour
	{
		public void OnEnable()
		{
			loadSettings();
		}

		public void loadSettings()
		{
			AnisotropicFiltering.loadValue();
			AntiAliasing.loadValue();
			Bloom.loadValue();
			Brightness.loadValue();
			VSync.loadValue();
			DepthOfField.loadValue();
			DetailDensity.loadValue();
			DetailDistance.loadValue();
			ShadowDistance.loadValue();
			TextureQuality.loadValue();
		}
	}
}
