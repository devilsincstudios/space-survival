﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using UnityStandardAssets.ImageEffects;

namespace DI.Graphics
{
	public static class AntiAliasing
	{
		public static void setValue(float value)
		{
			Antialiasing aa = Camera.main.GetComponent<Antialiasing>();
			switch ((int)value) {
				case 0:
					aa.enabled = false;
					break;
				case 1:
					aa.enabled = true;
					break;
			}
			saveValue(value);
		}

		public static void saveValue(float value)
		{
			PlayerPrefs.SetFloat("Settings-Graphics-AntiAliasing", value);
			PlayerPrefs.Save();
		}

		public static float loadValue()
		{
			float value = PlayerPrefs.GetFloat("Settings-Graphics-AntiAliasing", 1f);
			setValue(value);
			return value;
		}
	}
}