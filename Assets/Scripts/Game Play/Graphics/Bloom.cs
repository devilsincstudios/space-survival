﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using UnityStandardAssets.ImageEffects;

namespace DI.Graphics
{
	public static class Bloom
	{
		public static void setValue(float value)
		{
			BloomOptimized bloom = Camera.main.GetComponent<BloomOptimized>();

			switch ((int)value) {
				case 0:
					bloom.enabled = false;
					break;
				case 1:
					bloom.enabled = true;
					break;
			}
			saveValue(value);
		}

		public static void saveValue(float value)
		{
			PlayerPrefs.SetFloat("Settings-Graphics-Bloom", value);
			PlayerPrefs.Save();
		}

		public static float loadValue()
		{
			float value = PlayerPrefs.GetFloat("Settings-Graphics-Bloom", 1f);
			setValue(value);
			return value;
		}
	}
}