﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using UnityStandardAssets.ImageEffects;

namespace DI.Graphics
{
	public static class DepthOfField
	{
		public static void setValue(float value)
		{
			var effect = Camera.main.GetComponent<UnityStandardAssets.ImageEffects.DepthOfField>();

			switch ((int)value) {
				case 0:
					effect.enabled = false;
					break;
				case 1:
					effect.enabled = true;
					break;
			}
			saveValue(value);
		}

		public static void saveValue(float value)
		{
			PlayerPrefs.SetFloat("Settings-Graphics-DepthOfField", value);
			PlayerPrefs.Save();
		}

		public static float loadValue()
		{
			float value = PlayerPrefs.GetFloat("Settings-Graphics-DepthOfField", 1f);
			setValue(value);
			return value;
		}
	}
}