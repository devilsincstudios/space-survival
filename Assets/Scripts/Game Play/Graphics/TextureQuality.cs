﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine;

namespace DI.Graphics
{
	public static class TextureQuality
	{
		public static void setValue(float value)
		{
			QualitySettings.masterTextureLimit = (int)(3f - value);
			saveValue(value);
		}

		public static void saveValue(float value)
		{
			PlayerPrefs.SetFloat("Settings-Graphics-TextureQuality", value);
			PlayerPrefs.Save();
		}

		public static float loadValue()
		{
			float value = PlayerPrefs.GetFloat("Settings-Graphics-TextureQuality", 3f);
			setValue(value);
			return value;
		}
	}
}