﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine;

namespace DI.Graphics
{
	public static class DetailDistance
	{
		public static void setValue(float value)
		{
			if (Terrain.activeTerrain != null) {
				Terrain.activeTerrain.detailObjectDistance = value;
				saveValue(value);
			}
			else {
				saveValue(0f);
			}
		}

		public static void saveValue(float value)
		{
			PlayerPrefs.SetFloat("Settings-Graphics-DetailDistance", value);
			PlayerPrefs.Save();
		}

		public static float loadValue()
		{
			float value = PlayerPrefs.GetFloat("Settings-Graphics-DetailDistance", 100f);
			setValue(value);
			return value;
		}
	}
}