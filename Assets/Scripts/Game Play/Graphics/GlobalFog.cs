﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using UnityStandardAssets.ImageEffects;

namespace DI.Graphics
{
	public static class GlobalFog
	{
		public static void setValue(float value)
		{
			var script = Camera.main.GetComponent<UnityStandardAssets.ImageEffects.GlobalFog>();

			switch ((int)value) {
				case 0:
					script.enabled = false;
					break;
				case 1:
					script.enabled = true;
					break;
			}
			saveValue(value);
		}

		public static void saveValue(float value)
		{
			PlayerPrefs.SetFloat("Settings-Graphics-GlobalFog", value);
			PlayerPrefs.Save();
		}

		public static float loadValue()
		{
			float value = PlayerPrefs.GetFloat("Settings-Graphics-GlobalFog", 1f);
			setValue(value);
			return value;
		}
	}
}