﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using ImageEffects;

namespace DI.Graphics
{
	public static class Brightness
	{
		public static void setValue(float value)
		{
			ImageEffects.Brightness brightness = Camera.main.GetComponent<ImageEffects.Brightness>();
			brightness.brightness = value;
			saveValue(value);
		}

		public static void saveValue(float value)
		{
			PlayerPrefs.SetFloat("Settings-Graphics-Brightness", value);
			PlayerPrefs.Save();
		}

		public static float loadValue()
		{
			float value = PlayerPrefs.GetFloat("Settings-Graphics-Brightness", 1f);
			setValue(value);
			return value;
		}
	}
}