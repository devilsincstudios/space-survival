﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//
//
// Thanks to Sebastian Lague for the tutorial on FoV @ https://github.com/SebLague/Field-of-View
//

using DI.Core.Behaviours;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DI.Core.Helpers;

#if UNITY_EDITOR
using UnityEditor;
#endif

using DI.Core.Events;
using DI.Entities;

namespace DI.AI
{
    public class FieldOfView : DI_MonoBehaviour
    {
        [Range(0, 360)]
        public float viewAngle;
        public float viewRadius;

		//Maximum range of guard. Initially set to viewRadius
        public float maxRange;
		public float eyeHeight;

        public LayerMask obstacleMask;
		public LayerMask targetMask;

		public Mesh viewCone;
		public List<GameObject> objectsInView;
		public List<GameObject> lastObjectsInView;

		private GameObject viewConeHandle;
		private MeshCollider coneCollider;
		public MeshFilter coneFilter;

		[Range(0.1f, 10f)]
		public float meshResolution = 10f;
		public float targetFindingDelay = 0.2f;
		public int edgeDectectionIterations = 10;
		public float edgeDectectionDistanceThreshold = 0.5f;
		private DI_ViewInfo viewInfo;
		public bool updatingViewCone = false;

        public void OnEnable()
        {
			objectsInView = new List<GameObject>();
			lastObjectsInView = new List<GameObject>();

			StartCoroutine("updateTargets", targetFindingDelay);
			viewCone = new Mesh();
			viewInfo = new DI_ViewInfo(transform, viewRadius, obstacleMask);
			viewCone.name = gameObject.GetInstanceID() + "_View Cone";
        }

		public void OnDisable()
		{
			StopAllCoroutines();
		}

		public void LateUpdate()
		{
			if (updatingViewCone) {
				drawFieldOfView(true);
				viewInfo.viewRadius = viewRadius;
				viewInfo.obstacleMask = obstacleMask;
			}
		}

		private bool checkLOS(HumanBodyBones bone, EntityAnimator target, Vector3 castPosition)
		{
			// Step three, raycast to the target's head and see if anything marked as an obstacle blocks the cast.
			// This requires an animated player
			// Vector3 directionToTarget = (target.getBoneTransform(HumanBodyBones.Head).position - castPosition).normalized;
			// Since we aren't using that but are just using a simple collider
			Vector3 headPosition = new Vector3(target.transform.position.x, target.transform.position.y + target.GetComponent<CapsuleCollider>().height, target.transform.position.z);
			Vector3 directionToTarget = (headPosition - castPosition).normalized;

			//float distanceToTarget = Vector3.Distance (castPosition, target.getBoneTransform(HumanBodyBones.Head).position);
			float distanceToTarget = Vector3.Distance (castPosition, headPosition);
			Debug.DrawRay(castPosition, directionToTarget * distanceToTarget, Color.cyan, 0.2f);

			if (!Physics.Raycast (castPosition, directionToTarget, distanceToTarget, obstacleMask)) {
				// Nothing blocked us, we have a clear line of sight to the target.
				return true;
			}
			return false;
		}

		public IEnumerator updateTargets(float delay)
		{
			// Attempt to offset the AI's updates so that not all AIs will be updating at the same time.
			float randomDelay = UnityEngine.Random.Range(0, delay * 5);
			yield return new WaitForSeconds(randomDelay);

			while (true) {
				yield return new WaitForSeconds(delay);
				objectsInView.Clear();

				// Step one, filter out targets outside of the range
				Collider[] targetsInRange = Physics.OverlapSphere(transform.position, viewRadius, targetMask);
				for (int index = 0; index < targetsInRange.Length; index++) {
					// Step two, filter out targets outside of the asigned viewing angle.
					Transform target = targetsInRange[index].transform;
					Vector3 directionToTarget = (target.position - transform.position).normalized;
					if (Vector3.Angle (transform.forward, directionToTarget) < viewAngle / 2) {
						// Step three, check for obstructions in view.
						EntityAnimator targetAnimator = target.GetComponent<EntityAnimator>();
						if (targetAnimator != null) {
							Vector3 adjustedHeight = transform.position + new Vector3(0f, eyeHeight, 0f);
							if (checkLOS(HumanBodyBones.Spine, targetAnimator, adjustedHeight)) {
								objectsInView.Add(target.gameObject);
								for (int lastTargetsIndex = 0; lastTargetsIndex < lastObjectsInView.Count; lastTargetsIndex++) {
									if (lastObjectsInView[lastTargetsIndex].GetInstanceID() == target.gameObject.GetInstanceID()) {
										// Spotted a target
										DI_EventCenter<GameObject, GameObject>.invoke("OnTargetEnteredView", gameObject, target.gameObject);
										break;
									}
								}
							}
						}
					}
				}
				for (int index = 0; index < lastObjectsInView.Count; index++) {
					if (!objectsInView.Contains(lastObjectsInView[index])) {
						// Lost a target
						DI_EventCenter<GameObject, GameObject>.invoke("OnTargetLeftView", gameObject, lastObjectsInView[index]);
					}
				}

				lastObjectsInView = objectsInView;
			}
		}

		public bool canSeeTarget(GameObject target)
		{
			for (int index = 0; index < objectsInView.Count; index++) {
				if (objectsInView[index].GetInstanceID() == target.GetInstanceID()) {
					return true;
				}
			}

			return false;
		}

		public void drawFieldOfView(bool detectEdges)
		{
			if (viewCone == null) {
				viewCone = new Mesh();
				viewInfo = new DI_ViewInfo(transform, viewRadius, obstacleMask);
				viewCone.name = gameObject.GetInstanceID() + "_View Cone";
			}

			// How many base triangles will the view cone have.
			int stepCount = Mathf.RoundToInt(viewAngle * meshResolution);
			float stepAngleSize = viewAngle / stepCount;
			List<Vector3> viewPoints = new List<Vector3>();
			DI_ViewCastInfo lastCast = new DI_ViewCastInfo();
			if (meshResolution < 1f) {
				stepCount += 1;
			}
			// Check for edges and update the view cone to show the breaks
			for (int iteration = 0; iteration < stepCount; iteration++) {
				float angle = transform.eulerAngles.y - viewAngle / 2 + stepAngleSize * iteration;
				// Do a raycast at the specified angle and see if it hits anything
				DI_ViewCastInfo currentCast = DI_ViewCast.ViewCast(viewInfo, angle);
				if (detectEdges) {
					if (iteration > 0) {
						if (currentCast.hit != lastCast.hit) {
							// Is the edge we found likely on the same object?
							bool edgeDistanceThresholdExceeded = Mathf.Abs(lastCast.distance - currentCast.distance) > edgeDectectionDistanceThreshold;
							if (currentCast.hit && lastCast.hit && edgeDistanceThresholdExceeded) {
								DI_EdgeInfo edge = DI_EdgeCast.FindEdge(viewInfo, lastCast, currentCast, edgeDectectionIterations, edgeDectectionDistanceThreshold);
								if (edge.pointA != Vector3.zero) {
									viewPoints.Add (edge.pointA);
								}
								if (edge.pointB != Vector3.zero) {
									viewPoints.Add (edge.pointB);
								}
							}
						}
					}
				}
				viewPoints.Add(currentCast.point);
				lastCast = currentCast;
			}

			int vertexCount = viewPoints.Count + 1;
			List<Vector3> vertices = new List<Vector3>();
			List<int> triangles = new List<int>();

			vertices.Add(new Vector3(0f, eyeHeight, 0f));

			// Top Layer
			for (int i = 0; i < vertexCount - 1; i++) {
				vertices.Add(transform.InverseTransformPoint(new Vector3(viewPoints[i].x, viewPoints[i].y + eyeHeight, viewPoints[i].z)));

				if (i < vertexCount - 2) {
					triangles.Add(0);
					triangles.Add(i + 1);
					triangles.Add(i + 2);

					triangles.Add(i);
					triangles.Add(i + vertexCount);
					triangles.Add(i + 1);

					triangles.Add(i);
					triangles.Add(i + vertexCount - 1);
					triangles.Add(i + vertexCount);
				}
			}

			// Bottom Layer
			vertices.Add(new Vector3(0f, 0f, 0f));

			int secondLayerIndex = vertices.Count - 1;
			int viewPointIndex = 0;
			for (int i = secondLayerIndex; i < (vertexCount * 2) - 1; i++) {
				vertices.Add(transform.InverseTransformPoint(viewPoints[viewPointIndex]));
				if (i < (vertexCount * 2) - 2) {
					triangles.Add(i + 2);
					triangles.Add(i + 1);
					triangles.Add(secondLayerIndex);
				}
				++viewPointIndex;
			}

			// Left Side
			triangles.Add(0);
			triangles.Add(secondLayerIndex);
			triangles.Add(1);

			triangles.Add(secondLayerIndex);
			triangles.Add(secondLayerIndex + 1);
			triangles.Add(1);

			// Right Side
			triangles.Add(0);
			triangles.Add(vertexCount - 1);
			triangles.Add(secondLayerIndex);

			triangles.Add(secondLayerIndex);
			triangles.Add(vertexCount - 1);
			triangles.Add(secondLayerIndex + vertexCount - 1);

			// Fill in the missing 3 tris
			triangles.Add(vertexCount * 2 - 2);
			triangles.Add(vertexCount * 2 - 1);
			triangles.Add(vertexCount - 2);
			triangles.Add(vertexCount - 2);
			triangles.Add(vertexCount * 2 - 1);
			triangles.Add(vertexCount - 1);
			triangles.Add(vertexCount - 2);
			triangles.Add(vertexCount * 2 - 3);
			triangles.Add(vertexCount * 2 - 2);

			viewCone.Clear ();
			viewCone.vertices = vertices.ToArray();
			viewCone.triangles = triangles.ToArray();
			viewCone.RecalculateNormals ();
			coneFilter.sharedMesh = viewCone;
		}

		public void OnDrawGizmosSelected()
		{
			viewInfo = new DI_ViewInfo(transform, viewRadius, obstacleMask);
			drawFieldOfView(true);
			//Gizmos.DrawWireMesh(viewCone, transform.position);
		}
    }
}
