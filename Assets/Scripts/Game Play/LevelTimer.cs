﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//
using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using DI.Core;

namespace DI.Mechanics.Timer
{
	public class LevelTimer : MonoBehaviour
	{
		public float levelTime = 90f;
		public float timeRemaining = 90f;
		public Text levelTimeDisplay;
		public bool timerPaused = false;

		public void startTimer()
		{
			StartCoroutine(countDown());
		}

		public void stopTimer()
		{
			StopCoroutine(countDown());
		}

		public void pauseTimer()
		{
			timerPaused = true;
		}

		public void unpauseTimer()
		{
			timerPaused = false;
		}

		public void addTime(float time)
		{
			timeRemaining += time;
		}

		public void setTime(float time)
		{
			timeRemaining = time;
		}

		public void OnDisable()
		{
			stopTimer();
		}

		public IEnumerator countDown()
		{
			while (timeRemaining > 0f) {
				if (Management.instance.unlimitedTime) {
					timeRemaining = 99f;
					levelTimeDisplay.text = timeRemaining + "";
					yield return new WaitForFixedUpdate();
				}
				else {
					if (!timerPaused) {
						timeRemaining -= Time.deltaTime;
						levelTimeDisplay.text = Mathf.RoundToInt(100f * timeRemaining)/100f + "";
						yield return new WaitForFixedUpdate();
					}
				}
			}

			DI.Core.Events.DI_EventCenter.invoke("OnTimeExpired");
		}
	}
}

