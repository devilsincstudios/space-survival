﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine.AI;
using UnityEngine;
using System.Collections;

public class AITest : MonoBehaviour
{
	public Transform player;
	public NavMeshAgent agent;
	public MapGen.MapGenerator generator;

	public bool canMove = false;
	public NavMeshPath path;
	public bool updatingPath = false;

	public Vector3 startingPoint;

	public void Awake()
	{
		startingPoint = gameObject.transform.position;
	}

	public void OnEnable()
	{
		player = GameObject.FindGameObjectWithTag("Player").transform;
		generator = GameObject.Find("Map Generator").GetComponent<MapGen.MapGenerator>();
		StartCoroutine(initalize());
	}

	public IEnumerator initalize()
	{
//		GameObject parent = GameObject.Find("AI");
//		if (parent == null) {
//			parent = new GameObject("AI");
//		}

		//transform.parent = parent.transform;

		while (!generator.buildComplete()) {
			yield return new WaitForSecondsRealtime(5f);
		}
		//yield return new WaitForSecondsRealtime(1f);

		agent.enabled = true;
		agent.enabled = false;
		gameObject.transform.position = startingPoint;
		agent.enabled = true;

		canMove = true;
		yield return null;
	}

	public IEnumerator updatePathing()
	{
		agent.CalculatePath(player.transform.position, path);
		updatingPath = false;
		yield return null;
	}

	public void LateUpdate()
	{
		if (canMove) {
			agent.SetDestination(player.transform.position);
		}
	}


}