﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine;

using DI.Core.Behaviours;
using DI.SFX;

namespace DI.UI
{
	public class UISound : DI_MonoBehaviour
	{
		public DI_SFXClipProperties mouseOverSFX;
		public DI_SFXClipProperties mouseOutSFX;
		public DI_SFXClipProperties mousePressSFX;

		public void onMouseOver()
		{
			DI_SFX.playClipAtPoint(gameObject, transform.position, mouseOverSFX);
		}

		public void onMouseOut()
		{
			DI_SFX.playClipAtPoint(gameObject, transform.position, mouseOutSFX);
		}

		public void onMousePress()
		{
			DI_SFX.playClipAtPoint(gameObject, transform.position, mousePressSFX);
		}
	}
}