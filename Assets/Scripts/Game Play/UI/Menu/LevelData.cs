// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using System;

[Serializable]
public struct LevelData
{
	public string name;
	public int index;
}

[Serializable]
public struct LevelDataSet
{
	public LevelData[] data;
}