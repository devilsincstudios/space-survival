﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using DI.Core.Behaviours;

using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;

namespace DI.UI.Menu
{
	public class Menu : DI_MonoBehaviour
	{
		public GameObject topLevelMenu;
		public GameObject thisMenu;

		public void hideMenu()
		{
			thisMenu.SetActive(false);
			topLevelMenu.SetActive(true);
		}

		public void showMenu()
		{
			thisMenu.SetActive(true);
			topLevelMenu.SetActive(false);
		}
	}
}