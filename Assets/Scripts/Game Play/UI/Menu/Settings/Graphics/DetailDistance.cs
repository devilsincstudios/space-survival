﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using DI.Core.Behaviours;
using UnityEngine.UI;
using UnityEngine;

namespace DI.UI.Menu.Settings
{
	public class DetailDistance : GraphicsSettings
	{
		public override void sliderInit()
		{
			slider.minValue = 0f;
			slider.maxValue = 100f;
			slider.value = 40f;
			slider.wholeNumbers = true;
		}

		public override void setDefaultLow()
		{
			setValue(0);
		}

		public override void setDefaultMedium()
		{
			setValue(20);
		}

		public override void setDefaultHigh()
		{
			setValue(40);
		}

		public override void setDefaultUltra()
		{
			setValue(80);
		}

		public override void setValue(float value)
		{
			DI.Graphics.DetailDistance.setValue(value);
			setSliderValue(value);
		}

		public override void loadValue()
		{
			setSliderValue(DI.Graphics.DetailDistance.loadValue());
		}
	}
}