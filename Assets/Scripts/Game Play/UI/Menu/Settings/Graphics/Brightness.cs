﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using DI.Core.Behaviours;
using UnityEngine.UI;
using UnityEngine;
using UnityStandardAssets.ImageEffects;
using ImageEffects;

namespace DI.UI.Menu.Settings
{
	public class Brightness : GraphicsSettings
	{
		public override void sliderInit()
		{
			slider.minValue = 0f;
			slider.maxValue = 3f;
			slider.value = 1f;
			slider.wholeNumbers = false;
		}

		public override void setDefaultLow()
		{
			setValue(1);
		}

		public override void setDefaultMedium()
		{
			setValue(1);
		}

		public override void setDefaultHigh()
		{
			setValue(1);
		}

		public override void setDefaultUltra()
		{
			setValue(1);
		}

		public override void setValue(float value)
		{
			DI.Graphics.Brightness.setValue(value);
			setSliderValue(value);
		}

		public override void loadValue()
		{
			setSliderValue(DI.Graphics.Brightness.loadValue());
		}
	}
}