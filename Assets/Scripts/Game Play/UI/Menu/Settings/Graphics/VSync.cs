﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using DI.Core.Behaviours;
using UnityEngine.UI;
using UnityEngine;

namespace DI.UI.Menu.Settings
{
	public class VSync : GraphicsSettings
	{
		public override void sliderInit()
		{
			slider.maxValue = 1;
			slider.minValue = 0;
			slider.wholeNumbers = true;
		}

		public override void setDefaultLow()
		{
			setValue(0);
		}

		public override void setDefaultMedium()
		{
			setValue(0);
		}

		public override void setDefaultHigh()
		{
			setValue(0);
		}

		public override void setDefaultUltra()
		{
			setValue(0);
		}

		public override void setValue(float value)
		{
			DI.Graphics.VSync.setValue(value);
			setSliderValue(value);
		}

		public override void loadValue()
		{
			setSliderValue(DI.Graphics.VSync.loadValue());
		}
	}
}