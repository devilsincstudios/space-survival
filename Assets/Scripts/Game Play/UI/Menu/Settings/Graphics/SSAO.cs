﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using DI.Core.Behaviours;
using UnityEngine.UI;
using UnityEngine;
using UnityStandardAssets.ImageEffects;

namespace DI.UI.Menu.Settings
{
	public class SSAO : GraphicsSettings
	{
		public override void sliderInit()
		{
			slider.maxValue = 1;
			slider.minValue = 0;
			slider.wholeNumbers = true;
		}

		public override void setDefaultLow()
		{
			setValue(0);
		}

		public override void setDefaultMedium()
		{
			setValue(1);
		}

		public override void setDefaultHigh()
		{
			setValue(1);
		}

		public override void setDefaultUltra()
		{
			setValue(1);
		}

		public override void setValue(float value)
		{
			DI.Graphics.SSAO.setValue(value);
			setSliderValue(value);
		}

		public override void loadValue()
		{
			setSliderValue(DI.Graphics.SSAO.loadValue());
		}
	}
}