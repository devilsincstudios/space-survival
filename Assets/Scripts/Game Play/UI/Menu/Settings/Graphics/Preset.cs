﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using DI.Core.Behaviours;
using DI.Core.Events;
using UnityEngine;

namespace DI.UI.Menu.Settings
{
	public class Preset : DI_MonoBehaviour
	{
		public Animator lowButton;
		public Animator mediumButton;
		public Animator highButton;
		public Animator ultraButton;
		public Animator customButton;

		public void setPresetLow()
		{
			DI_EventCenter.invoke("SetDefaultLow");
			resetButtons();
			setSelected(lowButton);
		}

		public void setPresetMedium()
		{
			DI_EventCenter.invoke("SetDefaultMedium");
			resetButtons();
			setSelected(mediumButton);
		}

		public void setPresetHigh()
		{
			DI_EventCenter.invoke("SetDefaultHigh");
			resetButtons();
			setSelected(highButton);
		}

		public void setPresetUltra()
		{
			DI_EventCenter.invoke("SetDefaultUltra");
			resetButtons();
			setSelected(ultraButton);
		}

		public void setPresetCustom()
		{
			resetButtons();
			setSelected(customButton);
		}

		public void OnEnable()
		{
			DI_EventCenter.addListener("SetDefaultCustom", setPresetCustom);
		}

		public void OnDisable()
		{
			DI_EventCenter.removeListener("SetDefaultCustom", setPresetCustom);
		}

		public void resetButtons()
		{
			lowButton.SetBool("Selected", false);
			mediumButton.SetBool("Selected", false);
			highButton.SetBool("Selected", false);
			ultraButton.SetBool("Selected", false);
			customButton.SetBool("Selected", false);
		}

		public void setSelected(Animator button)
		{
			PlayerPrefs.SetString("Settings-Graphics-Preset", gameObject.name);
			PlayerPrefs.Save();
			button.SetBool("Selected", true);
		}

		public void loadSelected()
		{
			string preset = PlayerPrefs.GetString("Settings-Graphics-Preset", "Medium");
			switch (preset) {
				case "Low":
					setPresetLow();
					break;
				case "Medium":
					setPresetMedium();
					break;
				case "High":
					setPresetHigh();
					break;
				case "Ultra":
					setPresetUltra();
					break;
				default:
					setPresetCustom();
					break;
			}
		}
	}
}