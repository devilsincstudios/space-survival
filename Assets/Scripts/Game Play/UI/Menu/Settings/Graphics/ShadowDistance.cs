﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using DI.Core.Behaviours;
using UnityEngine.UI;
using UnityEngine;

namespace DI.UI.Menu.Settings
{
	public class ShadowDistance : GraphicsSettings
	{
		public override void sliderInit()
		{
			slider.minValue = 0f;
			slider.maxValue = 100f;
			slider.value = 40f;
			slider.wholeNumbers = true;
		}

		public override void setDefaultLow()
		{
			setValue(40);
		}

		public override void setDefaultMedium()
		{
			setValue(60);
		}

		public override void setDefaultHigh()
		{
			setValue(80);
		}

		public override void setDefaultUltra()
		{
			setValue(100);
		}

		public override void setValue(float value)
		{
			DI.Graphics.ShadowDistance.setValue(value);
			setSliderValue(value);
		}

		public override void loadValue()
		{
			setSliderValue(DI.Graphics.ShadowDistance.loadValue());
		}
	}
}