﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using DI.Core.Behaviours;
using UnityEngine.UI;
using UnityEngine;
using UnityStandardAssets.ImageEffects;
using ImageEffects;

namespace DI.UI.Menu.Settings
{
	public class TextureQuality : GraphicsSettings
	{
		public override void sliderInit()
		{
			slider.minValue = 0f;
			slider.maxValue = 3f;
			slider.value = 1f;
			slider.wholeNumbers = true;
		}

		public override void setDefaultLow()
		{
			setValue(0);
		}

		public override void setDefaultMedium()
		{
			setValue(1);
		}

		public override void setDefaultHigh()
		{
			setValue(2);
		}

		public override void setDefaultUltra()
		{
			setValue(3);
		}

		public override void setValue(float value)
		{
			DI.Graphics.TextureQuality.setValue(value);
			setSliderValue(value);
		}

		public override void loadValue()
		{
			setSliderValue(DI.Graphics.TextureQuality.loadValue());
		}
	}
}