﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

namespace DI.UI.Menu.Settings
{
	public interface SettingsInterface
	{
		void setDefaultLow();
		void setDefaultMedium();
		void setDefaultHigh();
		void setDefaultUltra();
		void setValue(float value);
		void loadValue();
	}
}