﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using DI.Core.Behaviours;
using UnityEngine.UI;
using UnityEngine;
using DI.Core.Cheat;

namespace DI.UI.Menu.Settings
{
	public class SetMapSeed : DI_MonoBehaviour
	{
		public void setSeed(string seed)
		{
			Cheat_MapSeed.activate(seed);
		}
	}
}