﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using DI.Core.Behaviours;
using UnityEngine.UI;
using UnityEngine;
using DI.Core.Cheat;

namespace DI.UI.Menu.Settings
{
	public class UnlimitedAmmo : CheatSetting
	{
		public override void sliderInit()
		{
			slider.maxValue = 1;
			slider.minValue = 0;
			slider.wholeNumbers = true;
			isBool = true;
		}

		public override void setDefaultLow()
		{
			setValue(0);
		}

		public override void setDefaultMedium()
		{
			setValue(0);
		}

		public override void setDefaultHigh()
		{
			setValue(0);
		}

		public override void setDefaultUltra()
		{
			setValue(0);
		}

		public override void setValue(float value)
		{
			if (value == 1) {
				DI.Core.Cheat.Cheat_UnlimitedAmmo.activate();
			}
			else {
				DI.Core.Cheat.Cheat_UnlimitedAmmo.deactivate();
			}
			setSliderValue(value);
		}

		public override void loadValue()
		{
			setSliderValue(0f);
		}
	}
}