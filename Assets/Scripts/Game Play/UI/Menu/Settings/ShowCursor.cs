﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//
using System;
using DI.Core.Behaviours;
using UnityEngine;

namespace DI.UI
{
	public class ShowCursor : DI_MonoBehaviour
	{
		public void OnEnable()
		{
			Cursor.lockState = CursorLockMode.None;
			Cursor.visible = true;
		}

		public void lockCursor()
		{
			Cursor.lockState = CursorLockMode.Locked;
			Cursor.visible = false;
		}
	}
}

