﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using DI.Core.Behaviours;
using UnityEngine.UI;
using DI.Core.Events;
using UnityEngine;

namespace DI.UI.Menu.Settings
{
	public class GraphicsSettings : DI_MonoBehaviour, SettingsInterface
	{
		public Slider slider;
		public Text value;
		public Text label;
		public bool isBool = false;

		protected bool initalized = false;

		public void OnEnable()
		{
			if (!initalized) {
				sliderInit();
				slider.onValueChanged.AddListener(delegate { setValue(slider.value); });
				label.text = gameObject.name;
				DI_EventCenter.addListener("SetDefaultLow", setDefaultLow);
				DI_EventCenter.addListener("SetDefaultMedium", setDefaultMedium);
				DI_EventCenter.addListener("SetDefaultHigh", setDefaultHigh);
				DI_EventCenter.addListener("SetDefaultUltra", setDefaultUltra);
				loadValue();
				initalized = true;
			}
		}

		public void OnDisable()
		{
			DI_EventCenter.addListener("SetDefaultLow", setDefaultLow);
			DI_EventCenter.addListener("SetDefaultMedium", setDefaultMedium);
			DI_EventCenter.addListener("SetDefaultHigh", setDefaultHigh);
			DI_EventCenter.addListener("SetDefaultUltra", setDefaultUltra);
		}

		public virtual void sliderInit()
		{
		}

		public virtual void setDefaultLow()
		{
		}

		public virtual void setDefaultMedium()
		{
		}

		public virtual void setDefaultHigh()
		{
		}

		public virtual void setDefaultUltra()
		{
		}

		public virtual void setValue(float value)
		{
		}

		public void setSliderValue(float value)
		{
			DI_EventCenter.invoke("SetDefaultCustom");
			slider.value = value;

			if (isBool) {
				switch ((int)value) {
					case 0:
						this.value.text = "Disabled";
						break;
					case 1:
						this.value.text = "Enabled";
						break;
					default:
						this.value.text = value + "";
						break;
				}
			}
			else {
				this.value.text = value + "";
			}
		}

		public virtual void loadValue()
		{
		}
	}
}