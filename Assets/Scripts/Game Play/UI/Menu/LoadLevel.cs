// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2014, 2015
// TODO: Include a description of the file here.
//
using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Collections;
using System.Text.RegularExpressions;
using DI.Core.Events;
using DI.Dialog.UI;
using UnityEngine.SceneManagement;

namespace DI.UI.Menu
{
	public class LoadLevel : MonoBehaviour
	{
		public DI_TypeWritter typeWritter;

		public void OnEnable()
		{
			Object.DontDestroyOnLoad(this.gameObject);
			DI_EventCenter<string, Image, Text>.addListener("LoadLevel", loadLevelHandler);
		}
		
		public void OnDisable()
		{
			DI_EventCenter<string, Image, Text>.removeListener("LoadLevel", loadLevelHandler);
		}
		
		public void loadLevelHandler(string levelToLoad, Image loadingBar, Text loadingProgress)
		{
			StartCoroutine(loadLevel(levelToLoad, loadingBar, loadingProgress));
		}
		
		public IEnumerator loadLevel(string levelToLoad, Image loadingBar, Text loadingProgress)
		{
			typeWritter.textBox.gameObject.SetActive(true);
			typeWritter.typeMessage(fetchQuote());
			AsyncOperation levelLoader = SceneManager.LoadSceneAsync(levelToLoad);
			levelLoader.allowSceneActivation = false;
			while (!levelLoader.isDone) {
				loadingBar.fillAmount = levelLoader.progress;
				loadingProgress.text = "Loading: " + Mathf.Round(levelLoader.progress * 100) + "%";
				
				if (levelLoader.progress >= 0.9f && !levelLoader.allowSceneActivation) {
					levelLoader.allowSceneActivation = true;
				}
				yield return new WaitForEndOfFrame();
			}
			this.gameObject.SetActive(false);
			//Destroy(this.gameObject);
		}

		public string fetchQuote()
		{
			TextAsset quotesFile = (TextAsset)Resources.Load("LoadingText", typeof(TextAsset));
			string[] quotes = Regex.Split(quotesFile.text, "\r\n");
			return quotes[UnityEngine.Random.Range(0, quotes.Length - 1)];
		}
	}
}