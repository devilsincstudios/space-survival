﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

using DI.Core.Behaviours;

namespace DI.UI
{
	public class DevLevelSelect : DI_MonoBehaviour
	{
		public Dropdown dropdown;

		public void Awake()
		{
			dropdown.ClearOptions();
			loadLevelNames();
		}

		public void loadLevelNames()
		{
			Debug.Log ("loading level names");
			string json = Resources.Load<UnityEngine.TextAsset>("LevelData").ToString();
			LevelDataSet levelDataSet = JsonUtility.FromJson<LevelDataSet>(json);
			for (int index = 0; index < levelDataSet.data.Length; index++) {
				dropdown.options.Add(new Dropdown.OptionData(levelDataSet.data[index].name));
			}
		}

		public int levelId = 0;

		public void setLevelId(int newLevelId)
		{
			levelId = newLevelId + 1;
		}

		public void changeLevel()
		{
			SceneManager.LoadScene(dropdown.value);
		}
	}
}