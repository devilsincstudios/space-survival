﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using UnityEngine.UI;

using DI.Core.Behaviours;
using DI.Mechanics.Placements;
using DI.Core.Events;
using DI.Entities.Player;
using DI.Entities;

namespace DI.UI.Menus
{
	public class SelectionItem : DI_MonoBehaviour
	{
		public Text itemName;
		public RawImage itemIcon;
		public Text itemCost;
		public Text itemBuildTime;
		public Text itemDescription;
		public DI.Entities.PlaceableEntity settings;
		public GameObject selectionUI;

		public void OnEnable()
		{
			itemName.text = settings.name;
			itemIcon.texture = settings.icon;
			itemBuildTime.text = "" + settings.buildTime;
			itemCost.text = "" + settings.buildCost;
			itemDescription.text = settings.description;
		}

		public void OnSelectPlacement()
		{
			DI_EventCenter<DI.Entities.PlaceableEntity>.invoke("OnSelectPlacement", settings);
			selectionUI.SetActive(false);
		}
	}
}