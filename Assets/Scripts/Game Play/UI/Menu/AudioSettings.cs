﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using DI.Core.Behaviours;
using UnityEngine.Audio;
using UnityEngine.UI;
using System;
using UnityEngine;
using DI.Audio;

namespace DI.UI.Menus
{
	public class AudioSettings : DI_MonoBehaviour
	{
		public AudioMixer mixer;
		public Slider audioSlider;
		public string channelName;

		public void OnEnable()
		{
			audioSlider.maxValue = 100;
			audioSlider.minValue = 0;
			audioSlider.wholeNumbers = true;
			audioSlider.value = DI.Audio.Volume.loadValue(mixer, channelName);
			audioSlider.onValueChanged.AddListener(delegate { DI.Audio.Volume.setValue(mixer, channelName, audioSlider.value); });
		}
	}
}