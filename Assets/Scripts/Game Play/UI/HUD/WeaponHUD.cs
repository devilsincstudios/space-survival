﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using UnityEngine.UI;

using DI.Core.Behaviours;
using DI.Entities.Player;
using DI.Mechanics.Weapons;
using DI.Mechanics.Loot;
using DI.Core.Events;
using DI.Core;

namespace DI.UI
{
	public class WeaponHUD : DI_MonoBehaviour
	{
		[Header("UI Settings")]
		[Header("Selected Weapon")]
		public RawImage selectedWeaponIcon;
		[Header("Primary Ammo")]
		public RawImage primaryAmmoIcon;
		public Text primaryAmmoCount;

		public void LateUpdate()
		{
			primaryAmmoCount.text = "" + Management.instance.player.inventory.getAmmo(Management.instance.player.inventory.selectedWeapon.primaryAmmo);
		}
	}
}