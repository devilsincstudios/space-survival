﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using UnityEngine.UI;

using DI.Core.Behaviours;
using DI.Entities.Player;
using DI.Mechanics.Weapons;
using DI.Mechanics.Loot;
using DI.Core.Events;
using DI.Core.Helpers;
using DI.Core;

namespace DI.UI
{
	public class StatusHUD : DI_MonoBehaviour
	{
		[Header("UI Settings")]
		[Header("Health")]
		public Image healthIcon;
		public Text healthText;

		public void LateUpdate()
		{
			healthIcon.fillAmount = Management.instance.player.getHealth() / Management.instance.player.getMaxHealth();
			healthText.text = DI_Math.roundFloatToHundreths(Management.instance.player.getHealth() / Management.instance.player.getMaxHealth()) * 100f + "%";
		}
	}
}