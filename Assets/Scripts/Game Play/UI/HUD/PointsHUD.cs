﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using UnityEngine.UI;

using DI.Core.Behaviours;
using DI.Entities.Player;
using DI.Core.Events;
using DI.Entities.Limbs;
using DI.Entities.Enemy;
using DI.Core;

namespace DI.UI
{
	public class PointsHUD : DI_MonoBehaviour
	{
		[Header("UI Settings")]
		public Text pointsDisplay;

		public void LateUpdate()
		{
			pointsDisplay.text = " " + Management.instance.getPoints();
		}
	}
}