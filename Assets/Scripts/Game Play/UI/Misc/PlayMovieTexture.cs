// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using UnityEngine.UI;

using DI.Core.Behaviours;

#if !UNITY_WEBGL
namespace DI.UI
{
	public class PlayMovieTexture : DI_MonoBehaviour
	{
		public MovieTexture movie;
		public void OnEnable()
		{
			movie = GetComponent<RawImage>().texture as MovieTexture;
			movie.loop = true;
		}

		public void Update()
		{
			if (movie.isReadyToPlay) {
				if (!movie.isPlaying) {
					movie.Play();
				}
			}
		}
	}
}
#endif