// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine.UI;
using UnityEngine;
using DI.Core.Behaviours;
using DI.Core;
using DI.Core.Events;

using System.Collections;
using System.Collections.Generic;

namespace DI.UI
{
	public class Fade : DI_MonoBehaviour
	{
		public float fadeSpeed = 0.0f;
		public Image fadeImage;
		public FadeType fadeType;
		public bool isFading = false;
		public List<GameObject> objectsToDestroy;
		public bool waitForEvent = false;
		public string fadeName;

		public void OnEnable()
		{
			isFading = false;
			DI_EventCenter<string>.addListener("OnStartFade", handleOnStartFade);
		}

		public void OnDisable()
		{
			DI_EventCenter<string>.removeListener("OnStartFade", handleOnStartFade);
		}

		public void handleOnStartFade(string name)
		{
			if (fadeName == name) {
				waitForEvent = false;
			}
		}

		public void Update()
		{
			if (!waitForEvent) {
				if (!isFading) {
					fadeImage.gameObject.SetActive(true);
					if (fadeType == FadeType.IN) {
						StartCoroutine(fadeIn());
					}
					else {
						StartCoroutine(fadeOut());
					}
					isFading = false;
					waitForEvent = true;
				}
			}
		}

		public IEnumerator fadeIn()
		{
			isFading = true;
			fadeImage.rectTransform.localScale = new Vector2(Screen.width, Screen.height);
			DI_EventCenter<FadeType, string>.invoke("OnFadeStart", fadeType, fadeName);
			Color originalColor = fadeImage.color;
			float fadeStep = 0f;
			while (fadeStep < fadeSpeed) {
				yield return new WaitForSeconds(0.1f);
				fadeImage.color = Color.Lerp(originalColor, Color.clear, (fadeStep / fadeSpeed));
				fadeStep += 0.1f;
			}
			fadeImage.color = Color.clear;
			endFade();
		}

		public IEnumerator fadeOut()
		{
			isFading = true;
			fadeImage.rectTransform.localScale = new Vector2(Screen.width, Screen.height);
			DI_EventCenter<FadeType, string>.invoke("OnFadeStart", fadeType, fadeName);
			Color originalColor = fadeImage.color;
			float fadeStep = 0f;
			while (fadeStep < fadeSpeed) {
				yield return new WaitForSeconds(0.1f);
				fadeImage.color = Color.Lerp(originalColor, Color.black, (fadeStep / fadeSpeed));
				fadeStep += 0.1f;
			}
			fadeImage.color = Color.black;
			endFade();
		}

		public void endFade()
		{
			DI_EventCenter<FadeType, string>.invoke("OnFadeEnd", fadeType, fadeName);
			for (int index = 0; index < objectsToDestroy.Count; ++index) {
				Destroy(objectsToDestroy[index]);
			}
		}
	}

}