﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine.UI;
using UnityEngine;
using MapGen;
using System.Collections;

public class SeedDisplay : MonoBehaviour
{
	public Text seedText;
	public bool plainText = false;

	public void OnEnable()
	{
		if (plainText) {
			seedText.text = MapGenerator.instance.getMapSeed();
		}
		else {
			seedText.text = "Please provide this map seed: [ " + MapGenerator.instance.getMapSeed() + " ] when reporting errors";
		}
	}
}

