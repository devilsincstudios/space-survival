﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoadSeed : MonoBehaviour
{
	public Text seedValue;

	public void playSeed()
	{
		PlayerPrefs.SetString("Seed", seedValue.text);
		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
	}
}