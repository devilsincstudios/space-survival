﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine.SceneManagement;
using DI.Core.Behaviours;

namespace DI.Misc
{
	public class LoadLevel : DI_MonoBehaviour
	{
		public string level;
		public void OnEnable()
		{
			SceneManager.LoadScene(level, LoadSceneMode.Single);
		}
	}
}