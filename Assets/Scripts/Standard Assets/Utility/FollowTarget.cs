using System;
using UnityEngine;


namespace UnityStandardAssets.Utility
{
    public class FollowTarget : MonoBehaviour
    {
        public Transform target;
        public Vector3 offset = new Vector3(0f, 7.5f, 0f);
		public bool followRotation = false;
		public bool miniMap = false;
		public bool useFixedUpdate = false;
		private Vector3 rotation;

		public void OnEnable()
		{
			target = DI.Core.Management.instance.player.transform;
		}

        public void LateUpdate()
        {
			if (!useFixedUpdate) {
            	transform.position = target.position + offset;
				if (followRotation) {
					rotation = target.localEulerAngles;
					rotation.x = transform.localEulerAngles.x;
					transform.localEulerAngles = rotation;
				}
				if (miniMap) {
					transform.localRotation = Quaternion.Euler(90f, 0f, -target.localEulerAngles.y);
				}
			}
        }

		public void FixedUpdate()
		{
			if (useFixedUpdate) {
				transform.position = target.position + offset;
				if (followRotation) {
					rotation = target.localEulerAngles;
					rotation.x = transform.localEulerAngles.x;
					transform.localEulerAngles = rotation;
				}
				if (miniMap) {
					transform.localRotation = Quaternion.Euler(90f, 0f, -target.localEulerAngles.y);

				}
			}
		}
    }
}
