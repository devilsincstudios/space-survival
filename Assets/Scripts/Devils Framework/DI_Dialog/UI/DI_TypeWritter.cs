// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2014
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Text.RegularExpressions;

using DI.Core.Events;
using DI.Core.Behaviours;
using DI.SFX;

namespace DI.Dialog.UI
{
	[AddComponentMenu("UI/Effects/Type Writter")]
	public class DI_TypeWritter : DI_MonoBehaviour
	{
		public float typingSpeed = 0.05f;
		public DI_SFXClipProperties typingSound;
		public Text textBox;
		public bool playSound = false;

		public void stopTyping()
		{
			StopAllCoroutines();
		}

		public IEnumerator startTyping(string message)
		{
			textBox.text = "";
			message = message.Replace("\\n", Environment.NewLine);

			if (typingSpeed <= 0f) {
				textBox.text = message;
			}
			else {
				for (int position = 0; position < message.Length; position++) {
					yield return new WaitForSeconds(typingSpeed);
					textBox.text = message.Substring(0, position);
					if (playSound) {
						DI_SFX.playClipAtPoint(gameObject, transform.position, typingSound);
					}
				}
			}
		}

		public void typeMessage(string message)
		{
			StartCoroutine(startTyping(message));
		}
	}
}