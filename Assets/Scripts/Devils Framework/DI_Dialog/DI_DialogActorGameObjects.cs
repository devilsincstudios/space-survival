// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2014
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using System;

namespace DI.Dialog
{
	[Serializable]
	public struct DI_DialogActorGameObjects
	{
		public DI_DialogActor actor;
		public GameObject actorAvatar;
	}
}