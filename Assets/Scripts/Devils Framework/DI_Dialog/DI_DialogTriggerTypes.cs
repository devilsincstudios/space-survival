// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2014
//
// TODO: Include a description of the file here.
//

namespace DI.Dialog
{
	public enum DI_DialogTriggerTypes
	{
		ON_START,
		ON_TRIGGER,
		ON_EVENT,
		ON_TIMER,
	}
}