// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2014
//
// TODO: Include a description of the file here.
//
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace DI.Dialog
{
	public class DI_DialogScript : ScriptableObject
	{
		public List<DI_DialogLine> lines;
	}
}