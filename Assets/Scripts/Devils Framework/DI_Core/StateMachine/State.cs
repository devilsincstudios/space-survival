﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using System;
using System.Collections.Generic;

namespace DI.Core.StateMachine
{
	[System.Serializable]
	public class State
	{
		// Name of the state.
		public Enum name = null;

		// Delegates for update, fixed update, and late update calls.
		public Action updateMethod = () => {};
		public Action fixedUpdateMethod = () => {};
		public Action lateUpdateMethod = () => {};

		// Delegate to call when entering the state.
		public Action <Enum> enterMethod = (state) => { };

		// Delegate to call when exiting the state.
		public Action <Enum> exitMethod = (state) => { };

		// If set, overrides the allowed transistion states.
		public bool forceTransition = false;

		// States we are allowed to transistion to.
		public List<Enum>  transitions = null;

		public State(Enum name) {
			this.name = name;
			this.transitions = new List<Enum> ();
		}
	}
}