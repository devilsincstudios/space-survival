// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using DI.Core.Debug;
using System;
using UnityEngine;

namespace DI.Core.Behaviours
{
	[Serializable]
	public class DI_MonoBehaviourSingleton<T> : DI_MonoBehaviour
	{
		public static T instance;

		public void makeSingleton(T child)
		{
			if (instance == null) {
				instance = (T)child;
			}
			else {
				instance = (T)child;
			}
		}
	}
}