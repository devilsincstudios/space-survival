// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
// TODO: Include a description of the file here.
//

namespace DI.Core.Debug
{
	public enum DI_DebugLevel
	{
		ALL,
		ENGINE,
		INFO,
		LOW,
		MEDIUM,
		HIGH,
		CRITICAL,
		NONE
	}
}