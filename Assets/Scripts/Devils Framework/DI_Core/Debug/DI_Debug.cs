// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
// TODO: Include a description of the file here.
//

using UnityEngine;
using System.Diagnostics;
using System;
using System.Collections.Generic;

namespace DI.Core.Debug
{
	public static class DI_Debug
	{
		#if DEBUG
			private static bool logToUnity = false;
			private static bool logToCustom = true;
		#endif

		private static DI_DebugLevel globalDebugLevel = DI_DebugLevel.ALL;
		public static List<DI_DebugLogEntry> debugLog;
		public static void setGobalDebugLevel(DI_DebugLevel debugLevel) {
			writeLog(DI_DebugLevel.INFO, "globalDebugLevel changed to: " + Enum.GetName(typeof(DI_DebugLevel), debugLevel));
			globalDebugLevel = debugLevel;
		}

		public static void setUnityLogging(bool shouldLog)
		{
			#if DEBUG
			logToUnity = shouldLog;
			#endif
		}

		public static void setCustomLogging(bool shouldLog)
		{
			#if DEBUG
			logToCustom = shouldLog;
			#endif
		}

		public static DI_DebugLevel getGlobalDebugLevel()
		{
			return globalDebugLevel;
		}

		public static Color getDebugLevelColor(DI_DebugLevel level)
		{
			switch (level) {
				case DI_DebugLevel.ALL:
					return Color.black;
				case DI_DebugLevel.ENGINE:
					return Color.grey;
				case DI_DebugLevel.INFO:
					return new Color(0.15f, 0.15f, 0.55f);
				case DI_DebugLevel.LOW:
					return Color.blue;
				case DI_DebugLevel.MEDIUM:
					return new Color(0.2F, 0.3F, 0.4F);
				case DI_DebugLevel.HIGH:
					return Color.red;
				case DI_DebugLevel.CRITICAL:
					return new Color(0.25f, 0.0f, 0.0f);
				default:
					return Color.green;
			}
		}

		public static bool debugLevelEnabled(DI_DebugLevel level)
		{
			return (level >= globalDebugLevel);
		}

		//Todo replace the unity console with our own stuff.
		public static void writeLog(DI_DebugLevel debugLevel, string message)
		{
			// writeLog does nothing if #DEBUG is not enabled.

			#if DEBUG
			if (debugLevel >= globalDebugLevel) {

				#if DEVELOPMENT_BUILD
				// Pull up the stack data so we know what is calling our logging function.
				StackTrace stackTrace = new StackTrace(false);
				StackFrame stackFrame = stackTrace.GetFrame(1);
				string prefix = "[" + Enum.GetName(typeof(DI_DebugLevel), debugLevel) + ": "
					+ stackFrame.GetFileName() + ":"
					+ stackFrame.GetFileLineNumber() + "] ";
				#else
				string prefix = "[" + Enum.GetName(typeof(DI_DebugLevel), debugLevel) + "]";
				#endif

				// Log entries should be prefixed like such: [<DEBUG LEVEL> FileName:LINE Time]
				if (debugLog == null) {
					debugLog = new List<DI_DebugLogEntry>();
				}

				if (logToCustom) {
					DI_DebugLogEntry logEntry = new DI_DebugLogEntry(debugLevel, prefix + message);
					for (int index = 0; index < debugLog.Count; index++) {
						if (debugLog[index].logLevel == logEntry.logLevel && debugLog[index].message == logEntry.message) {
							logEntry.count += debugLog[index].count;
							try {
								debugLog.Remove(debugLog[index]);
							}
							catch (Exception) {
							}
						}
					}

					debugLog.Add(logEntry);
				}
//
				if (logToUnity) {
					switch (debugLevel) {
						case DI_DebugLevel.ALL:
						UnityEngine.Debug.Log("<color=pink>" + prefix + "</color>" + message);
						break;
						case DI_DebugLevel.ENGINE:
						UnityEngine.Debug.Log("<color=cyan>" + prefix + "</color>" + message);
						break;
						case DI_DebugLevel.INFO:
						UnityEngine.Debug.Log("<color=grey>" + prefix + "</color>" + message);
						break;
						case DI_DebugLevel.LOW:
						UnityEngine.Debug.Log("<color=green>" + prefix + "</color>" + message);
						break;
						case DI_DebugLevel.MEDIUM:
						UnityEngine.Debug.LogWarning("<color=yellow>" + prefix + "</color>" + message);
						break;
						case DI_DebugLevel.HIGH:
						UnityEngine.Debug.LogError("<color=orange>" + prefix + "</color>" + message);
						break;
						case DI_DebugLevel.CRITICAL:
						UnityEngine.Debug.LogError("<color=red>" + prefix + "</color>" + message);
						break;
					}
				}
			}
			#endif
		}


		// Taken from SuperCharacterController by IronWarrior
		// It's licensed with the MIT License.
		// http://roystanross.wordpress.com/

		public static void DrawMarker(Vector3 position, float size, Color color, float duration, bool depthTest = true)
		{
			Vector3 line1PosA = position + Vector3.up * size * 0.5f;
			Vector3 line1PosB = position - Vector3.up * size * 0.5f;

			Vector3 line2PosA = position + Vector3.right * size * 0.5f;
			Vector3 line2PosB = position - Vector3.right * size * 0.5f;

			Vector3 line3PosA = position + Vector3.forward * size * 0.5f;
			Vector3 line3PosB = position - Vector3.forward * size * 0.5f;

			UnityEngine.Debug.DrawLine(line1PosA, line1PosB, color, duration, depthTest);
			UnityEngine.Debug.DrawLine(line2PosA, line2PosB, color, duration, depthTest);
			UnityEngine.Debug.DrawLine(line3PosA, line3PosB, color, duration, depthTest);
		}

		public static Color RandomColor()
		{
			return new Color(UnityEngine.Random.value, UnityEngine.Random.value, UnityEngine.Random.value);
		}
	}
}
