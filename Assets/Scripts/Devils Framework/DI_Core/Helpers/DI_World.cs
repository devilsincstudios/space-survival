// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using System;
using UnityEngine;

namespace DI.Core.Helpers
{
	public static class DI_World
	{
		public static float convertTimeToSunAngle(int hours, int minutes)
		{
			float minuteAngle = 0.25f;
			float start = 270.0f;

			float totalMinutes = hours * 60 + minutes;

			float angle = start + (totalMinutes * minuteAngle);

			if (angle > 360) {
				angle -= 360;
			}

			return angle;
		}

		// Taken from SuperCharacterController by IronWarrior
		// It's licensed with the MIT License.
		// http://roystanross.wordpress.com/

		public static Vector3 ClosestPointOn(BoxCollider collider, Vector3 to)
		{
			if (collider.transform.rotation == Quaternion.identity)
			{
				return collider.ClosestPointOnBounds(to);
			}

			return closestPointOnOBB(collider, to);
		}

		public static Vector3 ClosestPointOn(SphereCollider collider, Vector3 to)
		{
			Vector3 p;

			p = to - collider.transform.position;
			p.Normalize();

			p *= collider.radius * collider.transform.localScale.x;
			p += collider.transform.position;

			return p;
		}

		public static Vector3 closestPointOnOBB(BoxCollider collider, Vector3 to)
		{
			// Cache the collider transform
			var ct = collider.transform;

			// Firstly, transform the point into the space of the collider
			var local = ct.InverseTransformPoint(to);

			// Now, shift it to be in the center of the box
			local -= collider.center;

			// Inverse scale it by the colliders scale
			var localNorm =
				new Vector3(
					Mathf.Clamp(local.x, -collider.size.x * 0.5f, collider.size.x * 0.5f),
					Mathf.Clamp(local.y, -collider.size.y * 0.5f, collider.size.y * 0.5f),
					Mathf.Clamp(local.z, -collider.size.z * 0.5f, collider.size.z * 0.5f)
				);

			// Now we undo our transformations
			localNorm += collider.center;

			// Return resulting point
			return ct.TransformPoint(localNorm);
		}
	}
}