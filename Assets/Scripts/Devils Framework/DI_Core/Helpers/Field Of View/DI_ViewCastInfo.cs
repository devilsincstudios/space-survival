﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//
//
// Thanks to Sebastian Lague for the tutorial on FoV @ https://github.com/SebLague/Field-of-View
//
using UnityEngine;

namespace DI.Core.Helpers
{
	public struct DI_ViewCastInfo
	{
		public bool hit;
		public Vector3 point;
		public float distance;
		public float angle;

		public DI_ViewCastInfo(bool _hit, Vector3 _point, float _distance, float _angle)
		{
			hit = _hit;
			point = _point;
			distance = _distance;
			angle = _angle;
		}
	}
}