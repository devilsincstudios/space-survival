﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//
//
// Thanks to Sebastian Lague for the tutorial on FoV @ https://github.com/SebLague/Field-of-View
//
using UnityEngine;

namespace DI.Core.Helpers
{
	public struct DI_EdgeInfo
	{
		public Vector3 pointA;
		public Vector3 pointB;

		public DI_EdgeInfo(Vector3 _pointA, Vector3 _pointB)
		{
			pointA = _pointA;
			pointB = _pointB;
		}
	}
}