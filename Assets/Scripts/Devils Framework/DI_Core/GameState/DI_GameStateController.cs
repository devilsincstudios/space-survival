// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2014, 2015
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using System;
using System.Collections.Generic;
using UnityEngine.Networking;

using DI.Core.Behaviours;
using DI.Core.Debug;

// TODO refactor this class
// This contains elements of a menu system and gamestate controls and needs to be split for multiplayer.

namespace DI.Core.GameState
{
	[AddComponentMenu("Game State/Game State Controller")]
	public class DI_GameStateController : DI_MonoBehaviour
	{
		public DI_GameStates gameState;
		public static DI_GameStateController instance;

		//TODO REMOVE ME
		public Canvas skipInfo;

		/// <summary>
		/// Called when a cinema starts that is unskipable
		/// </summary>
		public void enterCinemaNoSkip()
		{
			#if DEBUG
			DI_Debug.writeLog(DI_DebugLevel.ENGINE,"enterCinemaNoSkip");
			#endif
			gameState = DI_GameStates.IN_CUT_SCENE;
		}

		/// <summary>
		/// Called when a cinema starts
		/// </summary>
		public void enterCinema()
		{
			#if DEBUG
			DI_Debug.writeLog(DI_DebugLevel.ENGINE,"enterCinema");
			#endif
			gameState = DI_GameStates.IN_CUT_SCENE;
			skipInfo.gameObject.SetActive(true);
		}

		/// <summary>
		/// Called when the cinema exits
		/// </summary>
		public void exitCinema()
		{
			#if DEBUG
			DI_Debug.writeLog(DI_DebugLevel.ENGINE,"exitCinema");
			#endif
			gameState = DI_GameStates.PLAYING;
			skipInfo.gameObject.SetActive(false);
		}

		/// <summary>
		/// Called when the cinema exits
		/// </summary>
		public void exitCinemaNoSkip()
		{
			#if DEBUG
			DI_Debug.writeLog(DI_DebugLevel.ENGINE,"exitCinemaNoSkip");
			#endif
			gameState = DI_GameStates.PLAYING;
		}

		public void Awake()
		{
			instance = this;
		}
	}
}
