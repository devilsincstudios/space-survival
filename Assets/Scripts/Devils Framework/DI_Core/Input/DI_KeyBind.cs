// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using System;
using System.Xml.Serialization;
using System.Xml;
using UnityEngine;

namespace DI.Core.Input
{
	[Serializable]
	public class DI_KeyBind
	{
		[XmlAttribute("Bind Type")]
		public DI_KeyBindType bindType;
		[XmlAttribute("Bound Key")]
		public string bindKey;
		public int bindIndex;
		[XmlAttribute("Player Id")]
		public int playerId;
		[XmlAttribute("Bind Name")]
		public string bindName;
		[XmlAttribute("Dead Zone")]
		public float deadZone;
		public DI_KeyState bindState;
		
		public string toString()
		{
			return ("Name: " + bindName + " Type: " + bindType + " Key: " + bindKey + " Player Id: " + playerId);
		}
		
		public DI_KeyBind() {}
		public DI_KeyBind(string name, DI_KeyBindType type, string key, int player)
		{
			bindName = name;
			bindType = type;
			bindKey = key;
			playerId = player;
		}
		
		public bool save()
		{
			if (bindName != null && bindKey != null)
			{
				string typeString = "Input Manager-" + playerId + "-" + bindName + "-Type";
				string boundKeyString = "Input Manager-" + playerId + "-" + bindName + "-Bound Key";
				string deadZoneString = "Input Manager-" + playerId + "-" + bindName + "-Dead Zone";
				
				PlayerPrefs.SetString(typeString, typeEnumToString(bindType));
				PlayerPrefs.SetString(boundKeyString, bindKey);
				PlayerPrefs.SetFloat(deadZoneString, deadZone);
				return true;
			}
			return false;
		}
		
		public static string typeEnumToString(DI_KeyBindType type)
		{
			string retval = "UNDEFINED";
			switch(type)
			{
			case DI_KeyBindType.BIND_AXIS:
				retval = "BIND_AXIS";
				break;
			case DI_KeyBindType.BIND_BUTTON:
				retval = "BIND_BUTTON";
				break;
			case DI_KeyBindType.BIND_KEY:
				retval = "BIND_KEY";
				break;
			}
			return retval;
		}
		
		public static DI_KeyBindType typeStringToEnum(string type)
		{
			DI_KeyBindType retval = DI_KeyBindType.UNDEFINED;
			switch (type)
			{
			case "BIND_AXIS":
				retval = DI_KeyBindType.BIND_AXIS;
				break;
			case "BIND_BUTTON":
				retval = DI_KeyBindType.BIND_BUTTON;
				break;
			case "BIND_KEY":
				retval = DI_KeyBindType.BIND_KEY;
				break;
			}
			return retval;
		}
	}
}