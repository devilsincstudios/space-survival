// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

namespace DI.Core.Input
{
	public enum DI_KeyState
	{
		AXIS_HELD_NEGATIVE,
		AXIS_HELD_POSITIVE,
		AXIS_PRESSED_POSITIVE,
		AXIS_PRESSED_NEGATIVE,
		AXIS_NOT_PRESSED,
		KEY_PRESSED,
		KEY_NOT_PRESSED,
		KEY_HELD,
		UNKNOWN
	}
}