// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using System;
using System.Xml.Serialization;
using System.Xml;

namespace DI.Core.Input
{
	[Serializable]
	public enum DI_ControllerType
	{
		CONTROLLER,
		KEYBOARD,
		NONE,
	}
}