// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using DI.Entities.Properties;
using DI.Core.Events;
using DI.Core.Debug;
using DI.SFX;

using UnityEngine;
using System;

namespace DI.Entities.Core
{
	[Serializable]
	public class DI_Entity : ScriptableObject
	{
		protected System.Guid entityId;

		[Header("Entity Properties")]
		[SerializeField]
		protected DI_MovementProperty movementSpeed;
		[SerializeField]
		protected float currentMovementSpeed;
		[SerializeField]
		protected DI_MovementProperty turningSpeed;
		[SerializeField]
		protected float currentTurningSpeed;
		[SerializeField]
		protected bool isAbleToMove = true;
		public float groundCheckDistance = 0.1f;

		[SerializeField]
		protected DI_HealthProperty healthProperties;

		[Header("Entity Sound FX Properties")]
		[SerializeField]
		protected DI_SFXProperty sfxPropertiesOnSpawn;
		[SerializeField]
		protected DI_SFXProperty sfxPropertiesOnDespawn;

		[Header("Entity Settings")]
		[SerializeField]
		protected GameObject entityBody;
		protected Rigidbody entityRigidbody;

		public DI_SFXProperty getSpawnSFX()
		{
			return sfxPropertiesOnSpawn;
		}

		public DI_SFXProperty getDespawnSFX()
		{
			return sfxPropertiesOnDespawn;
		}

		public void addSpawnSFX(DI_SFXClipProperties clip)
		{
			if (!sfxPropertiesOnSpawn.sfxs.Contains(clip)) {
				sfxPropertiesOnSpawn.sfxs.Add(clip);
			}
		}

		public void removeSpawnSFX(DI_SFXClipProperties clip)
		{
			if (sfxPropertiesOnSpawn.sfxs.Contains(clip)) {
				sfxPropertiesOnSpawn.sfxs.Remove(clip);
			}
		}

		// On Despawn Sound Effects
		public void addDespawnSFX(DI_SFXClipProperties clip)
		{
			if (!sfxPropertiesOnDespawn.sfxs.Contains(clip)) {
				sfxPropertiesOnDespawn.sfxs.Add(clip);
			}
		}
		
		public void removeDespawnSFX(DI_SFXClipProperties clip)
		{
			if (sfxPropertiesOnDespawn.sfxs.Contains(clip)) {
				sfxPropertiesOnDespawn.sfxs.Remove(clip);
			}
		}

		// Health Property
		public void setHealth(float newHealth)
		{
			healthProperties.currentHealth = newHealth;
		}

		public void removeHealth(float amount)
		{
			addHealth(-amount);
		}

		public void addHealth(float amount)
		{
			healthProperties.currentHealth = Mathf.Clamp(healthProperties.currentHealth + amount, 0f, healthProperties.maxHealth);
		}

		public float getHealth()
		{
			return healthProperties.currentHealth;
		}

		public void setMaxHealth(float newHealth)
		{
			healthProperties.maxHealth = newHealth;
		}

		public float getMaxHealth()
		{
			return healthProperties.maxHealth;
		}

		public void removeMaxHealth(float amount)
		{
			healthProperties.maxHealth = Mathf.Clamp(healthProperties.maxHealth - amount, 0f, healthProperties.maxHealth);
		}

		public void addMaxHealth(float amount)
		{
			healthProperties.maxHealth += amount;
		}

		// Movement Property
		public void setMovementSpeed(float newSpeed)
		{
			currentMovementSpeed = newSpeed;
		}

		public float getMovementSpeed()
		{
			return currentMovementSpeed;
		}

		public void setMovementSpeedIncreaseRate(float newSpeed)
		{
			movementSpeed.increaseRate = newSpeed;
		}
		
		public float getMovementSpeedIncreaseRate()
		{
			return movementSpeed.increaseRate;
		}

		public void setMaxMovementSpeed(float newSpeed)
		{
			movementSpeed.maxSpeed = newSpeed;
		}

		public float getMaxMovementSpeed()
		{
			return movementSpeed.maxSpeed;
		}

		public void setTurnSpeed(float newSpeed)
		{
			currentTurningSpeed = newSpeed;
		}

		public float getTurnSpeed()
		{
			return currentTurningSpeed;
		}

		public void setMaxTurnSpeed(float newSpeed)
		{
			turningSpeed.maxSpeed = newSpeed;
		}

		public float getMaxTurnSpeed()
		{
			return turningSpeed.maxSpeed;
		}

		public void setTurnSpeedIncreaseRate(float newSpeed)
		{
			turningSpeed.increaseRate = newSpeed;
		}
		
		public float getTurnSpeedIncreaseRate()
		{
			return turningSpeed.increaseRate;
		}

		public bool canMove()
		{
			return isAbleToMove;
		}

		public void canMove(bool movementAllowed)
		{
			isAbleToMove = movementAllowed;
		}

		public GameObject getEntity()
		{
			return entityBody;
		}

		public void setEntity(GameObject body)
		{
			entityBody = body;
		}

		public Rigidbody getEntityBody()
		{
			return entityRigidbody;
		}

		public void setEntityBody(Rigidbody _entityRigidbody)
		{
			entityBody = _entityRigidbody.gameObject;
			entityRigidbody = _entityRigidbody;
		}

		public void playSFX(DI_SFXClipProperties sfxProperty) {
			if (entityBody != null) {
				DI_SFX.playClipAtPoint(entityBody, entityBody.transform.position, sfxProperty);
			}
			else {
				DI_Debug.writeLog(DI_DebugLevel.HIGH, "Attempted to play an sfx at point without a location.");
			}
		}

		// We don't know what the entity looks like, does it need armor reset, animations?
		// The class that inherits this will need to set all this in its overriden onSpawn().
		public virtual void resetEntity() {}

		// This lets you keep track of the individual instance (life) of an entity.
		public System.Guid getEntityId()
		{
			return entityId;
		}

		// Entity logic
		public void onDespawn()
		{
			DI_EventCenter<DI_Entity>.invoke("OnDespawn", this);
			//DI_Debug.writeLog(DI_DebugLevel.LOW, "Entity onDespawn(" + this.entityId.ToString() + ")");
			if (sfxPropertiesOnDespawn.hasSFX) {
				DI_SFX.playRandomClipAtPoint(this.getEntity(), this.getEntity().transform.position, sfxPropertiesOnDespawn);
			}
		}

		public void onSpawn()
		{
			entityId = System.Guid.NewGuid();
			DI_EventCenter<DI_Entity>.invoke("OnSpawn", this);
			DI_Debug.writeLog(DI_DebugLevel.LOW, "Entity onSpawn(" + this.entityId.ToString() + ")");
			if (sfxPropertiesOnSpawn.hasSFX) {
				DI_SFX.playRandomClipAtPoint(this.getEntity(), this.getEntity().transform.position, sfxPropertiesOnDespawn);
			}
		}
	}
}