﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using System;

namespace DI.Entities.Properties
{
	[Serializable]
	public struct DI_RopeMovementProperty
	{
		public bool onRope;
		public float maxMovementSpeed;
		public float movementSpeedIncreaseRate;
	}
}