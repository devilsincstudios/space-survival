// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using System;

namespace DI.Entities.Properties
{
	[Serializable]
	public class DI_AnimationProperty
	{
		public bool hasAnimations;
		public Animator animator;
		public Vector3 leftHandIKTarget;
		public float leftHandWeight = 0f;
		public Vector3 rightHandIKTarget;
		public float rightHandWeight = 0f;
		public Vector3 leftFootIKTarget;
		public float leftFootWeight = 0f;
		public Vector3 rightFootIKTarget;
		public float rightFootWeight = 0f;
		public Vector3 headLookTarget;
		public float headLookWeight = 0f;
		public Vector3 spineIKRotation;
	}
}