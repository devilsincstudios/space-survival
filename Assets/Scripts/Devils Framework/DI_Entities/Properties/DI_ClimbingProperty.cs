﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using System;

namespace DI.Entities.Properties
{
	[Serializable]
	public struct DI_ClimbingProperty
	{
		public bool isClimbing;
		public float maxMovementSpeed;
		public float movementSpeedIncreaseRate;
	}
}