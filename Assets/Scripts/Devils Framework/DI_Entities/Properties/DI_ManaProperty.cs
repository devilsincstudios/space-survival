﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using System;

namespace DI.Entities.Properties
{
    [Serializable]
    public struct DI_ManaProperty
    {
        public float maxMana;
        public float currentMana;
        public float regenAmount;
        public bool doesRegen;
        public bool hasUnlimitedMana;
    }
}
