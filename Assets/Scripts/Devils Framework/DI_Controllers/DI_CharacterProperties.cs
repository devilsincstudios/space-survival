//// Devils Inc Studios
//// Copyright DEVILS INC. STUDIOS LIMITED 2016
////
//// TODO: Include a description of the file here.
////
//
//using System;
//using System.Collections.Generic;
//using UnityEngine;
//
//using DI.Entities.Properties;
//using DI.Entities.Player;
//
//namespace DI.Controllers
//{
//	[Serializable]
//	public class DI_CharacterProperties : DI.Entities.Player.Player
//	{
//		[Header("Controller Settings")]
//		public CharacterController controller;
//		[Header("Property Settings")]
//		public DI_RunSpeedProperty sneakSpeedSettings;
//		public DI_RunSpeedProperty runSpeedSettings;
//		public DI_RunSpeedProperty sprintSpeedSettings;
//		public DI_StaminaProperty staminaSettings;
//		public DI_JumpProperty jumpSettings;
//		[Header("Controller State Settings")]
//		public bool isWalking = false;
//		public bool isRunning = false;
//		public bool isSprinting = false;
//		public bool isSneaking = false;
//		public bool isIdle = true;
//		public bool isJumping = false;
//		public bool isInControl = true;
//		[Header("Ground Detection Settings")]
//		public float distanceFromGround = 0.0f;
//		public bool isGrounded = false;
//		public float groundFudge = 0.6f;
//		protected Vector3 colliderOffsets = Vector3.zero;
//		protected float colliderHeight = 0f;
//
//		[Header("Movement Sounds")]
//		public bool playMovementSounds = false;
//		public float strideLength = 0.10f;
//		public DI_SFXProperty footstepsWalking;
//		public DI_SFXProperty footstepsRunning;
//
//		[Header("Debug View: Input Cache")]
//		public float verticalMovement = 0.0f;
//		public float horizontalMovement = 0.0f;
//
//		public void setColliderOffsets(Vector3 center, float height)
//		{
//			colliderHeight = height;
//			colliderOffsets = center;
//		}
////
////		public void addMana(float amount)
////		{
////			manaProperties.currentMana = Mathf.Clamp(manaProperties.currentMana + amount, 0, manaProperties.maxMana);
////		}
////
////		public void removeMana(float amount)
////		{
////			manaProperties.currentMana = Mathf.Clamp(manaProperties.currentMana - amount, 0, manaProperties.maxMana);
////		}
//
//		// Convenience functions
//		[Obsolete()]
//		public void startSneak()
//		{
//			isSneaking = true;
//			isWalking = false;
//			isRunning = false;
//			isSprinting = false;
//			isIdle = false;
//			if (colliderHeight != 0f) {
//				controller.center = new Vector3(colliderOffsets.x, colliderOffsets.y, colliderOffsets.z);
//				controller.height = colliderHeight / 2;
//			}
//		}
//
//		[Obsolete()]
//		public void startWalk()
//		{
//			isSneaking = false;
//			isWalking = true;
//			isRunning = false;
//			isSprinting = false;
//			isIdle = false;
//
//			if (colliderHeight != 0f) {
//				controller.center = colliderOffsets;
//				controller.height = colliderHeight;
//			}
//		}
//
//		[Obsolete()]
//		public void startRun()
//		{
//			isSneaking = false;
//			isWalking = false;
//			isRunning = true;
//			isSprinting = false;
//			isIdle = false;
//			if (colliderHeight != 0f) {
//				controller.center = colliderOffsets;
//				controller.height = colliderHeight;
//			}
//		}
//
//		[Obsolete()]
//		public void startSprint()
//		{
//			isSneaking = false;
//			isWalking = false;
//			isRunning = false;
//			isSprinting = true;
//			isIdle = false;
//
//			if (colliderHeight != 0f) {
//				controller.center = colliderOffsets;
//				controller.height = colliderHeight;
//			}
//		}
//
//		public void resetInput()
//		{
//			isSneaking = false;
//			isWalking = false;
//			isRunning = false;
//			isSprinting = false;
//			isIdle = true;
//		}
//
//		[Obsolete()]
//		public void startIdle()
//		{
//			isSneaking = false;
//			isWalking = false;
//			isRunning = false;
//			isSprinting = false;
//			isIdle = true;
//
//			if (colliderHeight != 0f) {
//				controller.center = colliderOffsets;
//				controller.height = colliderHeight;
//			}
//		}
//
//		[Obsolete("updateAnimator")]
//		public void updateAnimator()
//		{
////			if (animationProperties.hasAnimations) {
////				if (isInControl) {
////					animationProperties.animator.SetBool("Sneaking", isSneaking);
////					animationProperties.animator.SetBool("Walking", isWalking);
////					animationProperties.animator.SetBool("Running", isRunning);
////					animationProperties.animator.SetBool("Sprinting", isSprinting);
////					animationProperties.animator.SetBool("Idling", isIdle);
////					if (isSneaking) {
////						animationProperties.animator.SetFloat("Speed", verticalMovement);
////					}
////					if (isWalking) {
////						animationProperties.animator.SetFloat("Speed", verticalMovement * 2);
////					}
////					else if (isSprinting) {
////						animationProperties.animator.SetFloat("Speed", verticalMovement * 4);
////					}
////					else if (isRunning) {
////						animationProperties.animator.SetFloat("Speed", verticalMovement * 3);
////					}
////					animationProperties.animator.SetFloat("Direction", horizontalMovement);
////					animationProperties.animator.SetFloat("Distance From Ground", distanceFromGround);
////					if (animationProperties.animator.GetFloat("Fall Height") <= distanceFromGround) {
////						animationProperties.animator.SetFloat("Fall Height", distanceFromGround);
////					}
////					else if (isGrounded) {
////						animationProperties.animator.SetFloat("Fall Height", distanceFromGround);
////					}
////				}
////			}
//		}
//	}
//}